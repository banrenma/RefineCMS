<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Response.CharSet            = "utf-8"
' Response.CharSet            = "gb2312"
Response.ContentType        = "text/html"
Response.CacheControl       = "no-cache"
Response.AddHeader "pragma", "no-cache"
Response.Expires            = -1
Response.ExpiresAbsolute    = Now()-1

Dim g_twStack
Set g_twStack = new cls_Stack
 '计算页面总执行时间，开始计时位置。注意：由于该函数用到了g_twStack，导致前面的时间无法统计在内。
 ' 与之对应的结束位置在 \libs\cls_timewatcher.asp 的 Function showWatcherList()。
rfStartWatch()


Dim g_timerWatcher
Set g_timerWatcher = new cls_TimeWatcher

' 本系统暂时没有用到加解密的地方，故先行注释
' Dim g_rsa
' Set g_rsa = new cls_CustomRSA

Dim g_conn
Set g_conn = Server.CreateObject("ADODB.connection")
g_conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath (RF_DB_DATABASE)

Dim lang
Set lang = new cls_Language
lang.init()
Set lang = Nothing

Dim g_action_category_map
Set g_action_category_map = Server.CreateObject("Scripting.Dictionary")
Dim map_i, map_item
For map_i=0 to Ubound(ACTION_CATEGORY_MAP)
    map_item = ACTION_CATEGORY_MAP(map_i)
    g_action_category_map.add map_item(0), map_item(1)
Next

%>
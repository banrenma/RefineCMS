<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<%

Dim action
action = Request.QueryString(QP("rf_action"))
If action = "backend" Then
    rfOpenTreeviewTag
    rfAccessAuthority
    Select Case Request.QueryString("module")
        Case "login"
            rf_mod_login
        Case "logout"
            rf_mod_logout
        Case "dashboard"
            rf_mod_dashboard
        Case "user.list"
            rf_mod_user_list
        Case "user.info"
            rf_mod_user_info
        Case "ds.category"
            rf_mod_ds_category
        Case "ds.category.info"
            rf_mod_ds_category_info
        Case "ds.essays"
            rf_mod_ds_essay_list
        Case "ds.essays.info"
            rf_mod_ds_essay_info
        Case "ds.singlepage"
            rf_mod_ds_singlepage
        Case "ds.singlepage.info"
            rf_mod_ds_singlepage_info
        Case "ds.photos"
            rf_mod_ds_photo_list
        Case "ds.photos.insert"
            rf_mod_ds_photo_list
        Case "ds.photos.info"
            rf_mod_ds_photo_info
        Case "ds.photos.get"
            rf_mod_ds_photo_get
        Case "ds.friendlinks"
            rf_mod_ds_friendlinks_list
        Case "ds.friendlinks.info"
            rf_mod_ds_friendlinks_info
        Case "presentation.list"
            rf_mod_presentation_list
        Case "presentation.info"
            rf_mod_presentation_info
        Case "tag.list"
            rf_mod_tag_list
        Case "tag.import"
            rf_mod_tag_import
        Case "tag.essay_list"
            rf_mod_tag_essay_list
        Case "tag.photo_list"
            rf_mod_tag_photo_list
        Case "tag.singlepage"
            rf_mod_tag_singlepage
        Case "tag.breadcrumb"
            rf_mod_tag_breadcrumb
        Case "tag.mainnav"
            rf_mod_tag_mainnav
        Case "tag.subnav"
            rf_mod_tag_subnav
        Case "tag.friendlink"
            rf_mod_tag_friendlink
        Case "tag.partial"
            rf_mod_tag_partial
        Case "tag.flexslider"
            rf_mod_tag_flexslider
        Case "tag.config"
            rf_mod_tag_config
        Case "menus"
            rf_mod_menu_list
        Case "menus.info"
            rf_mod_menu_info
        Case "tpl"
            rf_mod_tpl
        Case "tpl.info"
            rf_mod_tpl_info
        Case "help"
            rf_mod_help
        Case Else
            rfShowErrorAuto "index.asp", T("rf_lang_page_not_found")
    End Select
Else
    ' 前端页面
    
    rfPageCacheGet
    Dim parser
    Set parser = new cls_Parser
    parser.render
End If

    %>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<!-- #include file="libs/helper.asp" -->
<!-- #include file="libs/sha256.asp" -->
<!-- #include file="libs/cls_customRSA.asp" -->
<!-- #include file="libs/freeASPUpload.asp" -->
<!-- #include file="libs/cls_imginfo.asp" -->
<!-- #include file="libs/cls_language.asp" -->
<!-- #include file="libs/cls_timewatcher.asp" -->
<!-- #include file="libs/cls_textfile.asp" -->
<!-- #include file="libs/cls_stack.asp" -->
<!-- #include file="libs/cls_pagination.asp" -->
<!-- #include file="libs/cls_loadphotoplugin.asp" -->
<!-- #include file="libs/cls_parser.asp" -->

<!-- #include file="models/cls_admin.asp" -->
<!-- #include file="models/cls_category.asp" -->
<!-- #include file="models/cls_singlepage.asp" -->
<!-- #include file="models/cls_essay.asp" -->
<!-- #include file="models/cls_media.asp" -->
<!-- #include file="models/cls_friendlink.asp" -->
<!-- #include file="models/cls_presentation.asp" -->
<!-- #include file="models/cls_tag.asp" -->
<!-- #include file="models/cls_menu.asp" -->
<!-- #include file="models/cls_config.asp" -->

<!-- #include file="modules/backend/dashboard/dashboard.asp" -->
<!-- #include file="modules/backend/admin/login.asp" -->
<!-- #include file="modules/backend/admin/user_list.asp" -->
<!-- #include file="modules/backend/admin/user_info.asp" -->
<!-- #include file="modules/backend/datasource/category.asp" -->
<!-- #include file="modules/backend/datasource/category_info.asp" -->
<!-- #include file="modules/backend/datasource/essay_list.asp" -->
<!-- #include file="modules/backend/datasource/essay_info.asp" -->
<!-- #include file="modules/backend/datasource/singlepage_list.asp" -->
<!-- #include file="modules/backend/datasource/singlepage_info.asp" -->
<!-- #include file="modules/backend/datasource/photo_list.asp" -->
<!-- #include file="modules/backend/datasource/photo_info.asp" -->
<!-- #include file="modules/backend/datasource/friendlink_list.asp" -->
<!-- #include file="modules/backend/datasource/friendlink_info.asp" -->
<!-- #include file="modules/backend/presentation/list.asp" -->
<!-- #include file="modules/backend/presentation/info.asp" -->
<!-- #include file="modules/backend/tag/list.asp" -->
<!-- #include file="modules/backend/tag/import.asp" -->
<!-- #include file="modules/backend/tag/essay_list.asp" -->
<!-- #include file="modules/backend/tag/photo_list.asp" -->
<!-- #include file="modules/backend/tag/singlepage.asp" -->
<!-- #include file="modules/backend/tag/breadcrumb.asp" -->
<!-- #include file="modules/backend/tag/mainnav.asp" -->
<!-- #include file="modules/backend/tag/subnav.asp" -->
<!-- #include file="modules/backend/tag/friendlink.asp" -->
<!-- #include file="modules/backend/tag/partial.asp" -->
<!-- #include file="modules/backend/tag/flexslider.asp" -->
<!-- #include file="modules/backend/tag/config.asp" -->
<!-- #include file="modules/backend/menu/list.asp" -->
<!-- #include file="modules/backend/menu/info.asp" -->
<!-- #include file="modules/backend/tpl/list.asp" -->
<!-- #include file="modules/backend/tpl/info.asp" -->
<!-- #include file="modules/backend/help/index.asp" -->
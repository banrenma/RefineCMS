<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Option Explicit
' ****************************************************
'  关闭该注释，可以呈现系统默认的报错页。但是，本系统是UTF8，导致错误页乱码。
'  当本系统给出的有限报错信息不足以找到问题代码时，可以尝试注释掉下面的代码。
'  建议，在生产环境打开
' ****************************************************
' On Error Resume Next

' ****************************************************
'  网站名称
' ****************************************************
Const RF_CMS_NAME = "RefineCMS"

' ****************************************************
'  网站首页title，及meta中description、keywords 设置
' ****************************************************
Const RF_HOMEPAGE_TITLE = "RefineCMS首页"
Const RF_HOMEPAGE_DESCRIPTION = "RefineCMS是一款功能简单的ASP内容管理系统（CMS），像LEGO积木的建站系统"
Const RF_HOMEPAGE_KEYWORDS = "RefineCMS, ASP, CMS, 内容管理系统"

' ****************************************************
'  开发环境设为 True ；上线后设置为 False
' ****************************************************
Const ENV_DEBUG = True

' ****************************************************
'  TimeWatcher开关
' 便于查看页面执行时间、数据库访问次数及耗时、以及已有重要执行事件的耗时
' 注：如果设为False，系统支持的下列静态标签将显示未经统计的默认值，请在模板中去掉它们：
    ' {{refinecms_event_all}}
    ' {{refinecms_event_db_total_hits}}
    ' {{refinecms_event_db_total_time}}
' ****************************************************
Const DEBUG_TIMEWATCHER = True

' ****************************************************
'  前台页面缓存开关
' ****************************************************
Const RF_PAGE_CACHE = False

' ****************************************************
'  表单提交时，针对 CSRF 隐藏字段进行校验
' ****************************************************
Const RF_CSRF_CHECK = True

' ****************************************************
'  虚拟目录，比如: '/mysite'
' ****************************************************
Const RF_VIRTUAL_DIR = ""

' ****************************************************
'  格式说明：Array( 系统使用的Request.QueryString(variable),   允许自定义的variable )
' ------------------------------
'  比如：把 Array("rf_action", "action") 修改成Array ("rf_action", "a") ，则
' 原地址："index.asp?action=backend&module=dashboard" 
' 新地址："index.asp?a=backend&module=dashboard"
' ------------------------------
'  比如：
' 把 Array("rf_photos_id", "photos_id") 修改成Array("rf_photos_id", "p_id") ，
' 把 Array("rf_presentation_id", "presentation_id") 修改成Array("rf_presentation_id", "pre_id") ，
' 把 Array("rf_menu_id", "menu_id") 修改成Array("rf_menu_id", "mid") ，
' 则
' 原地址："?photos_id=38&presentation_id=13&listinfotype_id=1&tag_id=0&menu_id=12" 
' 新地址："?p_id=38&pre_id=13&listinfotype_id=1&tag_id=0&mid=12"
' ------------------------------
'  注意：（1）自定义的字符串，不可重复
'  注意：（2）ACTION_CATEGORY_MAP 的每个元素是数组，其中第一个元素以 "rf_" 打头，不可修改；第二个元素可修改，以实现自定义效果。
' ****************************************************
Dim ACTION_CATEGORY_MAP
ACTION_CATEGORY_MAP = Array(_
    Array("rf_action", "action"),_
    Array("rf_siglepage_id", "siglepage_id"),_
    Array("rf_tag_id", "tag_id"),_
    Array("rf_menu_id", "menu_id"),_
    Array("rf_essays_id", "essays_id"),_
    Array("rf_photos_id", "photos_id"),_
    Array("rf_category_id", "category_id"),_
    Array("rf_presentation_id", "presentation_id"),_
    Array("rf_listinfotype_id", "listinfotype_id"),_
    Array("rf_page", "page")_
)

' ****************************************************
'  网站语言
'  对应于 /languages/ 下的一个文件夹
' ****************************************************
Const RF_LANGUAGE = "zh_cn"
' Const RF_LANGUAGE = "en_us"

' ****************************************************
'  后台单个IP、当天登录次数上限
'  防止暴力破解登录，尽管有图片验证码了
' ****************************************************
Const RF_TODAY_ADMIN_LOGIN_COUNT = 100

' ****************************************************
'  上传目录
'   放置在站点所属虚拟目录之下
' ****************************************************
Dim RF_UPLOAD_FOLDER, RF_UPLOAD_FILESIZE, RF_UPLOAD_FILETYPE_PHOTO
RF_UPLOAD_FOLDER = RF_VIRTUAL_DIR & "/uploads/"
RF_UPLOAD_FILESIZE = 1024 * 1024 * 2 '''Bytes 上传文件最大字节数
RF_UPLOAD_FILETYPE_PHOTO = Array("jpg", "gif", "png", "bmp") '注：支持类型受限于cls_ImgInfo类，不可随意增加

' ****************************************************
'  模板目录
' ****************************************************
Dim RF_TEMPLATE_FOLDER, RF_TEMPLATE_FOLDER_FRONTEND, RF_TEMPLATE_FILETYPE
RF_TEMPLATE_FOLDER = RF_VIRTUAL_DIR & "/templates/"
RF_TEMPLATE_FOLDER_FRONTEND = RF_VIRTUAL_DIR & "/templates/frontend/"
RF_TEMPLATE_FILETYPE = Array("asp", "html", "htm", "txt") '注：只能是文本文件

'  数据库
Dim RF_DB_TYPE
' ****************************************************
'  数据库：Access
'  请务必修改数据库名字和路径
' ****************************************************
Dim RF_DB_DATABASE
RF_DB_TYPE              = 1
RF_DB_DATABASE     = RF_VIRTUAL_DIR & "/database/refine_2005_2018!.mdb"
' ****************************************************
'  数据库：SQL Server 2000/2005
'  取消注释，数据库采用 SQL Server
'  ！！注意：先不支持，暂且做个样子咯
' ****************************************************
' Dim SQLServer_SERVER, SQLServer_DB, SQLServer_UID, SQLServer_PWD
' RF_DB_TYPE         = 2                 '2
' SQLServer_SERVER = ""                'server
' SQLServer_DB     = ""                'database
' SQLServer_UID    = ""                'user
' SQLServer_PWD    = ""                'password

' ****************************************************
'  不允许直接单独访问（或运行）其它文件，请勿修改！
' ****************************************************
Dim DIRECT_VISIT
DIRECT_VISIT = True

' ****************************************************
'  当总页数=1时，是否隐藏页码
' 为了方便调试以及避免不必要的混淆，默认显示。
' ****************************************************
Const Hidden_IF_LESS_THAN_PAGESIZE = False

' ****************************************************
'  后台重要页面列表分页条数设置
' ****************************************************
Const RF_PAGINATION_USER_LIST = 10
Const RF_PAGINATION_CATEGORY_LIST = 10
Const RF_PAGINATION_ESSAY_LIST = 10
Const RF_PAGINATION_PHOTO_LIST = 10
Const RF_PAGINATION_SINGLEPAGE_LIST = 10
Const RF_PAGINATION_PRESENTATION_LIST = 10
Const RF_PAGINATION_TAG_LIST = 10

' ****************************************************
'  站点URL http打头
' ****************************************************
Const RF_SITE_URL_PRE = "http://"

' ****************************************************
'  前台页面缓存前缀
' ****************************************************
Const RF_PAGE_CACHE_PREFIX = "rf_page_cache_"
%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
# 以"#"打头的行，约定为注释并忽略。系统以"rf_lang_"为前缀进行正则匹配。
# 你也可以添加自定义的标签，请确保不要key不要重复，以免覆盖。
# "@" 左侧代表key，右侧代表呈现在网站前后台网页里的对应文本。"@" 两侧的文本的两侧空格会去掉，但是文本中间的空格会保留。
# 此文件将根据 RF_LANGUAGE 指向对应的文件名，比如 Const RF_LANGUAGE = "zh_cn"，则 zh_cn.inc 将被解析。


#################### 以上字符，尽量别动 ####################

#refineCMS 前台页面


#refineCMS 公共词语
rf_lang_homepage @ 首页
rf_lang_submit @ 提交
rf_lang_opt @ 操作
rf_lang_save @ 保存
rf_lang_add @ 添加
rf_lang_edit @ 修改
rf_lang_copy @ 复制
rf_lang_delete @ 删除
rf_lang_has @ 有
rf_lang_has_not @ 无
rf_lang_list @ 清单
rf_lang_unknow @ 未知
rf_lang_are_you_sure @ 你确定要执行这项操作吗？
rf_lang_list_is_empty @ 还没有任何记录~
rf_lang_created_at @ 创建于
rf_lang_updated_at @ 更新于
rf_lang_operater @ 操作人员
rf_lang_operate_fail @ 操作失败
rf_lang_detail @ 详情
rf_lang_remark @ 备注
rf_lang_property @ 属性
rf_lang_official @ 官方
rf_lang_no_official @ 自定义
rf_lang_cannot_find_the_item @ 找不到对应的条目
rf_lang_config_presentation @ 默认设置
rf_lang_config_presentation_cannot_find @ 默认设置加载失败
rf_lang_config_other @ 杂项
rf_lang_close_window @ 关闭
rf_lang_page_not_found @ 路由错误，找不到你要访问的页面
rf_lang_file_not_found @ 找不到要加载的文件
rf_lang_flush_cache @ 清除缓存
rf_lang_help @ 帮助中心
rf_lang_unit_number @ 个
rf_lang_reference @ 参考资料
rf_lang_iis @ IIS 配置
rf_lang_uploads @ 上传图片文件夹
rf_lang_uploads_size @ 上传图片文件夹占用空间
rf_lang_folder_not_exists @ 该文件夹不存在
rf_lang_database @ 数据库地址
rf_lang_upload_max_filesize @ 上传文件大小限制
rf_lang_upload_filetype @ 上传文件的允许种类
rf_lang_language @ 语言包
rf_lang_env_debug @ 开发模式（Debug）状态
rf_lang_timewatcher @ 页面执行时间统计
rf_lang_current_and_total_pagenum @ 页码
rf_lang_status_success @ 良好
rf_lang_status_fail @ 不对劲儿


#refineCMS 后台管理页面
rf_lang_construction @ 栏目管理
rf_lang_tools @ 工具箱
rf_lang_admin_page_user_management @ 管理员
rf_lang_login_user @ 账号
rf_lang_access_authority_deny @ 权限不足，只有超级管理员才能访问
rf_lang_login_user_validate @ 账号必须为字母、数字或下划线，只能以字母打头，不超过32个字符
rf_lang_login_user_exists @ 账号已经存在了
rf_lang_login_password @ 密码
rf_lang_login_password_placeholder @ 分配初始密码或重置密码，如不修改，请留空
rf_lang_login_scode @ 验证码
rf_lang_csrf_deny @ 非法提交表单
rf_lang_captcha_deny @ 图片验证码输入错误
rf_lang_login_username_password_dismatch @ 账号或密码错误
rf_lang_login_too_many_count @ 登录次数过多，禁止操作
rf_lang_login_the_user_is_forbidden @ 该账号已被停用
rf_lang_login_the_user_is_deleted @ 该账号已被删除
rf_lang_login_role @ 角色
rf_lang_user_role_admin @ 超级管理员
rf_lang_user_role_editor @ 编辑
rf_lang_login_created_at @ 创建于
rf_lang_login_created_ip @ 创建IP
rf_lang_login_last_time @ 最后登录于
rf_lang_login_last_ip @ 最后登录IP
rf_lang_login_hits @ 总登录次数
rf_lang_login_status @ 状态
rf_lang_user_status_normal @ 正常
rf_lang_user_status_forbidden @ 禁止登录
rf_lang_user_status_deleted @ 已删除
rf_lang_dashboard @ 仪表盘
rf_lang_datasource @ 内容
rf_lang_datasource_singlepage @ 单页
rf_lang_datasource_category @ 分类
rf_lang_datasource_category_essays @ 分类（文章）
rf_lang_datasource_category_photos @ 分类（图片）
rf_lang_datasource_category_friendlinks @ 分类（友情链接）
rf_lang_datasource_category_singlepage @ 分类（单页）
rf_lang_datasource_category_all @ 所有分类
rf_lang_catid_validate @ 分类不能为空
rf_lang_category_catname_validate @ 长度介于1~30个字符
rf_lang_category_catname_exists @ 分类名称已存在
rf_lang_category_catname @ 名称
rf_lang_category_cattype @ 类型
rf_lang_category_has_items @ 该类别下还有内容，请先删除该类别下的数据。
rf_lang_essay_title_validate @ 长度介于1~255个字符
rf_lang_essay_title_exists @ 标题已存在
rf_lang_essay_title @ 标题
rf_lang_essay_catid_validate @ 分类不能为空
rf_lang_essay_excerpt @ 摘要
rf_lang_essay_keywords @ 关键词
rf_lang_essay_thumbnail_insert @ 设为缩略图
rf_lang_essay_thumbnail_remove @ 移除缩略图
rf_lang_essay_thumbnail_tips @ 从当前编辑器的"媒体库"选取即可
rf_lang_essay_insert_mainbody @ 将图片插入正文
rf_lang_essay_mainbody @ 正文
rf_lang_essay_username @ 发布人
rf_lang_essay_hits @ 打开次数
rf_lang_media_filename_exists @ 文件名称已存在
rf_lang_media_medianame_validate @ 长度介于1~255个字符
rf_lang_media_filename_validate @ 必须为字母、数字、_或-，只能以字母打头，不超过100个字符
rf_lang_media_medianame @ 文件名称（呈现于网页）
rf_lang_media_filename @ 文件名称（用于URL）
rf_lang_media_filename_pinyin @ 试试把中文名称转为拼音
rf_lang_media_alt @ 图片ALT描述
rf_lang_media_alt_validate @ 长度不超过255个字符
rf_lang_media_excerpt @ 简介
rf_lang_media_excerpt_validate @ 简介长度不超过255个字符
rf_lang_media_keywords @ 关键词
rf_lang_media_ext @ 文件类型
rf_lang_media_filesize @ 文件大小
rf_lang_media_fullscale @ 原始文件
rf_lang_media_thumbnail @ 缩略图
rf_lang_media_thumbnail_caution @ 只有图片才能选做缩略图
rf_lang_media_thumbnail_tips @ 在图像无法显示或者用户禁用图像显示时，代替图像显示在浏览器中的内容。
rf_lang_media_thumbnail_tips_2 @ 如果不传缩略图，系统会根据大图等比例呈现。
rf_lang_media_filesize_validate @ 上传文件超过了允许的最大字节数
rf_lang_media_filesize_IIS_validate @ IIS对上传文件大小限制默认为200KB，尽管你调整了config.asp中的 RF_UPLOAD_FILESIZE。<br/>请调整IIS相应设置，把“最大请求实体主体限制”值修改为 2000000
rf_lang_media_filetype_validate @ 上传文件类型不被允许
rf_lang_media_unselected @ 你没有选取图片吧
rf_lang_friendlink_catid_validate @ 分类不能为空
rf_lang_friendlink_linkname_validate @ 长度介于1~50个字符
rf_lang_friendlink_linkname_exists @ 链接名称已存在
rf_lang_friendlink_linkname @ 链接名称
rf_lang_friendlink_linkurl @ 链接地址
rf_lang_friendlink_fontcolor @ 文字颜色
rf_lang_friendlink_fontcolor_default @ 默认
rf_lang_friendlink_fontcolor_red @ 红色
rf_lang_friendlink_fontcolor_blue @ 蓝色
rf_lang_friendlink_fontcolor_grey @ 灰色
rf_lang_presentation @ 呈现方式
rf_lang_presentation_prename @ 名称
rf_lang_presentation_prename_validate @ 长度介于1~50个字符
rf_lang_presentation_prename_validate_copy @ [复制]功能会在原名称后面追加随机字符，因此可能导致新名称超出50个字符而报错。
rf_lang_presentation_prename_exists @ 该名称已经存在
rf_lang_presentation_prename_exists_copy @ 太巧合了，追加了随机字符的名称居然存在了，请重新尝试
rf_lang_do_you_copy @ [复制]功能帮助你快速加工已有呈现方式。
rf_lang_presentation_codehtml @ 代码
rf_lang_presentation_pretype @ 类型
rf_lang_presentation_official @ 官方
rf_lang_presentation_logic @ 逻辑语句
rf_lang_presentation_modifier @ 变量修饰器
rf_lang_presentation_pretype_unknow @ 未知的呈现方式分类
rf_lang_presentation_pretype_validate @ 分类不能为空
rf_lang_presentation_pretype_essay_list @ 文章列表
rf_lang_presentation_pretype_essay_info @ 文章详情
rf_lang_presentation_pretype_photo_list @ 图片列表
rf_lang_presentation_pretype_photo_info @ 图片详情
rf_lang_presentation_pretype_singlepage @ 单页
rf_lang_presentation_pretype_breadcrumb @ 面包屑
rf_lang_presentation_pretype_mainnav @ 一级导航
rf_lang_presentation_pretype_subnav @ 二级导航
rf_lang_presentation_pretype_friendlink @ 友情链接
rf_lang_presentation_pretype_photo_flexslider @ 轮播图
rf_lang_partial @ 区块
rf_lang_partial_is_empty @ 区块不能为空
rf_lang_presentation_is_empty @ [呈现方式]为空、或没有选中！
rf_lang_pagination @ 分页
rf_lang_pagination_tag @ 相应的分页标签
rf_lang_pagination_validate @ 缺少分页呈现方式
rf_lang_pagination_page_number_big  @ 带页码（大）
rf_lang_pagination_page_number_normal @ 带页码（中）
rf_lang_pagination_page_number_small @ 带页码（小）
rf_lang_pagination_page_turning_normal @ 翻页（挨着的）
rf_lang_pagination_page_turning_align @ 翻页（两端对齐）
rf_lang_tpl_common @ 通用
rf_lang_tpl_custom @ 自定义
rf_lang_tpl_filename @ 文件名称
rf_lang_tpl_filestr @ 文件内容
rf_lang_tpl_filetype_validate @ 模板文件必须为文本文件，允许的类型有：
rf_lang_tpl_common_file_miss @ 找不到common.asp
rf_lang_tpl_info_file_miss @ 你要删除的模板文件本不存在！
rf_lang_tpl_file_miss @ 你要删除的模板文件本不存在！
rf_lang_tpl_deleted_files @ 伪删除的模板文件
rf_lang_tpl_deleted_files_desc @ 这些文件的来源有两种途径：1）栏目，从“频道页”切换为“功能页”时，先前以“menu”打头的模板文件就被弃之不用了；2）“文章列表”或“图片列表”标签设置时，就详情页模板从“自定义”切换为“通用”时，先前以“info”打头的模板文件就到了这里。
rf_lang_tpl_menu_files @ 频道页栏目类型的模板
rf_lang_tpl_menu_files_desc @ 当栏目类型是“频道页”时，相应地就会多出一个模板。注意：如果状态显示“不对劲儿”、而非“良好”，很可能对应的栏目不需要这个模板文件，应该是手动放在模板文件夹造成的。
rf_lang_tpl_info_files @ 文章/图片详情页所指定的模板
rf_lang_tpl_info_files_desc @ 对于文章/图片列表类型的标签，如果其文章/图片详情页的模板设定为“自定义”时，相应地就会多出一个模板。文件名称中的数字，就是该标签的ID。注意：如果状态显示“不对劲儿”、而非“良好”，很可能对应的栏目不需要这个模板文件，应该是手动放在模板文件夹造成的。
rf_lang_tpl_index_asp @ 首页模板文件
rf_lang_tpl_index_asp_desc @ RefineCMS 约定用这个文件作为首页模板。注意，本文件夹下还有个 index.html，这是为了防止 IIS 中“目录浏览”功能未关闭时，而默认显示当前文件夹下的文件清单。建议：1）关闭 IIS 中的“目录浏览”功能；2）确认“index.html”存在。
rf_lang_tpl_index_html @ 屏蔽 IIS “目录浏览”功能的文件
rf_lang_tpl_index_html_desc @ 如上所述，这是为了防止 IIS 中“目录浏览”功能未关闭时，而默认显示当前文件夹下的文件清单。RefineCMS 几乎每个目录下都有这个文件。建议：1）关闭 IIS 中的“目录浏览”功能；2）确认“index.html”存在。
rf_lang_tpl_common_html @ 通用模板文件
rf_lang_tpl_common_html_desc @ 当栏目不是“频道页”、详情页不是“自定义”时，RefineCMS 就会把 common.asp 作为兜底模板。
rf_lang_tpl_error @ 系统出错页
rf_lang_tpl_error_desc @ 在生产环境（ENV_DEBUG = False）时，RefineCMS 出错会跳转到 404 页面；在开发环境（ENV_DEBUG = True）时，才会跳转到该页面。
rf_lang_tpl_404 @ 404 网页
rf_lang_tpl_404_desc @ 用户访问一个不存在网页时，所出现的网页。
rf_lang_tpl_unknown_files @ 不属于 RefineCMS 所使用的文件
rf_lang_tpl_unknown_files_desc @ 下面的文件可能是你手动上传的、临时存放的，总之，RefineCMS 没有使用它们。不过，它们的存在可能对你有用，RefineCMS 支持编辑功能。
rf_lang_tpl_unknown_folders @ 不属于 RefineCMS 所使用的文件夹
rf_lang_tpl_unknown_folders_desc @ RefineCMS 不再提供浏览子文件夹的功能了，你自行处理吧

rf_lang_tpl_commontag_tips @ 你看到这行字，是为了提醒：该模板来自于common.asp，你可以自己添加标签哟
rf_lang_tag_tagname @ 标签名称
rf_lang_tag_tagtext @ 生成的标签
rf_lang_tag_tagtext_parse_error @ 标签解析失败
rf_lang_tag_tagname_validate @ 长度介于1~50个字符，且不能带有"+"
rf_lang_tag_tagname_exists @ 该名称已经存在
rf_lang_tag @ 标签
rf_lang_tag_system @ 静态标签
rf_lang_tpl @ 模板
rf_lang_tpl_validate @ 模板未正确选取
rf_lang_tag_menu_associate @ 详情页的栏目归属
rf_lang_tag_menu_is_empty @ 详情页的栏目归属为空、或没有选中！
rf_lang_content_from @ 内容来源
rf_lang_content_from_is_empty @ [内容管理]为空、或没有选中！
rf_lang_tag_listnum @ 条数/每页条数
rf_lang_tag_listnum_validate @ 条数/每页条数必须为正整数
rf_lang_tag_wordnum @ 每条字数
rf_lang_tag_wordnum_validate @ 每条字数必须为正整数
rf_lang_tag_order @ 排序方式
rf_lang_tag_order_validate @ 排序方式的值有误
rf_lang_tag_order_id_desc @ 最新的靠前
rf_lang_tag_order_id_asc @ 最新的靠后
rf_lang_tag_order_id_hits @ 文章打开次数
rf_lang_menu @ 栏目
rf_lang_menu_parentmenu @ 父级栏目
rf_lang_menu_parentmenu_can_not_find @ 找不到对应的父级栏目
rf_lang_menu_top @ 顶级
rf_lang_menu_menuname @ 栏目名称
rf_lang_menu_menuname_validate @ 长度介于1~20个字符
rf_lang_menu_menuname_exists @ 栏目名称已存在
rf_lang_menu_type @ 栏目类型
rf_lang_menu_excerpt @ 摘要
rf_lang_menu_keywords @ 关键词
rf_lang_menu_type_func @ 功能页
rf_lang_menu_type_channel @ 频道页
rf_lang_menu_type_error @ 错误的栏目类型
rf_lang_sys_iis_version @ IIS版本
rf_lang_sys_ScriptTimeout @ 脚本超时时间
rf_lang_sys_ScriptEngine @ 脚本引擎
rf_lang_sys_OS @ 操作系统
rf_lang_sys_NUMBER_OF_PROCESSORS @ CPU通道数
rf_lang_sys_PROCESSOR_IDENTIFIER @ CPU详情
rf_lang_sys_Application_number @ Application 变量个数
rf_lang_sys_Session_number @ Session 变量个数
rf_lang_sys_lang_cache @ 语言包缓存
rf_lang_sys_other_cache @ 其它缓存
rf_lang_sys_tab_profile @ 总览
rf_lang_sys_tab_server_info @ 服务器信息
rf_lang_sys_tab_components @ 组件
rf_lang_sys_tab_application @ Application/缓存
rf_lang_sys_tab_session @ Session
rf_lang_sys_page_cache @ 页面缓存


#refineCMS 系统相关
rf_lang_timewatcher_list_is_empty  @ Timer Watcher 没有记录！
rf_lang_timewatcher_panel_title_DB @ 数据库访问统计
rf_lang_timewatcher_panel_title_DB_detail @ 数据库访问清单
rf_lang_timewatcher_pannel_title_other @ 其它事件清单：
rf_lang_timewatcher_event_title @ 事件描述：
rf_lang_timewatcher_event_time @ 耗时（ms）：
rf_lang_timewatcher_DbTotalHits @ 数据库访问总次数：
rf_lang_timewatcher_DbTotalTime @ 数据库访问总耗时（ms）：
rf_lang_stack_pop_no_item @ 栈为空，pop失败，请确认
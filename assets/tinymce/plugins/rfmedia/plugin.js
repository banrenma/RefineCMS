tinymce.PluginManager.add('rfmedia', function(editor, url) {

    // Adds a menu item to the tools menu
    editor.addButton('rfmedia', {
        text: '图片库',
        context: 'tools',
        onclick: function() {
            // Open window with a specific url
            editor.windowManager.open({
                    title: '可供选择的图片',
                    url: url.replace("assets/tinymce/plugins/rfmedia", "") + "?action=backend&module=ds.photos.insert",
                    width: '1000',
                    height: 600
                }, {
                    ed: editor
                }
            );
        }
    });
});

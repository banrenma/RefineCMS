<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Singlepage
    Public iID, sTitle, sUsername, sExcerpt, sMainbody, sFilepath, sResolution, dCreatedAt, dUpdatedAt, sKeywords
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        clearObj
    End Sub

    Private Sub class_terminate
    End Sub

    Private Sub clearObj
        ' on error resume next
        iID = 0
        sTitle = ""
        sExcerpt = ""
        sKeywords = ""
        sMainbody = ""
        sFilepath = ""
        sResolution = ""
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    Private Function validateTitle()
        validateTitle = False
        If Not rfIsEmpty(sTitle) And rfStrLen(sTitle) <= 255 Then
            validateTitle = True
        End If
    End Function

    Private Function titleExists
        titleExists = False
        Dim rs, sql
        sql = "SELECT id FROM `singlepages` WHERE title='" & rfCleanSingleQuotation(sTitle) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                titleExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `singlepages` WHERE id=" & id
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sTitle = rs("title")
            sUsername = rs("username")
            sExcerpt = rs("excerpt")
            sKeywords = rs("keywords")
            sMainbody = rs("mainbody")
            sFilepath = rs("filepath")
            sResolution = rs("resolution")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
        Else
            clearObj
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Function save()
        If Not validateTitle() Then
            save = T("rf_lang_essay_title_validate")
            Exit Function
        End If
        
        If titleExists Then
            save = T("rf_lang_essay_title_exists")
            Exit Function
        End If
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT * FROM singlepages WHERE id=" & iID
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT * FROM singlepages WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
            rs("created_at") = Now()
        End If
        rfEndWatchWithDB(sql)
        rs("title") = sTitle
        rs("username") = Session("rf_user_username")
        rs("excerpt") = sExcerpt
        rs("keywords") = sKeywords
        rs("mainbody") = sMainbody
        rs("filepath") = sFilepath
        rs("resolution") = sResolution
        rs("updated_at") = Now()
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.singlepages:update-save()")
        save = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function list()
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT ID, title, username, excerpt, mainbody, filepath, updated_at, created_at, resolution, keywords"
        sql = sql & " FROM singlepages"
        sql = sql & " ORDER BY ID DESC"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Function listByOrder(param_showorder)
        listByOrder = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT ID, title, username, excerpt, mainbody, filepath, updated_at, created_at, resolution, keywords"
        sql = sql & " FROM singlepages"
        sql = sql & " ORDER BY " & fmtOrderby(param_showorder)
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            listByOrder = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Private Function fmtOrderby(param_val)
        Select Case param_val
            Case "asc"
                fmtOrderby = " ID ASC"
            Case Else
                fmtOrderby = " ID DESC"
        End Select
    End Function

    Public Sub del
        Dim sql
        sql = "DELETE FROM `singlepages` WHERE id=" & rfConvertInt(iID)
        rfStartWatch()
        g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
    End Sub

    Public Function getItemsCount()
        getItemsCount = 0
        Dim rs, sql
        sql = "SELECT count(*) FROM `singlepages`"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            getItemsCount = rs(0)
        End If
        rs.Close
        Set rs = Nothing
    End Function


End Class

%>
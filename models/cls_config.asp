<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Config
    Public sConfigKey, sConfigText

    Private Sub Class_Initialize
        ' on error resume next
        sConfigKey = ""
        sConfigText = ""
    End Sub

    Private Sub class_terminate
    End Sub

    Public Sub init(param_configKey)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT config_key, config_text FROM `configs` WHERE config_key='" &param_configKey & "'"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            sConfigKey = rs("config_key")
            sConfigText = rs("config_text")
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Function save()
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If Not rfIsEmpty(sConfigKey) Then
            sql = "SELECT config_key, config_text FROM `configs` WHERE config_key='" &sConfigKey & "'"
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT config_key, config_text FROM `configs`  WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
        End If
        rfEndWatchWithDB(sql)
        rs("config_key") = sConfigKey
        rs("config_text") = sConfigText
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.configs:update-save()")
        rs.Close
        save = True
        Set rs = Nothing
    End Function

End Class

%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Presentation
    Public iID, sPreName, iPreType, sCodehtml, iOfficial, sRemark, sUsername, dCreatedAt, dUpdatedAt
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        clearObj
    End Sub

    Private Sub class_terminate
    End Sub

    Private Sub clearObj
        ' on error resume next
        iID = 0
        sPreName = ""
        iPreType = 0
        sCodehtml = ""
        iOfficial = 0
        sRemark = ""
        sUsername = ""
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    Private Function validatePrename()
        validatePrename = False
        If Not rfIsEmpty(sPreName) And rfStrLen(sPreName) <= 50 Then
            validatePrename = True
        End If
    End Function

    Private Function prenameExists
        prenameExists = False
        Dim rs, sql
        sql = "SELECT id FROM `presentations` WHERE pre_name='" & rfCleanSingleQuotation(sPreName) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                prenameExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `presentations` WHERE id=" & rfConvertInt(id)
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sPreName = rs("pre_name")
            iPreType = rs("pre_type")
            sCodehtml = rs("codehtml")
            iOfficial = rs("official")
            sRemark = rs("remark")
            sUsername = rs("username")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
        Else
            clearObj
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Function save()
        If Not validatePrename() Then
            save = T("rf_lang_presentation_prename_validate")
            Exit Function
        End If
        If prenameExists Then
            save = T("rf_lang_presentation_prename_exists")
            Exit Function
        End If
        If iPreType <= 0 Then
            save = T("rf_lang_presentation_pretype_validate")
            Exit Function
        End If
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT * FROM presentations WHERE id=" & rfConvertInt(iID)
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT * FROM presentations WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
            rs("created_at") = Now()
        End If
        rfEndWatchWithDB(sql)
        rs("pre_name") = sPreName
        rs("pre_type") = iPreType
        rs("codehtml") = sCodehtml
        rs("official") = 2 '写死为“非官方”。
        rs("remark") = sRemark
        rs("username") = Session("rf_user_username")
        rs("updated_at") = Now()
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.presentations:update-save()")
        save = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function list(param_preType)
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `presentations` "
        If param_preType > 0 Then
            sql = sql & " WHERE pre_type=" & rfConvertInt(param_preType)
        End If
        sql = sql & " ORDER BY ID DESC"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function


    Public Sub del
        Dim sql
        sql = "DELETE FROM `presentations` WHERE id=" & rfConvertInt(iID)
        rfStartWatch()
        g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
    End Sub

    Public Function copy()
        init iID
        sPreName = sPreName & "_" & LCase(rfGenerateHash)
        If Not validatePrename() Then
            copy = T("rf_lang_presentation_prename_validate_copy")
            Exit Function
        End If
        If prenameExists Then
            copy = T("rf_lang_presentation_prename_exists_copy")
            Exit Function
        End If
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM presentations WHERE 1=2"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 3
        rfEndWatchWithDB(sql)
        rs.AddNew
        rs("created_at") = Now()
        rs("pre_name") = sPreName
        rs("pre_type") = iPreType
        rs("codehtml") = sCodehtml
        rs("official") = 2 '写死为“非官方”。
        rs("remark") = sRemark
        rs("username") = Session("rf_user_username")
        rs("updated_at") = Now()
        rs.Update
        copy = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function getItemsCount(param_preType)
        getItemsCount = 0
        Dim rs, sql
        sql = "SELECT count(*) FROM `presentations`"
        If param_preType > 0 Then
            sql = sql & " WHERE pre_type=" & rfConvertInt(param_preType)
        End If
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            getItemsCount = rs(0) 
        End If
        rs.Close
        Set rs = Nothing
    End Function

End Class

%>
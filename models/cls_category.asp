<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Category
    Public iID, sCatName, iCatType
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        clearObj
    End Sub

    Private Sub clearObj
        ' on error resume next
        iID = 0
        sCatName = ""
        iCatType = 0
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Private Sub class_terminate
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    Private Function validateCatname()
        validateCatname = False
        If Not rfIsEmpty(sCatName) And rfStrLen(sCatName) <= 30 Then
            validateCatname = True
        End If
    End Function

    Private Function catnameExists
        catnameExists = False
        Dim rs, sql
        sql = "SELECT id FROM `categories` WHERE cat_name='" & rfCleanSingleQuotation(sCatName) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                catnameExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `categories` WHERE id=" & rfConvertInt(id)
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sCatName = rs("cat_name")
            iCatType = rs("cat_type")
        Else
            clearObj
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Function save()
        If Not validateCatname() Then
            save = T("rf_lang_category_catname_validate")
            Exit Function
        End If
        If catnameExists Then
            save = T("rf_lang_category_catname_exists")
            Exit Function
        End If
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT * FROM categories WHERE id=" & rfConvertInt(iID)
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT * FROM categories WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
        End If
        rfEndWatchWithDB(sql)
        rs("cat_name") = sCatName
        rs("cat_type") = iCatType
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.categories:update-save()")
        save = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function list()
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `categories` ORDER BY ID DESC"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Function listByType(param_catType)
        listByType = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `categories` WHERE cat_type=" & rfConvertInt(param_catType) & " ORDER BY ID DESC"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            listByType = rs.GetRows()
        End If
        rs.Close
        Set rs = Nothing
    End Function


    Public Sub del
        If itemCountByCatID(iID) > 0 Then
            rfShowError T("rf_lang_operate_fail"), T("rf_lang_category_has_items")
        Else
            Dim sql
            sql = "DELETE FROM `categories` WHERE id=" & rfConvertInt(iID)
            rfStartWatch()
            g_conn.Execute(sql)
            rfEndWatchWithDB(sql)
        End If
    End Sub

    Private Function itemCountByCatID(param_id)
        itemCountByCatID = 0
        init param_id
        If iID > 0 Then
            Select Case iCatType
                Case 1
                    Dim essay
                    Set essay = new cls_Essay
                    itemCountByCatID = essay.getItemsCount(iID)
                    Set essay = Nothing
                Case 2
                    Dim media
                    Set media = new cls_Media
                    itemCountByCatID = media.getItemsCount(iID)
                    Set media = Nothing
                Case 3
                    Dim friendlink
                    Set friendlink = new cls_Friendlink
                    itemCountByCatID = friendlink.getItemsCount(iID)
                    Set friendlink = Nothing
            End Select
        End If
    End Function

End Class

%>
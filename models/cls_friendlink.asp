<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Friendlink
    Public iID, sLinkname, iCatId, sLinkurl, iFontcolor, sResolution, sFilepath, sUsername, dCreatedAt, dUpdatedAt
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        iID = 0
        sLinkname = ""
        iCatId = 0
        sLinkurl = ""
        iFontcolor = 0
        sResolution = ""
        sFilepath = ""
        sUsername = ""
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Private Sub class_terminate
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property
    Private Function validateCatId()
        validateCatId = False
        If iCatId > 0 Then
            validateCatId = True
        End If
    End Function
    Public Function validateLinkname()
        validateLinkname = False
        If Not rfIsEmpty(sLinkname) And rfStrLen(sLinkname) <= 50 Then
            validateLinkname = True
        End If
    End Function

    Public Function linknameExists
        linknameExists = False
        Dim rs, sql
        sql = "SELECT id FROM `friendlinks` WHERE linkname='" & rfCleanSingleQuotation(sLinkname) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                linknameExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT * FROM `friendlinks` WHERE id=" & id
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sLinkname = rs("linkname")
            iCatId = rs("cat_id")
            sLinkurl = rs("linkurl")
            iFontcolor = rs("fontcolor")
            sResolution = rs("resolution")
            sFilepath = rs("filepath")
            sUsername = rs("username")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
        End If
        rs.Close
        Set rs = Nothing
    End Sub
    Private Sub clearTmpFilepath
        rfRemoveFile(sFilepath)
        sFilepath = ""
        sResolution = ""
    End Sub

    Public Function save()
        If iCatId <= 0 Then
            clearTmpFilepath
            save = T("rf_lang_friendlink_catid_validate")
            Exit Function
        End If
        If Not validateLinkname() Then
            clearTmpFilepath
            save = T("rf_lang_friendlink_linkname_validate")
            Exit Function
        End If
        If linknameExists Then
            clearTmpFilepath
            save = T("rf_lang_friendlink_linkname_exists")
            Exit Function
        End If
        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT * FROM friendlinks WHERE id=" & iID
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT * FROM friendlinks WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
            rs("created_at") = Now()
        End If
        rfEndWatchWithDB(sql)
        rs("linkname") = sLinkname
        rs("cat_id") = iCatId
        rs("linkurl") = sLinkurl
        rs("fontcolor") = iFontcolor
        rs("resolution") = sResolution
        If Not rfIsEmpty(rs("filepath")) And rs("filepath") <> sFilepath Then
            rfRemoveFile(rs("filepath"))'''''''''(1)
        End If
        rs("filepath") = sFilepath '''''''''(2)
        rs("username") = Session("rf_user_username")
        rs("updated_at") = Now()
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.friendlinks:update-save()")
        save = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function list(param_CatId)
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT f.ID, f.linkname, f.linkurl, f.cat_id, c.cat_name, f.fontcolor, f.resolution, f.filepath, f.username, f.updated_at, f.created_at"
        sql = sql & " FROM friendlinks f LEFT JOIN categories c ON c.id=f.cat_id"
        If param_CatId > 0 Then
            sql = sql & " WHERE f.cat_id=" & rfConvertInt(param_CatId)
        End If
        sql = sql & " ORDER BY f.ID DESC"
        ' rfExit sql
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub del
        Dim sql
        sql = "DELETE FROM `friendlinks` WHERE id=" & rfConvertInt(iID)
        rfStartWatch()
        g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rfIsEmpty(sFilepath) Then
            rfRemoveFile(sFilepath)
        End If
    End Sub

    Public Function getItemsCount(param_CatId)
        getItemsCount = 0
        Dim rs, sql
        sql = "SELECT count(*) FROM `friendlinks`"
        If param_CatId > 0 Then
            sql = sql & " WHERE cat_id=" & rfConvertInt(param_CatId)
        End If
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            getItemsCount = rs(0) 
        End If
        rs.Close
        Set rs = Nothing
    End Function


End Class

%>
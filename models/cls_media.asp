<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Media
    Public iID, sMedianame, sFilename, iCatId, sAlt, sExt, iFilesize, sResolution, sFilepath, sUsername, dCreatedAt, dUpdatedAt, sExcerpt, sKeywords
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        iID = 0
        sMedianame = ""
        sFilename = ""
        iCatId = 0
        sAlt = ""
        sExt = ""
        iFilesize = 0
        sResolution = ""
        sFilepath = ""
        sExcerpt = ""
        sKeywords = ""
        sUsername = ""
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Private Sub class_terminate
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    Public Function validateMedianame()
        validateMedianame = False
        If Not rfIsEmpty(sMedianame) And rfStrLen(sMedianame) <= 255 Then
            validateMedianame = True
        End If
    End Function
    Public Function validateFilename()
        validateFilename = False
        If Not rfIsEmpty(sFilename) And rfStrLen(sFilename) <= 100 Then
             Dim pattern, regEx
            pattern = "^[a-zA-Z][a-zA-Z0-9_-]{0,99}$"
            Set regEx = New RegExp
            regEx.Pattern = pattern
            regEx.IgnoreCase = True
            regEx.Global = True
            If regEx.Test(sFilename) Then
                validateFilename = True
            End If
            Set regEx = Nothing
        End If
    End Function

    Public Function filenameExists
        filenameExists = False
        Dim rs, sql
        sql = "SELECT id FROM `medias` WHERE filename='" & rfCleanSingleQuotation(sFilename) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                filenameExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT id, medianame, filename, cat_id, alt, ext, filesize, resolution, filepath, username, updated_at, created_at, excerpt, keywords FROM `medias` WHERE id=" & id
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sMedianame = rs("medianame")
            sFilename = rs("filename")
            iCatId = rs("cat_id")
            sAlt = rs("alt")
            sExt = rs("ext")
            iFilesize = rs("filesize")
            sResolution = rs("resolution")
            sFilepath = rs("filepath")
            sUsername = rs("username")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
            sExcerpt = rs("excerpt")
            sKeywords = rs("keywords")
        End If
        rs.Close
        Set rs = Nothing
    End Sub
    Private Sub clearTmpFilepath
        ' rfRemoveFile(sFilepath)
        sExt = ""
        iFilesize = 0
        sResolution = ""
        sFilepath = ""
    End Sub

    Public Function save()
        If iCatId <= 0 Then
            clearTmpFilepath
            save = T("rf_lang_catid_validate")
            Exit Function
        End If
        If Not validateMedianame() Then
            clearTmpFilepath
            save = T("rf_lang_media_medianame_validate")
            Exit Function
        End If
        If Not validateFilename() Then
            clearTmpFilepath
            save = T("rf_lang_media_filename_validate")
            Exit Function
        End If
        If filenameExists Then
            clearTmpFilepath
            save = T("rf_lang_media_filename_exists")
            Exit Function
        End If
        If rfStrLen(sAlt) > 255 Then
            save = T("rf_lang_media_alt") & T("rf_lang_media_alt_validate")
            Exit Function
        End If
        If rfStrLen(sExcerpt) > 255 Then
            save = T("rf_lang_media_excerpt") & T("rf_lang_media_excerpt_validate")
            Exit Function
        End If

        Dim rs, sql
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT * FROM medias WHERE id=" & iID
            rs.Open sql, g_conn, 1, 3
        Else
            sql = "SELECT * FROM medias WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
            rs("created_at") = Now()
        End If
        rfEndWatchWithDB(sql)
        rs("medianame") = sMedianame
        rs("filename") = sFilename
        rs("cat_id") = iCatId
        rs("alt") = sAlt
        rs("excerpt") = sExcerpt
        rs("keywords") = sKeywords
        If Not rfIsEmpty(rs("filepath")) And rs("filepath") <> sFilepath Then
            rfRemoveFile(rs("filepath"))'''''''''(1)
        End If
        rs("ext") = sExt
        rs("filesize") = iFilesize
        rs("resolution") = sResolution
        rs("filepath") = sFilepath'''''''''(2)
        rs("username") = Session("rf_user_username")
        rs("updated_at") = Now()
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.medias:update-save()")
        save = True
        rs.Close
        Set rs = Nothing
    End Function

    Public Function list(param_CatId)
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT m.ID, m.medianame, m.filename, m.cat_id, c.cat_name, m.alt, m.ext, m.filesize, m.resolution, m.filepath, m.username, m.updated_at, m.created_at, m.excerpt, m.keywords"
        sql = sql & " FROM medias m LEFT JOIN categories c ON c.id=m.cat_id"
        If param_CatId > 0 Then
            sql = sql & " WHERE m.cat_id=" & rfConvertInt(param_CatId)
        End If
        sql = sql & " ORDER BY m.ID DESC"
        ' rfExit sql
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Function listByOrder(param_CatId, param_showorder)
        listByOrder = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT m.ID, m.medianame, m.filename, m.cat_id, c.cat_name, m.alt, m.ext, m.filesize, m.resolution, m.filepath, m.username, m.updated_at, m.created_at, m.excerpt, m.keywords"
        sql = sql & " FROM medias m LEFT JOIN categories c ON c.id=m.cat_id"
        If param_CatId > 0 Then
            sql = sql & " WHERE m.cat_id=" & rfConvertInt(param_CatId)
        End If
        sql = sql & " ORDER BY " & fmtOrderby(param_showorder)
        ' rfExit sql
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            listByOrder = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Private Function fmtOrderby(param_val)
        Select Case param_val
            Case "asc"
                fmtOrderby = " m.ID ASC"
            Case Else
                fmtOrderby = " m.ID DESC"
        End Select
    End Function

    Public Sub del
        Dim sql
        sql = "DELETE FROM `medias` WHERE id=" & rfConvertInt(iID)
        rfStartWatch()
        g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rfIsEmpty(sFilepath) Then
            rfRemoveFile(sFilepath)
        End If
    End Sub

    Public Function getItemsCount(param_CatId)
        getItemsCount = 0
        Dim rs, sql
        sql = "SELECT count(*) FROM `medias`"
        If param_CatId > 0 Then
            sql = sql & " WHERE cat_id=" & rfConvertInt(param_CatId)
        End If
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            getItemsCount = rs(0) 
        End If
        rs.Close
        Set rs = Nothing
    End Function

End Class

%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Tag
    Public iID, sTagName, iTagType, sTagText, sUsername, dCreatedAt, dUpdatedAt, iInfotplFlag
    Private iPageSize, iAbsolutePage, iPageCount

    Private Sub Class_Initialize
        ' on error resume next
        clearObj
    End Sub

    Private Sub class_terminate
    End Sub

    Private Sub clearObj
        ' on error resume next
        iID = 0
        sTagName = ""
        iTagType = 0
        sTagText = ""
        sUsername = ""
        iInfotplFlag = 0
        iPageSize = 1000
        iAbsolutePage = 1
    End Sub

    Property Get PageCount()
        PageCount = iPageCount
    End Property

    Property Let PageSize(param_pageSize)
        iPageSize = param_pageSize
    End Property

    Property Get AbsolutePage()
        AbsolutePage = iAbsolutePage
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    Private Function validateTagname()
        validateTagname = False
        If Not rfIsEmpty(sTagName) And rfStrLen(sTagName) <= 50 And Not rfRegexTest(sTagName, "\+") Then
            validateTagname = True
        End If
    End Function

    Private Function tagnameExists
        tagnameExists = False
        Dim rs, sql
        sql = "SELECT id FROM `tags` WHERE tag_name='" & rfCleanSingleQuotation(sTagName) & "'"
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            If rs(0) <> iID Then
                tagnameExists = True
            End If    
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub init(id)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT id, tag_name, tag_type, tag_text, username, updated_at, created_at, infotpl_flag FROM `tags` WHERE id=" & rfConvertInt(id)
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = id
            sTagName = rs("tag_name")
            iTagType = rs("tag_type")
            sTagText = rs("tag_text")
            sUsername = rs("username")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
            iInfotplFlag =  rs("infotpl_flag")
        Else
            clearObj
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Sub init_by_tag_name(param_tagName)
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT id, tag_name, tag_type, tag_text, username, updated_at, created_at, infotpl_flag FROM `tags` WHERE tag_name='" & rfCleanInput(param_tagName) & "'"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            iID = rs("id")
            sTagName = rs("tag_name")
            iTagType = rs("tag_type")
            sTagText = rs("tag_text")
            sUsername = rs("username")
            dUpdatedAt = rs("updated_at")
            dCreatedAt = rs("created_at")
            iInfotplFlag =  rs("infotpl_flag")
        Else
            clearObj
        End If
        rs.Close
        Set rs = Nothing
    End Sub

    Public Function save()
        If Not validateTagname() Then
            save = T("rf_lang_tag_tagname_validate")
            Exit Function
        End If
        If tagnameExists Then
            save = T("rf_lang_tag_tagname_exists")
            Exit Function
        End If
        Dim rs, sql
        Dim flagOld
        Set rs = Server.CreateObject("ADODB.recordset")
        rfStartWatch()
        If iID > 0 Then
            sql = "SELECT id, tag_name, tag_type, tag_text, username, updated_at, created_at, infotpl_flag FROM tags WHERE id=" & rfConvertInt(iID)
            rs.Open sql, g_conn, 1, 3
            flagOld = rs("infotpl_flag")
        Else
            sql = "SELECT id, tag_name, tag_type, tag_text, username, updated_at, created_at, infotpl_flag FROM tags WHERE 1=2"
            rs.Open sql, g_conn, 1, 3
            rs.AddNew
            rs("created_at") = Now()
            flagOld = 0
        End If
        rfEndWatchWithDB(sql)
        rs("tag_name") = sTagName
        rs("tag_type") = iTagType
        rs("tag_text") = sTagText
        rs("username") = Session("rf_user_username")
        rs("updated_at") = Now()
        rs("infotpl_flag") = iInfotplFlag
        rfStartWatch()
        rs.Update
        rfEndWatchWithDB("ADO: table.tags:update-save()")
        rfStartWatch()
        save = setTpl(flagOld, iInfotplFlag, rs) '处理文章详情或图片详情的模板
        rfEndWatch("生成或删除列表标签所使用的模板")
        rs.Close
        Set rs = Nothing
    End Function

    Private Function getFilenameOfDel(id)
        Dim infoTpl_del, d, curTime
        d = Now
        curTime = Year(d) & Month(d) & Day(d) & Hour(d) & Minute(d) & Second(d)
        getFilenameOfDel = RF_TEMPLATE_FOLDER_FRONTEND & "info_" & id & "_deleted_" & curTime & ".asp"
    End Function

    Public Function getFilenameOfSource(id)
        getFilenameOfSource = RF_TEMPLATE_FOLDER_FRONTEND & "info_" & id & ".asp"
    End Function

    Private Function deleteInfoTpl(id)
        Dim infoTpl, infoTpl_del, fs
        infoTpl = getFilenameOfSource(id)
        infoTpl_del = getFilenameOfDel(id)
        Set fs = Server.CreateObject("Scripting.FileSystemObject")
        If fs.FileExists(Server.MapPath(infoTpl)) Then
                fs.MoveFile Server.MapPath(infoTpl), Server.MapPath(infoTpl_del)
        End If
        Set fs = Nothing
    End Function

    Private Function setTpl(flagOld, iInfotplFlag, rs)
        setTpl = True
        Dim infoTpl, infoTpl_del, commonTpl
        commonTpl = RF_TEMPLATE_FOLDER_FRONTEND & "common.asp"
        infoTpl = getFilenameOfSource(rs("id"))
        infoTpl_del = getFilenameOfDel(rs("id"))
        Dim fs
        Set fs = Server.CreateObject("Scripting.FileSystemObject")
        If (CInt(flagOld)=0 And CInt(iInfotplFlag)=2) Or (CInt(flagOld)=1 And CInt(iInfotplFlag)=2) Then
             If fs.FileExists(Server.MapPath(commonTpl)) Then
                fs.CopyFile Server.MapPath(commonTpl), Server.MapPath(infoTpl)
            Else
                   setTpl = T("rf_lang_tpl_common_file_miss")
            End If
        End If
        If CInt(flagOld)=2 And CInt(iInfotplFlag)=1 Then
            If fs.FileExists(Server.MapPath(infoTpl)) Then
                fs.MoveFile Server.MapPath(infoTpl), Server.MapPath(infoTpl_del)
            Else
                   setTpl = T("rf_lang_tpl_info_file_miss") & infoTpl
            End If
        End If
        Set fs = Nothing
    End Function

    Public Function list(param_tagType)
        list = null
        Dim rs, sql
        Set rs=Server.CreateObject("ADODB.recordset")
        sql = "SELECT id, tag_name, tag_type, tag_text, username, updated_at, created_at, infotpl_flag FROM `tags` "
        If param_tagType > 0 Then
            sql = sql & " WHERE tag_type=" & rfConvertInt(param_tagType)
        End If
        sql = sql & " ORDER BY ID DESC"
        rfStartWatch()
        rs.Open sql, g_conn, 1, 1
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            rs.PageSize = iPageSize
            rs.AbsolutePage = fmtPaginationAbsolutePage(iAbsolutePage, rs.PageCount)
            iPageCount = rs.PageCount
            iAbsolutePage = rs.AbsolutePage
            list = rs.GetRows(rs.PageSize)
        End If
        rs.Close
        Set rs = Nothing
    End Function

    Public Sub del
        Dim sql
        sql = "DELETE FROM `tags` WHERE id=" & rfConvertInt(iID)
        rfStartWatch()
        g_conn.Execute(sql)
        rfEndWatchWithDB(sql)

        rfStartWatch()
        deleteInfoTpl(rfConvertInt(iID)) '无脑伪删除
        rfEndWatch("无脑伪删除列表标签所使用的模板")
    End Sub

    Public Function getItemsCount(param_tagType)
        getItemsCount = 0
        Dim rs, sql
        sql = "SELECT count(*) FROM `tags`"
        If param_tagType > 0 Then
            sql = sql & " WHERE tag_type=" & rfConvertInt(param_tagType)
        End If
        rfStartWatch()
        Set rs = g_conn.Execute(sql)
        rfEndWatchWithDB(sql)
        If Not rs.EOF Then
            getItemsCount = rs(0) 
        End If
        rs.Close
        Set rs = Nothing
    End Function


End Class

%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_TimeWatcher

    Private Sub Class_Initialize
        ' on error resume next
        Session("rf_timewatcher_iDbTotalHits") = 0
        Session("rf_timewatcher_iDbTotalTime") = 0 'ms
    End Sub

    Private Sub class_terminate

    End Sub

    Public Sub clearLastLog
        Session.Contents.Remove("rf_timewatcher_iDbTotalHits")
        Session.Contents.Remove("rf_timewatcher_iDbTotalTime")
        Session.Contents.Remove("rf_timewatcher_log_other")
        Session.Contents.Remove("rf_timewatcher_log_db")
    End Sub

    Property Get DbTotalHits()
        DbTotalHits = rfConvertInt(Session("rf_timewatcher_iDbTotalHits"))
    End Property

    Property Get DbTotalTime()
        DbTotalTime = rfConvertInt(Session("rf_timewatcher_iDbTotalTime"))
    End Property

    Public Sub addDbItem(queryTime)
        Session("rf_timewatcher_iDbTotalHits") = rfConvertInt(Session("rf_timewatcher_iDbTotalHits")) + 1
        Session("rf_timewatcher_iDbTotalTime") = rfConvertInt(Session("rf_timewatcher_iDbTotalTime")) + queryTime
    End Sub

    Private Sub appendSession(sessionKey, eventDesc, queryTime)
        Session(sessionKey) = rfConvertStr(Session(sessionKey)) & eventDesc & "|" & queryTime & "@"
    End Sub

    Public Sub addWatcherItem(eventDesc, queryTime)
        appendSession "rf_timewatcher_log_other", eventDesc, queryTime
    End Sub

    Public Sub addWatcherItemDB(eventDesc, queryTime)
        appendSession "rf_timewatcher_log_db", eventDesc, queryTime
    End Sub

    Private Function getEventListHtml(listName, listLang)
        Dim result:result = ""
        Dim sesstionEventStr, sessionEventArr, sessionEventCount, eventKeyValue, i
        sesstionEventStr = rfConvertStr(Session(listName))
        sessionEventArr = Split(sesstionEventStr, "@", -1, 1)
        sessionEventCount = Ubound(sessionEventArr)
        If sessionEventCount > 0 Then
            result = result &"<div class=""panel panel-warning"">"
            result = result &"<div class=""panel-heading"">"
            result = result &"<h3 class=""panel-title"">" & listLang & "</h3>"
            result = result &"</div>"
            result = result &"<div class=""panel-body"">"
            For i=0 to sessionEventCount
                If Not rfIsEmpty(sessionEventArr(i)) Then
                    eventKeyValue = Split(sessionEventArr(i), "|", -1, 1)
                    result = result &"<p>" & T("rf_lang_timewatcher_event_title") & eventKeyValue(0) & ", "
                    result = result & T("rf_lang_timewatcher_event_time") & eventKeyValue(1) & "</p>"
                End If
            Next
            result = result &"</div>"
            result = result &"</div>"
        End If
            getEventListHtml = result
    End Function

    Public Function showSimpleInfo
        ' 存在session中的日志：页面载入总耗时|8@
        showSimpleInfo =  rfReplace(rfReplace(rfConvertStr(Session("rf_timewatcher_log_other")), "|", ": "), "@", "ms")
    End Function

    Public Function showWatcherList()
        rfEndWatch("页面载入总耗时") ' 与之对应的开始位置在 \init.asp。
        Dim result:result = "<div class=""row""><div class=""col-md-10 col-md-offset-1"">"
        If rfConvertInt(Session("rf_timewatcher_iDbTotalHits"))  > 0 Then
            result = result &"<div class=""panel panel-danger"">"
            result = result &"<div class=""panel-heading"">"
            result = result &"<h3 class=""panel-title"">" & T("rf_lang_timewatcher_panel_title_DB") & "</h3>"
            result = result &"</div>"
            result = result &"<div class=""panel-body"">"
            result = result &"<p>" & T("rf_lang_timewatcher_DbTotalHits") & rfConvertInt(Session("rf_timewatcher_iDbTotalHits")) & "</p>"
            result = result &"<p>" & T("rf_lang_timewatcher_DbTotalTime") & rfConvertInt(Session("rf_timewatcher_iDbTotalTime")) & "</p>"
            result = result &"</div>"
            result = result &"</div>"
            result = result &"</div></div>"
        End If
        result = result &"<div class=""row""><div class=""col-md-10 col-md-offset-1"">"
        result = result & getEventListHtml("rf_timewatcher_log_other", T("rf_lang_timewatcher_pannel_title_other"))
        result = result &"</div></div>"

        result = result &"<div class=""row""><div class=""col-md-10 col-md-offset-1"">"
        result = result & getEventListHtml("rf_timewatcher_log_db", T("rf_lang_timewatcher_panel_title_DB_detail"))
        result = result &"</div></div>"

        showWatcherList = result
    End Function


    


End Class
%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Stack
    Private arrayStack()

    Private Sub Class_Initialize
        ' on error resume next
        Redim Preserve arrayStack(0)
        arrayStack(0) = 1 '保持至少1个元素，便于避免Ubound下标越界的错误 
    End Sub

    Private Sub class_terminate
    End Sub

    Property Get FullArrayStack
        FullArrayStack = arrayStack
    End Property

    Public Function pop
        Dim index, result
        index = Ubound(arrayStack)
        If index = 0 Then
            rfExit(T("rf_lang_stack_pop_no_item"))
        Else
            result = arrayStack(index)
            Redim Preserve arrayStack(index-1)
            pop = result
        End If
    End Function

    Public Function push(arrItem)
        Dim index
        index = Ubound(arrayStack)
        Redim Preserve arrayStack(index+1)
        arrayStack(index+1) = arrItem
    End Function

    Public Function toString(delimiter)
        toString = Join(arrayStack, delimiter)
    End Function
    '------------------------------------------------- 
    ' 函数名称: 返回栈长度，不包括该类保留的第一个元素
    ' 注意：数组下标 和 长度 相差1
    '-------------------------------------------------
    Public Function getItemCount
        getItemCount = Ubound(arrayStack)
    End Function

End Class
%>
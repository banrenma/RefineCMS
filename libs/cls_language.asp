<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Language
    Private p_lang_file, p_prefix, p_autoload
    Public sTxtKey
    Private Sub Class_Initialize
        ' on error resume next
        p_lang_file = RF_VIRTUAL_DIR & "/languages/" & RF_LANGUAGE & ".asp"
        p_prefix = RF_LANGUAGE & "_" 
        p_autoload = p_prefix & "autoload" 
    End Sub

    Private sub class_terminate
    End Sub

    Private Sub checkLangFile
        Dim fs
        Set fs=Server.CreateObject("Scripting.FileSystemObject") 
        If (fs.FileExists(Server.MapPath(p_lang_file))) = False Then
            rfExit(p_lang_file & " 不存在 (Not Exist)!")
        End If
        Set fs = Nothing
    End Sub

    Private Function getLangFileContent
        Dim tf
        Set tf = new cls_TextFile
        getLangFileContent = tf.ReadFile(p_lang_file)
        Set tf = Nothing
    End Function

    Private Sub regexParse(pattern, global)
        Dim regEx, matches, match
        Set regEx = New RegExp
        regEx.Pattern = pattern
        regEx.IgnoreCase = True ' 忽略大小写。
        regEx.Global = global ' 设置全程可用性。
        Set matches = regEx.Execute(getLangFileContent)
        If matches.Count = 0 Then
            rfExit("语言文件解析时，正则匹配行数为0，说明语言文件格式有误，或者企图查找一个不存在的语言文本标签(Regex Pattern：" & pattern & ")")
        End If
        Dim k, v
        For Each match in matches
            k = match.SubMatches(0)
            v = match.SubMatches(1)
            Application( p_prefix & trim(k) ) = trim(v)
        Next
        Set regEx = Nothing
    End Sub

    Public Sub init
        If  rfConvertInt(Application(p_autoload)) = 1 Then
            Exit Sub
        End If
        rfStartWatch()
        checkLangFile()
        regexParse "(rf_lang_\w+?)\s*@\s*([^\n]*?)(\r\n|\n|$)" , True
        Application(p_autoload) = 1
        rfEndWatch("语言文件首次解析并加载到Application")
    End Sub

    Public Function getLangTxt()
        rfStartWatch()
        If  rfIsEmpty(Application(p_prefix & sTxtKey)) Then
            seekLangTxt()
        End If
        rfEndWatch("从语言文件查找 " & sTxtKey & " ,解析并加载到Application")
        getLangTxt = Application(p_prefix & sTxtKey)
    End Function

    Private Sub seekLangTxt()
        checkLangFile()
        regexParse "(" & sTxtKey & ")\s*@\s*([^\n]*?)(\r\n|\n|$)"  , False
    End Sub



End Class
%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_Pagination
    Private iPageCount, iAbsolutePage, sResult
    Private sStyle, sPreviousText, sNextText
    Private Sub Class_Initialize
        ' on error resume next
        sStyle = "page_number_normal"
        sPreviousText = "上页"
        sNextText = "下页"
    End Sub

    Private sub class_terminate
    End Sub

    '------------------------------------------------- 
    '设置"上页"、“下页”的文字
    ' 仅生效于翻页样式
    '-------------------------------------------------
    Property Let PreviousText(param_previousText)
        sPreviousText = param_previousText
    End Property
    '------------------------------------------------- 
    '设置"上页"、“下页”的文字
    ' 仅生效于翻页样式
    '-------------------------------------------------
    Property Let NextText(param_nextText)
        sNextText = param_nextText
    End Property

    '------------------------------------------------- 
    '设置分页展现形式
    ' page_number_big  带页码（大）
    ' page_number_normal  带页码（中），默认
    ' page_number_small  带页码（小）
    ' page_turning_normal  翻页（挨着的）
    ' page_turning_align  翻页（两端对齐）
    '-------------------------------------------------
    Property Let Style(param_style)
        sStyle = param_style
    End Property

    Property Let PageCount(param_pageCount)
        iPageCount = param_pageCount
    End Property

    Property Let AbsolutePage(param_absolutePage)
        iAbsolutePage = param_absolutePage
    End Property

    ' page_number_big  带页码（大）
    ' page_number_normal  带页码（中），默认
    ' page_number_small  带页码（小）
    ' page_turning_normal  翻页（挨着的）
    ' page_turning_align  翻页（两端对齐）
    Public Function render
        If Hidden_IF_LESS_THAN_PAGESIZE And rfConvertInt(iPageCount)<=1 Then
            render = ""
            Exit Function
        End If

        Select Case sStyle
            Case "page_number_big"
                render = showPageNumber("big")
            Case "page_number_normal"
                render = showPageNumber("")
            Case "page_number_small"
                render = showPageNumber("small")
            Case "page_turning_normal"
                render = showPageTurningNormal
            Case "page_turning_align"
                render = showPageTurningAlign
            Case Else
                render = ""
        End Select
    End Function

    Private Function fmtUrlByPage(param_page)
        Dim pattern, regEx, replaceStr
        pattern = "(\?|&)" & QP("rf_page") & "=\d+"
        replaceStr = "$1" & QP("rf_page") & "=" & param_page
        Set regEx = New RegExp
        regEx.Pattern = pattern
        regEx.IgnoreCase = True ' 忽略大小写。
        regEx.Global = True ' 设置全程可用性。
        If regEx.Test(rfCurrentUrl) Then
            fmtUrlByPage = regEx.Replace(rfCurrentUrl, replaceStr)
        Else
            fmtUrlByPage = rfCurrentUrl & "&" & QP("rf_page") & "=" & param_page
        End If
        ' 去掉 "&opt=copy"、"&opt=del"、"csrfToken=UM6QH7SE"
        ' 后台“复制”、“删除”执行完成之后，如果 redirect 到列表页，就没必要这样处理了。
        fmtUrlByPage = rfReplaceRegex(fmtUrlByPage, "&opt=(copy|del)", "")
        fmtUrlByPage = rfReplaceRegex(fmtUrlByPage, "&csrfToken=[a-zA-Z0-9]{8}", "")
        Set regEx = Nothing
    End Function

    Private Function showPageNumber(param_size)
        sResult = "<nav aria-label=""..."">" & vbNewLine
        sResult = sResult & vbTab & "<ul class=""pagination" & fmtSize(param_size) & """>" & vbNewLine
        Dim i, indexStart, index_end
        indexStart = iAbsolutePage - 2
        If indexStart < 1 Then
            indexStart = 1
        End If
        index_end = iAbsolutePage + 2
        If index_end > iPageCount Then
            index_end = iPageCount
        End If
        If iAbsolutePage = indexStart Then
            sResult = sResult & vbTab & "<li class=""disabled""><span><span aria-hidden=""true"">&laquo;</span></span></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li><a href=""" & fmtUrlByPage(iAbsolutePage-1) & """ aria-label=""Previous""><span aria-hidden=""true"">«</span></a></li>" & vbNewLine
        End If
 
        For i=indexStart to index_end
            If i = iAbsolutePage Then
                sResult = sResult & vbTab & "<li class=""active""><span>" & i & " <span class=""sr-only"">(current)</span></span></li>" & vbNewLine
            Else
                sResult = sResult & vbTab & "<li><a href=""" & fmtUrlByPage(i) & """>" & i & " </a></li>" & vbNewLine
            End If
        Next
        If iAbsolutePage = index_end Then
            sResult = sResult & vbTab & "<li class=""disabled""><span><span aria-hidden=""true"">&raquo;</span></span></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li><a href=""" & fmtUrlByPage(iAbsolutePage+1) & """ aria-label=""Next""><span aria-hidden=""true"">&raquo;</span></a></li>" & vbNewLine
        End If
        sResult = sResult & vbTab & "</ul>" & vbNewLine
        sResult = sResult & "</nav>" & vbNewLine
        showPageNumber = sResult
    End Function
' sPreviousText, sNextText
    ' page_turning_normal  翻页（挨着的）
    ' page_turning_align  翻页（两端对齐）
    Private Function showPageTurningNormal()
        sResult = "<nav aria-label=""..."">" & vbNewLine
        sResult = sResult & vbTab & "<ul class=""pager"">" & vbNewLine

        If iAbsolutePage = 1 Then
            sResult = sResult & vbTab & "<li class=""disabled""><a href=""#"">" & rfHtmlEncode(sPreviousText) & "</a></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li><a href=""" & fmtUrlByPage(iAbsolutePage-1) & """>" & rfHtmlEncode(sPreviousText) & "</a></li>" & vbNewLine
        End If
        If iAbsolutePage = iPageCount Then
            sResult = sResult & vbTab & "<li class=""disabled""><a href=""#"">" & rfHtmlEncode(sNextText) & "</a></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li><a href=""" & fmtUrlByPage(iAbsolutePage+1) & """>" & rfHtmlEncode(sNextText) & "</a></li>" & vbNewLine
        End If

        sResult = sResult & vbTab & "</ul>" & vbNewLine
        sResult = sResult & "</nav>" & vbNewLine
        showPageTurningNormal = sResult
    End Function
    Private Function showPageTurningAlign()
        sResult = "<nav aria-label=""..."">" & vbNewLine
        sResult = sResult & vbTab & "<ul class=""pager"">" & vbNewLine

        If iAbsolutePage = 1 Then
            sResult = sResult & vbTab & "<li class=""previous disabled""><a href=""#"">" & rfHtmlEncode(sPreviousText) & "</a></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li class=""previous""><a href=""" & fmtUrlByPage(iAbsolutePage-1) & """>" & rfHtmlEncode(sPreviousText) & "</a></li>" & vbNewLine
        End If
        If iAbsolutePage = iPageCount Then
            sResult = sResult & vbTab & "<li class=""next disabled""><a href=""#"">" & rfHtmlEncode(sNextText) & "</a></li>" & vbNewLine
        Else
            sResult = sResult & vbTab & "<li class=""next""><a href=""" & fmtUrlByPage(iAbsolutePage+1) & """>" & rfHtmlEncode(sNextText) & "</a></li>" & vbNewLine
        End If

        sResult = sResult & vbTab & "</ul>" & vbNewLine
        sResult = sResult & "</nav>" & vbNewLine
        showPageTurningAlign = sResult
    End Function

    Private Function fmtSize(param_size)
        Select Case param_size
            Case "big"
                fmtSize = " pagination-lg" '注：字符串前有一个空格
            Case "small"
                fmtSize = " pagination-sm" '注：字符串前有一个空格
            Case Else
                fmtSize = ""
        End Select
    End Function
End Class
%>
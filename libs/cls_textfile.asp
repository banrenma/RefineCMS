<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_TextFile
    ' CharSet-编码格式(utf-8,gb2312.....) 
    Public mCharSet
    Private stm
    Private Sub Class_Initialize
        ' on error resume next
        mCharSet = "utf-8"
    End Sub

    Private Sub class_terminate
    End Sub
    '------------------------------------------------- 
    '函数名称:ReadFile 
    '作用:利用AdoDb.Stream对象来读取各种格式的文本文件 
    '---------------------------------------------------- 
    Function ReadFile(FileUrl)
        Dim fs
        Set fs = Server.CreateObject("Scripting.FileSystemObject")
        If  False = fs.FileExists(Server.MapPath(FileUrl)) Then
            rfShowErrorAuto "cls_TextFile.asp", T("rf_lang_file_not_found") & ": " & FileUrl
        End If
        Set fs = Nothing

        Dim Str
        Set stm = CreateObject("Adodb.Stream")
        stm.Type = 2 '文本
        stm.mode = 3 '读写
        stm.charset = mCharSet
        stm.Open
        stm.loadfromfile Server.MapPath(FileUrl)
        Str = stm.readtext
        stm.Close
        Set stm = Nothing
        ReadFile = Str
    End Function

    '------------------------------------------------- 
    '函数名称:WriteToFile 
    '作用:利用Adodb.Stream对象来写入各种格式的文本文件 
    '参数:
        ' FileUrl-文件相对路径;
        ' Str-文件内容;
    '---------------------------------------------------- 
    Sub WriteToFile (FileUrl, Str)
        Set stm = CreateObject("Adodb.Stream")
        stm.Type = 2 '文本
        stm.mode = 3'读写
        stm.charset = mCharSet
        stm.Open
        stm.WriteText Str
        stm.SaveToFile Server.MapPath(FileUrl), 2 
        stm.flush
        stm.Close
        Set stm = Nothing
    End Sub
End class
%>
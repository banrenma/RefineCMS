<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Class cls_LoadPhotoPlugin
    Private sTplHtml
    Private sPatternLightbox, sPatternFlexslider

    Private Sub Class_Initialize
        sPatternFlexslider = "class=(""|')[^""']*?flexslider[^""']*?(""|')"
        sPatternLightbox = "data-lightbox=(""|')[^""']*?(""|')"
    End Sub

    Private Sub class_terminate
    End Sub

    Property Let TplHtml(param_tplHtml)
        sTplHtml = param_tplHtml
    End Property

    Public Function autoLoad()
        loadLightbox
        loadFlexslider
        autoLoad = sTplHtml
    End Function

    Private Function commentStart(param_plugin)
        commentStart = "<!--  ######## RefineCMS 自动加载的 " & param_plugin & " 插件 start  ########  -->"
    End Function

    Private Function commentEnd(param_plugin)
        commentEnd = "<!--  ######## RefineCMS 自动加载的 " & param_plugin & " 插件 end  ########  -->"
    End Function

    Private Function cssFlexslider
        cssFlexslider = vbNewLine & commentStart("Flexslider") & vbNewLine
        cssFlexslider = cssFlexslider & vbTab & "<link href=""assets/css/flexslider.css"" rel=""stylesheet"">" & vbNewLine
        cssFlexslider = cssFlexslider & commentEnd("Flexslider") & vbNewLine
    End Function
    Private Function jsFlexslider
        jsFlexslider = vbNewLine & commentStart("Flexslider") & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & "<script src=""assets/js/jquery.flexslider.js""></script>" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & "<script>" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & "$(function() {" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & vbTab & "$('.flexslider').flexslider({" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & vbTab & "animation: ""slide""" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & vbTab & "});" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & "});" & vbNewLine
        jsFlexslider = jsFlexslider & vbTab & "</script>" & vbNewLine
        jsFlexslider = jsFlexslider & commentEnd("Flexslider") & vbNewLine
    End Function
    Private Sub loadFlexslider
        If rfRegexTest(sTplHtml, sPatternFlexslider) Then
            sTplHtml = rfReplaceRegex(sTplHtml, "</head>", cssFlexslider&"</head>")
            sTplHtml = rfReplaceRegex(sTplHtml, "</body>", jsFlexslider&"</body>")
        End If
    End Sub

    Private Function cssLightbox
        cssLightbox = vbNewLine & commentStart("Lightbox") & vbNewLine
        cssLightbox = cssLightbox & vbTab & "<link href=""assets/css/lightbox.css"" rel=""stylesheet"">" & vbNewLine
        cssLightbox = cssLightbox & commentEnd("Lightbox") & vbNewLine
    End Function
    Private Function jsLightbox
        jsLightbox = vbNewLine & commentStart("Lightbox") & vbNewLine
        jsLightbox = jsLightbox & vbTab & "<script src=""assets/js/lightbox.min.js""></script>" & vbNewLine
        jsLightbox = jsLightbox & vbTab & "<script>" & vbNewLine
        jsLightbox = jsLightbox & vbTab & "$(function() {" & vbNewLine
        jsLightbox = jsLightbox & vbTab & vbTab & "lightbox.option({" & vbNewLine
        jsLightbox = jsLightbox & vbTab & vbTab & "'resizeDuration': 200," & vbNewLine
        jsLightbox = jsLightbox & vbTab & vbTab & "'wrapAround': true" & vbNewLine
        jsLightbox = jsLightbox & vbTab & vbTab & "})" & vbNewLine
        jsLightbox = jsLightbox & vbTab & "});" & vbNewLine
        jsLightbox = jsLightbox & vbTab & "</script>" & vbNewLine
        jsLightbox = jsLightbox & commentEnd("Lightbox") & vbNewLine
    End Function
    Private Sub loadLightbox
        If rfRegexTest(sTplHtml, sPatternLightbox) Then
            sTplHtml = rfReplaceRegex(sTplHtml, "</head>", cssLightbox&"</head>")
            sTplHtml = rfReplaceRegex(sTplHtml, "</body>", jsLightbox&"</body>")
        End If
    End Sub

End Class
%>
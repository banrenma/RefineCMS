<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_tag_subnav
    Dim iID, tag, tagName, tagTextArr, presentation, presentationList, message, resultCsrf

    Set tag = new cls_Tag
    tag.iTagType = 8 'tags.tag_type=1
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            iID = rfConvertInt(Request.Form("id_hidden"))
            If iID>0 Then
                tag.init iID
                If tag.iID <= 0 Then
                    rfShowError "tag/subnav.asp", T("rf_lang_cannot_find_the_item")
                End If
            End If
            If rfConvertInt(Request.Form("subnav_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_subnav") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            tag.sTagName = Request.Form("tag_name")
            tagTextArr = Array(Request.Form("subnav_pre_id"))
            tag.sTagText = Request.Form("subnav_pre_id")
            tag.iInfotplFlag = 0
            If  rfIsEmpty(message) Then
                message = tag.save
                If message = True Then
                    Response.Redirect("?" & QP("rf_action") & "=backend&module=tag.list&tagtype=" & tag.iTagType)
                End If
            End If
        Else
            message = resultCsrf
        End If
    End If
    
    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        tagName = Request.QueryString("tag_name")
        If Not rfIsEmpty(tagName) Then
            tag.init_by_tag_name tagName
            If tag.sTagName = "" Then
                rfShowError "tag/subnav.asp", T("rf_lang_cannot_find_the_item")
            Else
                '  12
                tagTextArr = Split(tag.sTagText, "|", -1, 1)
                If UBound(tagTextArr) <> 0 Then
                    rfShowError "tag/subnav.asp", T("rf_lang_tag_tagtext_parse_error") & ": " & tag.sTagText
                End If
            End If
        Else
            Redim tagTextArr(0)
            tagTextArr(0) = 0
        End If
    End If

    Set presentation = new cls_Presentation
    presentationList = presentation.list(tag.iTagType)  'presentations.pre_type
%>
<!-- #include file="../../../templates/backend/tag/subnav.asp" -->
<%
Set tag = Nothing
Set presentation = Nothing
End Sub
%>
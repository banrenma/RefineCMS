<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_tag_config
    Dim config, tagTextArr
    Dim message, resultCsrf
    Dim presentation, essaylistPresentationList, essayinfoPresentationList
    Dim photolistPresentationList, photoinfoPresentationList, singlepagePresentationList, friendlinkPresentationList

    Set config = new cls_Config
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            config.init "default_presentation"
            If rfIsEmpty(config.sConfigKey) Then
                rfShowError "tag/config.asp", T("rf_lang_config_presentation_cannot_find")
            End If

            If rfConvertInt(Request.Form("listnum")) <=0 Then
                message = message & T("rf_lang_tag_listnum_validate") & "<br/>"
            End If
            If Not rfInArray(rfConvertStr(Request.Form("showorder")), Array("asc", "desc"))  Then
                message = message & T("rf_lang_tag_order_validate") & "<br/>"
            End If
            If rfIsEmpty(rfConvertStr(Request.Form("pagination")))  Then
                message = message & T("rf_lang_pagination_validate") & "<br/>"
            End If
            If rfConvertInt(Request.Form("essay_list_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_essay_list") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("essay_info_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_essay_info") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("photo_list_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_photo_list") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("photo_info_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_photo_info") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("singlepage_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_singlepage") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("friendlink_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_friendlink") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            
            tagTextArr = Array(Request.Form("listnum"), Request.Form("showorder"), Request.Form("pagination"), Request.Form("essay_list_pre_id"), Request.Form("essay_info_pre_id"), Request.Form("photo_list_pre_id"), Request.Form("photo_info_pre_id"), Request.Form("singlepage_pre_id"), Request.Form("friendlink_pre_id"))
            config.sConfigText = Join(tagTextArr, "|")
   ' rfExit tagTextArr(0)
            If  rfIsEmpty(message) Then
                message = config.save
                If message = True Then
                    Response.Redirect("?" & QP("rf_action") & "=backend&module=tag.config")
                End If
            End If
        Else
            message = resultCsrf
        End If
    End If
    
    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        config.init "default_presentation"
        If rfIsEmpty(config.sConfigKey) Then
            rfShowError "tag/config.asp", T("rf_lang_config_presentation_cannot_find")
        End If
        If Not rfIsEmpty(config.sConfigText) Then
            tagTextArr = Split(config.sConfigText, "|", -1, 1)
            If UBound(tagTextArr) <> 8 Then
                tagTextArr = initConfigArray
            End If
        Else
            tagTextArr = initConfigArray
        End If

    End If

    Set presentation = new cls_Presentation
    essaylistPresentationList = presentation.list(1) '1：呈现方式种类pre_type的ID
    essayinfoPresentationList = presentation.list(2) '1：呈现方式种类pre_type的ID
    photolistPresentationList = presentation.list(3) '1：呈现方式种类pre_type的ID
    photoinfoPresentationList = presentation.list(4) '1：呈现方式种类pre_type的ID
    singlepagePresentationList = presentation.list(5) '1：呈现方式种类pre_type的ID
    friendlinkPresentationList = presentation.list(9) '1：呈现方式种类pre_type的ID
%>
<!-- #include file="../../../templates/backend/tag/config.asp" -->
<%
Set config = Nothing
Set presentation = Nothing
End Sub

Function initConfigArray
    initConfigArray = Array(10, "desc", "page_number_normal", 0, 0, 0, 0, 0, 0)
End Function
%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_tag_flexslider
    Dim iID, tag, tagName, tagTextArr, category, categoryList, presentation, presentationList, message, resultCsrf

    Set tag = new cls_Tag
    tag.iTagType = 11 'tags.tag_type=3
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            iID = rfConvertInt(Request.Form("id_hidden"))
            If iID>0 Then
                tag.init iID
                If tag.iID <= 0 Then
                    rfShowError "tag/flexslider.asp", T("rf_lang_cannot_find_the_item")
                End If
            End If
            If rfConvertInt(Request.Form("cat_id")) <=0 Then
                message = message & T("rf_lang_content_from_is_empty") & "<br/>"
            End If
            If rfConvertInt(Request.Form("listnum")) <=0 Then
                message = message & T("rf_lang_tag_listnum_validate") & "<br/>"
            End If
            If Not rfInArray(rfConvertStr(Request.Form("showorder")), Array("asc", "desc"))  Then
                message = message & T("rf_lang_tag_order_validate") & "<br/>"
            End If
            If rfConvertInt(Request.Form("photo_list_pre_id")) <=0 Then
                message = message & T("rf_lang_presentation_pretype_photo_list") & ": " & T("rf_lang_presentation_is_empty") & "<br/>"
            End If
            tag.sTagName = Request.Form("tag_name")
            tagTextArr = Array(Request.Form("cat_id"), Request.Form("listnum"), Request.Form("showorder"), Request.Form("photo_list_pre_id"))
            tag.sTagText = Join(tagTextArr, "|")
            tag.iInfotplFlag = 0
            If  rfIsEmpty(message) Then
                message = tag.save
                If message = True Then
                    Response.Redirect("?" & QP("rf_action") & "=backend&module=tag.list&tagtype=" & tag.iTagType)
                End If
            End If
        Else
            message = resultCsrf
        End If
    End If
    
    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        tagName = Request.QueryString("tag_name")
        If Not rfIsEmpty(tagName) Then
            tag.init_by_tag_name tagName
            If tag.sTagName = "" Then
                rfShowError "tag/flexslider.asp", T("rf_lang_cannot_find_the_item")
            Else
                '  12|1|asc|2
                tagTextArr = Split(tag.sTagText, "|", -1, 1)
                If UBound(tagTextArr) <> 3 Then
                    rfShowError "tag/flexslider.asp", T("rf_lang_tag_tagtext_parse_error") & ": " & tag.sTagText
                End If
            End If
        Else
            Redim tagTextArr(4)
            tagTextArr(0) = 0
            tagTextArr(1) = 10
            tagTextArr(2) = ""
            tagTextArr(3) = 0
        End If
    End If

    Set category = new cls_Category
    categoryList = category.listByType(2) 'categories.cat_type
    Set presentation = new cls_Presentation
    presentationList = presentation.list(tag.iTagType) 'presentations.pre_type
%>
<!-- #include file="../../../templates/backend/tag/flexslider.asp" -->
<%
Set tag = Nothing
Set category = Nothing
Set presentation = Nothing
End Sub
%>
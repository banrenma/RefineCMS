<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
' 本功能参考了 阿江ASP探针 V1.93 20060602
' 感谢：阿江守候 http://www.ajiang.net
Sub rf_mod_dashboard
    If Request.QueryString("opt") = "flush_cache" Then
        rfCheckCsrfAuto()
        Application.Contents.RemoveAll()
        Response.Redirect("?" & QP("rf_action") & "=backend&module=dashboard")
    End If

    'Profile
    Dim countEssay, countPhoto, countFriendlink, countSinglepage, countPresentation, countTag
    Dim essay, media, friendlink, singlepage, presentation, tag

    Set essay = new cls_Essay
    countEssay = essay.getItemsCount(0)
    Set essay = Nothing

    Set media = new cls_Media
    countPhoto = media.getItemsCount(0)
    Set media = Nothing

    Set friendlink = new cls_Friendlink
    countFriendlink = friendlink.getItemsCount(0)
    Set friendlink = Nothing

    Set singlepage = new cls_Singlepage
    countSinglepage = singlepage.getItemsCount
    Set singlepage = Nothing

    Set presentation = new cls_Presentation
    countPresentation = presentation.getItemsCount(0)
    Set presentation = Nothing

    Set tag = new cls_Tag
    countTag = tag.getItemsCount(0)
    Set tag = Nothing

    ' 服务器
    Dim os, cpuNumber, cpuID
    os = T("rf_lang_unknow")
    cpuNumber = T("rf_lang_unknow")
    cpuID = T("rf_lang_unknow")

    Dim objArr
    objArr = rfCreateObject("WScript.Shell")
    If objArr(0) Then
        Dim WshShell, WshSysEnv
        Set WshShell = Server.CreateObject("WScript.Shell")
        Set WshSysEnv = WshShell.Environment("SYSTEM")
        os = Cstr(WshSysEnv("OS"))
        If rfIsEmpty(os) Then
            os = T("rf_lang_unknow")
        End If
        cpuNumber = Cstr(WshSysEnv("NUMBER_OF_PROCESSORS"))
        If rfIsEmpty(cpuNumber) Then
            cpuNumber = Request.ServerVariables("NUMBER_OF_PROCESSORS")
        End If
        If rfIsEmpty(cpuNumber) Then
            cpuNumber = T("rf_lang_unknow")
        End If
        cpuID = Cstr(WshSysEnv("PROCESSOR_IDENTIFIER"))
        If rfIsEmpty(cpuID) Then
            cpuID = T("rf_lang_unknow")
        End If
        Set WshShell = Nothing
        Set WshSysEnv = Nothing
    End If

    ' 组件
    Dim components
    components = Array("Microsoft.XMLHTTP", "WScript.Shell", "Scripting.FileSystemObject", "Adodb.Connection", "Adodb.Stream")

    ' 缓存
    rfStartWatch()
    Dim keyApp, trCacheStr, trOtherStr, trLangStr, cacheSize
    cacheSize = 0
    For Each keyApp in Application.Contents
        If rfRegexTest(keyApp, RF_PAGE_CACHE_PREFIX) Then
            trCacheStr = trCacheStr & "<tr>" & vbNewLine
            trCacheStr = trCacheStr & "<th scope=""row"">" & keyApp & "</th>" & vbNewLine
            trCacheStr = trCacheStr & "<td>" & fmtFileSize(rfStrLen(Application.Contents(keyApp))) & "</td>" & vbNewLine
            trCacheStr = trCacheStr & "<td class=""my-td""><u style=""cursor: pointer;"" onclick=""javascript:$(this).parent().toggleClass('my-td');"">显示全部...</u>" & fmtObjectToString(Application.Contents(keyApp)) & "</td>" & vbNewLine
            trCacheStr = trCacheStr & "</tr>" & vbNewLine
            cacheSize = cacheSize + rfStrLen(Application.Contents(keyApp))
        ElseIf rfRegexTest(keyApp, "_rf_lang_") Then
            trLangStr = trLangStr & "<tr>" & vbNewLine
            trLangStr = trLangStr & "<th scope=""row"">" & keyApp & "</th>" & vbNewLine
            trLangStr = trLangStr & "<td>" & fmtObjectToString(Application.Contents(keyApp)) & "</td>" & vbNewLine
            trLangStr = trLangStr & "</tr>" & vbNewLine
        Else 
            trOtherStr = trOtherStr & "<tr>" & vbNewLine
            trOtherStr = trOtherStr & "<th scope=""row"">" & keyApp & "</th>" & vbNewLine
            trOtherStr = trOtherStr & "<td>" & fmtObjectToString(Application.Contents(keyApp)) & "</td>" & vbNewLine
            trOtherStr = trOtherStr & "</tr>" & vbNewLine
        End If
    Next
    rfEndWatch("遍历 Application.Contents 集合")

    
    ' upload 文件夹
    Dim fs, fo, uploadSize
    Set fs = Server.CreateObject("Scripting.FileSystemObject")
    If fs.FolderExists(Server.MapPath(RF_UPLOAD_FOLDER)) Then
        Set fo=fs.GetFolder(Server.MapPath(RF_UPLOAD_FOLDER))
        uploadSize = fmtFileSize(fo.Size)
    Else
        uploadSize = T("rf_lang_folder_not_exists")
    End If
    Set fs = Nothing
    Set fo = Nothing




%>
<!-- #include file="../../../templates/backend/dashboard/dashboard.asp" -->
<%
End Sub
%>
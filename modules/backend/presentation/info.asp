<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_presentation_info
    Dim presentation, iID, message, resultCsrf, suffixUrl
    suffixUrl = ""
    Set presentation = new cls_Presentation
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            iID = rfConvertInt(Request.Form("id_hidden"))
            If iID>0 Then
                presentation.init iID
                If presentation.iID <= 0 Then
                    rfShowError "presentation/info.asp", T("rf_lang_cannot_find_the_item")
                End If
            End If
            presentation.sPreName = Request.Form("pre_name")
            presentation.iPreType = rfConvertInt(Request.Form("pre_type"))
            suffixUrl = "&pretype=" & rfConvertInt(Request.Form("pre_type"))
            presentation.sCodehtml = Request.Form("codehtml")
            presentation.sRemark = Request.Form("remark")
            message = presentation.save
            If message = True Then
                Response.Redirect("?" & QP("rf_action") & "=backend&module=presentation.list" & suffixUrl)
            End If
        Else
            message = resultCsrf
        End If
    End If
    
    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        Response.Cookies("treeview")="010"
        presentation.iPreType = Request.QueryString("pretype")
        iID = rfConvertInt(Request.QueryString("id"))
        If iID>0 Then
            presentation.init iID
        End If
    End If

%>
<!-- #include file="../../../templates/backend/presentation/info.asp" -->
<%
Set presentation = Nothing
End Sub
%>
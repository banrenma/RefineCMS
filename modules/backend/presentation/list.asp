<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_presentation_list()
    Dim presentation
    If Request.QueryString("opt") = "del" Then
        rfCheckCsrfAuto()
        Set presentation = new cls_Presentation
        presentation.iID = Request.QueryString("id")
        presentation.del
        Set presentation = Nothing
    End If
    If Request.QueryString("opt") = "copy" Then
        rfCheckCsrfAuto()
        Set presentation = new cls_Presentation
        presentation.iID = rfConvertInt(Request.QueryString("id"))
        Dim result
        result = presentation.copy
        If result <> True Then
            rfShowError "presentation/list.asp", result
        End If
        Set presentation = Nothing
    End If

    Dim presentationList, pretype, page, csrf
    csrf = rfCsrfTokenQueryString
    pretype = rfConvertInt(Request.QueryString("pretype"))
    page = rfConvertInt(Request.QueryString(QP("rf_page")))
    Set presentation = new cls_Presentation
    presentation.PageSize = RF_PAGINATION_PRESENTATION_LIST
    presentation.AbsolutePage = page
    presentationList = presentation.list(pretype)

    Dim pager, showPage
    Set pager = new cls_Pagination
    pager.PageCount = presentation.PageCount
    pager.AbsolutePage = presentation.AbsolutePage
    ' pager.Style = "page_turning_align"
    showPage = pager.render()

%>
    <!-- #include file="../../../templates/backend/presentation/list.asp" -->
<%
    Set presentation = Nothing
    Set pager = Nothing
End Sub

%>
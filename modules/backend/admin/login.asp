<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_login
    Dim admin, message, resultCsrf, resultCaptcha, resultLogin
    Set admin = new cls_Admin
    If Request.Form("btnaction") = "loginAction" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            resultCaptcha = rfCheckCaptcha()
            If  resultCaptcha = True Then
                resultLogin = admin.login()
                If resultLogin = True Then
                    If rfRegexTest(Request.ServerVariables("HTTP_REFERER"), "module=log(in|out)") Then
                        Response.Redirect("?" & QP("rf_action") & "=backend&module=dashboard")
                    Else
                        Response.Redirect(Request.ServerVariables("HTTP_REFERER"))
                    End If
                Else
                    message = resultLogin
                End If
            Else
                message = resultCaptcha
            End If
        Else
            message = resultCsrf
        End If
    End If
    
%>
<!-- #include file="../../../templates/backend/admin/login.asp" -->
<%
    Set admin = Nothing
    
End Sub
Sub rf_mod_logout
    Session.Contents.RemoveAll()
    rfTransferLogin
    ' Response.Redirect("?" & QP("rf_action") & "=backend&module=login")
End Sub
%>
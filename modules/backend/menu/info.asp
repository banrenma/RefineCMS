<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_menu_info
    Dim iID, menu, mainMenus, category, categoryList, singlepage, singlepageList, message, resultCsrf

    Set menu = new cls_Menu
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            iID = rfConvertInt(Request.Form("id_hidden"))
            If iID>0 Then
                menu.init iID
                If menu.iID <= 0 Then
                    rfShowError "menu/info.asp", T("rf_lang_cannot_find_the_item")
                End If
            End If
            menu.sMenuName = Request.Form("menu_name")
            menu.sExcerpt = Request.Form("excerpt")
            menu.sKeywords = Request.Form("keywords")
            menu.iPid = Request.Form("pid")
            menu.iMenuType = Request.Form("menu_type")
            menu.iIsSinglepage = 0
            If rfConvertInt(menu.iMenuType) = 1 Then
                If Left(Request.Form("data_id"), 11) = "singlepage_" Then
                    menu.iIsSinglepage = 1
                    menu.iDataID = Mid(Request.Form("data_id"), 12)
                Else
                    menu.iDataID = Request.Form("data_id")
                End If
            Else
                menu.iDataID = 0
            End If
            If  rfIsEmpty(message) Then
                message = menu.save
                If message = True Then
                    Response.Redirect("?" & QP("rf_action") & "=backend&module=menus")
                End If
            End If
        Else
            message = resultCsrf
        End If
    End If
    
    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        iID = rfConvertInt(Request.QueryString("id"))
        If iID>0 Then
            menu.init iID
        End If
    End If

    mainMenus = menu.list(0)
    Set category = new cls_Category
    categoryList = category.list
    Set singlepage = new cls_Singlepage
    singlepageList = singlepage.list
%>
<!-- #include file="../../../templates/backend/menu/info.asp" -->
<%
Set singlepage = Nothing
Set category = Nothing
End Sub
%>
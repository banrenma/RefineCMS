<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_tpl_info

    Dim tf, currentFile
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
            currentFile = RF_TEMPLATE_FOLDER_FRONTEND & Request.Form("filename")
            Set tf = new cls_TextFile
            tf.WriteToFile currentFile, Request.Form("filestr")
            Set tf = Nothing
            Response.Redirect("?" & QP("rf_action") & "=backend&module=tpl")
    End If
    
    If rfIsEmpty(Request.QueryString("file")) Then
        rfShowError "modules/backend/tpl/info.asp", T("rf_lang_file_not_found") & " [empty parameter]"
    End If
    Dim fileName, fileStr, dateLastModified, dateCreated
    fileName = Request.QueryString("file")
    currentFile = RF_TEMPLATE_FOLDER_FRONTEND & rfReplace(Request.QueryString("file"), "../", "")'只允许修改该目录下的文件
    
    Dim fs, f, ext
    Set fs=Server.CreateObject("Scripting.FileSystemObject")
    ext = fs.GetExtensionName(Server.MapPath(currentFile))
    Set f=fs.GetFile(Server.MapPath(currentFile))
    If Not rfInArray(ext, RF_TEMPLATE_FILETYPE) Then
        rfShowError "modules/backend/tpl/info.asp", T("rf_lang_tpl_filetype_validate") & Join(RF_TEMPLATE_FILETYPE, " | ")
    End If
    dateLastModified = f.DateLastModified
    dateCreated = f.DateCreated
    Set f=nothing
    Set fs=nothing

    Set tf = new cls_TextFile
    fileStr = tf.ReadFile(currentFile)
    Set tf = Nothing

%>
<!-- #include file="../../../templates/backend/tpl/info.asp" -->
<%
End Sub
%>
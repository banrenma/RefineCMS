<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_tpl
    If Request.QueryString("opt") = "flush_cache" Then
        rfCheckCsrfAuto()
        Application.Contents.RemoveAll()
        Response.Redirect("?" & QP("rf_action") & "=backend&module=dashboard")
    End If
    
    Dim tmpFile
    Dim fileIndexAsp, fileIndexHtml, fileCommon
    Dim file404, fileError
    Dim filelistInfo, filelistMenu, filelistDeleted
    Dim filelistUnknow, folderlistUnknow

    Set filelistInfo = new cls_Stack
    Set filelistMenu = new cls_Stack
    Set filelistDeleted = new cls_Stack
    Set filelistUnknow = new cls_Stack
    Set folderlistUnknow = new cls_Stack

    Dim fs, fo, x, folderFrontendNotExists
    folderFrontendNotExists = ""
    Set fs = Server.CreateObject("Scripting.FileSystemObject")
    If fs.FolderExists(Server.MapPath(RF_TEMPLATE_FOLDER_FRONTEND)) Then
        Set fo=fs.GetFolder(Server.MapPath(RF_TEMPLATE_FOLDER_FRONTEND))
        ' 遍历文件
        For Each x in fo.files
            Select Case x.Name
                Case "index.html"
                    fileIndexHtml = x.Name
                Case "index.asp"
                    fileIndexAsp = x.Name
                Case "common.asp"
                    fileCommon = x.Name
                Case "404.asp"
                    file404 = x.Name
                Case "error.asp"
                    fileError = x.Name
                Case Else
                    If rfRegexTest(x.Name, "info_\d+\.asp") Then
                        filelistInfo.push(x.Name)
                    ElseIf rfRegexTest(x.Name, "menu_\d+\.asp") Then
                        filelistMenu.push(x.Name)
                    ElseIf rfRegexTest(x.Name, "(menu|info)_\d+_deleted_\d+\.asp") Then
                        filelistDeleted.push(x.Name)
                    Else
                        filelistUnknow.push(x.Name)
                    End If
            End Select
        Next
        ' 遍历文件夹
        For Each x in fo.SubFolders
            folderlistUnknow.push(x.Name)
        Next
    Else
        folderFrontendNotExists = T("rf_lang_folder_not_exists") & ": " & RF_TEMPLATE_FOLDER_FRONTEND
    End If

    Set fs = Nothing
    Set fo = Nothing

%>
<!-- #include file="../../../templates/backend/tpl/list.asp" -->
<%
Set filelistInfo = Nothing
Set filelistMenu = Nothing
Set filelistDeleted = Nothing
Set filelistUnknow = Nothing
Set folderlistUnknow = Nothing
End Sub
%>
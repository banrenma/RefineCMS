<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_ds_category
    Dim category
    If Request.QueryString("opt") = "del" Then
        rfCheckCsrfAuto()
        Set category = new cls_Category
        category.iID = rfConvertInt(Request.QueryString("id"))
        category.del
        Set category = Nothing
    End If

    Dim catList, page, csrf
    csrf = rfCsrfTokenQueryString
    page = rfConvertInt(Request.QueryString(QP("rf_page")))
    Set category = new cls_Category
    category.PageSize = RF_PAGINATION_CATEGORY_LIST
    category.AbsolutePage = page
    catList = category.list()

    Dim pager, showPage
    Set pager = new cls_Pagination
    pager.PageCount = category.PageCount
    pager.AbsolutePage = category.AbsolutePage
    ' pager.Style = "page_turning_align"
    showPage = pager.render()
%>
<!-- #include file="../../../templates/backend/datasource/category.asp" -->
<%
    Set category = Nothing
End Sub

Function rf_mod_ds_category_by_type(moduleName, catType)
    Dim category, catList, result
    result = ""
    ' 此处增加判断是为了减少不必要的、对数据表(categories)的访问
    If rfRegexTest(Request.QueryString("module"), "^ds\.") Then
        Set category = new cls_Category
        catList = category.listByType(catType)
        If Not IsNull(catList) Then
            result = "<ul>" & vbNewLine
            Dim i
            For i=LBound(catList, 2) to UBound(catList, 2)
                result = result & vbTab & "<li><a href=""?" & QP("rf_action") & "=backend&module=" & moduleName & "&catID=" & catList(0, i) & """>" & catList(1, i) & "</a></li>" & vbNewLine
            Next
            result = result & "</ul>"
        End If
        Set category = Nothing
    End If
    rf_mod_ds_category_by_type = result
End Function
%>
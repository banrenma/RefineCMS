<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_ds_essay_info
    Dim essay, category, catgoryOptions, iID, message, resultCsrf
    Set essay = new cls_Essay
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        resultCsrf = rfCheckCsrf()
        If resultCsrf = True Then
            iID = rfConvertInt(Request.Form("id_hidden"))
            If iID>0 Then
                essay.init iID
                If essay.iID <= 0 Then
                    rfShowError "essay_info.asp", T("rf_lang_cannot_find_the_item")
                End If
            End If
            essay.iCatId = rfConvertInt(Request.Form("cat_id"))
            essay.sTitle = Request.Form("title")
            essay.sExcerpt = Request.Form("excerpt")
            essay.sKeywords = Request.Form("keywords")
            essay.sFilepath = Request.Form("filepath_hidden")
            essay.sResolution = Request.Form("resolution_hidden")
            essay.sMainbody = Request.Form("mainbody")
            message = essay.save
            If message = True Then
                Response.Redirect("?" & QP("rf_action") & "=backend&module=ds.essays&catID=" & essay.iCatId)
            End If
        Else
            message = resultCsrf
        End If

    End If

    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        essay.iCatId = rfConvertInt(Request.QueryString("catID"))
        iID = rfConvertInt(Request.QueryString("id"))
        If iID>0 Then
            essay.init iID
            If essay.iID <= 0 Then
                rfShowError "essay_info.asp", T("rf_lang_cannot_find_the_item")
            End If
        End If
    End If

    Set category = new cls_Category
    catgoryOptions = category.listByType(1)
    category.init(essay.iCatId)

%>
<!-- #include file="../../../templates/backend/datasource/essay_info.asp" -->
<%
Set essay = Nothing
Set category = Nothing
End Sub
%>
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_ds_friendlinks_list()
    Dim friendlink
    If Request.QueryString("opt") = "del" Then
        rfCheckCsrfAuto()
        Set friendlink = new cls_Friendlink
        friendlink.init Request.QueryString("id")
        If friendlink.iID > 0 Then
            friendlink.del
        End If
        Set friendlink = Nothing
    End If

    Dim friendlinkList, catID, page, csrf
    csrf = rfCsrfTokenQueryString
    catID = rfConvertInt(Request.QueryString("catID"))
    page = rfConvertInt(Request.QueryString(QP("rf_page")))
    Set friendlink = new cls_Friendlink
    friendlink.PageSize = RF_PAGINATION_PHOTO_LIST
    friendlink.AbsolutePage = page
    friendlinkList = friendlink.list(catID)

    Dim pager, showPage
    Set pager = new cls_Pagination
    pager.PageCount = friendlink.PageCount
    pager.AbsolutePage = friendlink.AbsolutePage
    ' pager.Style = "page_turning_align"
    showPage = pager.render()

    Dim category
    Set category = New cls_Category
    category.init(catID)
    %>
    <!-- #include file="../../../templates/backend/datasource/friendlink_list.asp" -->
<%

    Set friendlink = Nothing
    Set pager = Nothing
    Set category = Nothing
End Sub

%>
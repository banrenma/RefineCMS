<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_ds_friendlinks_info
    Dim friendlink, category, catgoryOptions, iID, message, resultCsrf
    Set friendlink = new cls_Friendlink
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        Dim folder, filename, d, ext
        d = Now
        folder = RF_UPLOAD_FOLDER & Year(d) & "/" & Month(d) & "/" & Day(d)
        filename = LCase(rfGenerateHash) & "_" & Hour(d) & Minute(d) & Second(d)

        rfCreateFolder(folder)
        Dim Upload, ks, fileKey
        Set Upload = New FreeASPUpload
        Upload.Save Server.MapPath(folder)
        ' Upload.DumpData
        
        iID = rfConvertInt(Upload.FormElements.Item("id_hidden"))
        If iID>0 Then
            friendlink.init iID
            If friendlink.iID <= 0 Then
                rfShowError "friendlink_info.asp", T("rf_lang_cannot_find_the_item")
            End If
        End If
        resultCsrf = rfCheckCsrfByParam(Upload.FormElements.Item(LCase("csrfToken"))) 'FreeASPUpload把key做了小写处理 
        friendlink.iCatId = Upload.FormElements.Item("cat_id")
        friendlink.sLinkurl = Upload.FormElements.Item("linkurl")
        friendlink.iFontcolor = Upload.FormElements.Item("fontcolor")
        If resultCsrf = True Then
            friendlink.sLinkname = Upload.FormElements.Item("linkname")
            If Not friendlink.validateLinkname() Then
                message = T("rf_lang_friendlink_linkname_validate")
            ElseIf friendlink.linknameExists() Then
                message = T("rf_lang_friendlink_linkname_exists")
            ElseIf rfConvertInt(friendlink.iCatId) < 0 Then
                message = T("rf_lang_catid_validate")
            Else
                ks = Upload.UploadedFiles.keys
                If (UBound(ks) <> -1) Then
                    Dim fs
                    Set fs = Server.CreateObject("Scripting.FileSystemObject")
                    For Each fileKey in Upload.UploadedFiles.keys
                        ext = LCase(fs.GetExtensionName(Upload.UploadedFiles(fileKey).Path))
                        If Not rfInArray(ext, RF_UPLOAD_FILETYPE_PHOTO) Then
                            message = T("rf_lang_media_filetype_validate")
                            Exit For
                        End If
                        If CLng(Upload.UploadedFiles(fileKey).Length) > RF_UPLOAD_FILESIZE Then
                            message = T("rf_lang_media_filesize_validate")
                            Exit For
                        End If
                        Upload.UploadedFiles(fileKey).rename  filename&"."&ext, Server.MapPath(folder)
                        friendlink.sFilepath = folder & "/" & filename&"."&ext
                        Dim pp, w, h
                        Set pp=new cls_ImgInfo 
                        w=pp.imgW(Server.MapPath(friendlink.sFilepath))
                        h=pp.ImgH(Server.MapPath(friendlink.sFilepath))
                        friendlink.sResolution = w & "x" & h
                        Set pp=nothing
                    Next
                    Set fs = Nothing
                End If
        
                If  rfIsEmpty(message) Then
                    message = friendlink.save
                    If message = True Then
                        Response.Redirect("?" & QP("rf_action") & "=backend&module=ds.friendlinks&catID=" & friendlink.iCatId)
                    End If
                End If
            End If
        Else
            message = resultCsrf
        End If
   
        ks = Upload.UploadedFiles.keys
        If (UBound(ks) <> -1) Then
            For Each fileKey in Upload.UploadedFiles.keys
                Upload.UploadedFiles(fileKey).delete()
            Next 
        End If

        Set Upload = Nothing
    End If

    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        friendlink.iCatId = rfConvertInt(Request.QueryString("catID"))
        friendlink.iFontcolor = 1
        iID = rfConvertInt(Request.QueryString("id"))
        If iID>0 Then
            friendlink.init iID
                If friendlink.iID <= 0 Then
                    rfShowError "friendlink_info.asp", T("rf_lang_cannot_find_the_item")
                End If
        End If

    End If

    Set category = new cls_Category
    catgoryOptions = category.listByType(3)
    category.init(friendlink.iCatId)

%>
<!-- #include file="../../../templates/backend/datasource/friendlink_info.asp" -->
<%
Set friendlink = Nothing
Set category = Nothing
End Sub


%>
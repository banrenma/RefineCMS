<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<%
Sub rf_mod_ds_photo_info
    Dim media, category, catgoryOptions, iID, message, resultCsrf
    Set media = new cls_Media
    If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
        Dim folder, d, ext
        d = Now
        folder = RF_UPLOAD_FOLDER & Year(d) & "/" & Month(d) & "/" & Day(d)
        rfCreateFolder(folder)
        Dim Upload, ks, fileKey
        Set Upload = New FreeASPUpload
        Upload.Save Server.MapPath(folder)
        ' Upload.DumpData
        iID = rfConvertInt(Upload.FormElements.Item("id_hidden"))
        If iID>0 Then
            media.init iID
            If media.iID <= 0 Then
                rfShowError "photo_info.asp", T("rf_lang_cannot_find_the_item")
            End If
        End If

        media.iCatId = Upload.FormElements.Item("cat_id")
        media.sMedianame = Upload.FormElements.Item("medianame")
        media.sAlt = Upload.FormElements.Item("alt")
        media.sExcerpt = Upload.FormElements.Item("excerpt")
        media.sKeywords = Upload.FormElements.Item("keywords")
        media.sFilename = Upload.FormElements.Item("filename")
        resultCsrf = rfCheckCsrfByParam(Upload.FormElements.Item(LCase("csrfToken"))) 'FreeASPUpload把key做了小写处理 
        If resultCsrf = True Then
            ks = Upload.UploadedFiles.keys
            Dim photoPathTmp, phptoPathFinal
            If (UBound(ks) <> -1) Then
                Dim fs
                Set fs = Server.CreateObject("Scripting.FileSystemObject")
                For Each fileKey in Upload.UploadedFiles.keys
                    ext = LCase(fs.GetExtensionName(Upload.UploadedFiles(fileKey).Path))
                    If Not rfInArray(ext, RF_UPLOAD_FILETYPE_PHOTO) Then
                        message = T("rf_lang_media_filetype_validate")
                        Exit For
                    End If
                    If CLng(Upload.UploadedFiles(fileKey).Length) > RF_UPLOAD_FILESIZE Then
                        message = T("rf_lang_media_filesize_validate")
                        Exit For
                    End If
                    ' 防止重名的图片，还未在model验证时，就覆盖掉了被重名的图片
                    ' If rfFileExists(folder & "/" & media.sFilename&"."&ext) Then
                    '     message = T("rf_lang_media_filename_exists")
                    '     Exit For
                    ' End If
                    ' 图片地址为正式的，但图片文件名称加后缀"_tmp"，待model->save()成功后再去掉后缀。
                    ' 如果不成功，则删除该临时图片文件
                    photoPathTmp = folder & "/" & media.sFilename&"_tmp."&ext
                    ' phptoPathFinal = folder & "/" & media.sFilename&"."&ext
                    Upload.UploadedFiles(fileKey).rename  media.sFilename&"_tmp."&ext, Server.MapPath(folder)
                    media.sExt = ext
                    media.iFilesize = CLng(Upload.UploadedFiles(fileKey).Length)
                    media.sFilepath = folder & "/" & media.sFilename&"."&ext
          
                    Dim pp, w, h
                    Set pp = new cls_ImgInfo 
                    w=pp.imgW(Server.MapPath(photoPathTmp))
                    h=pp.ImgH(Server.MapPath(photoPathTmp))
                    media.sResolution = w & "x" & h
                    Set pp=Nothing
                Next
                Set fs = Nothing
            End If

            If  rfIsEmpty(message) Then
                message = media.save
                If message = True Then
                    If Not rfIsEmpty(photoPathTmp) Then
                        rfRenameFile photoPathTmp, media.sFilepath
                    End If
                    Response.Redirect("?" & QP("rf_action") & "=backend&module=ds.photos&catID=" & media.iCatId)
                Else
                    If (UBound(ks) <> -1) Then
                        rfRemoveFile(photoPathTmp)
                        media.sExt = ""
                        media.iFilesize = 0
                        media.sResolution = ""
                        media.sFilepath = ""
                    End If
                End If
            End If
        Else
            message = resultCsrf
        End If
   
        ks = Upload.UploadedFiles.keys
        If (UBound(ks) <> -1) Then
            For Each fileKey in Upload.UploadedFiles.keys
                Upload.UploadedFiles(fileKey).delete()
            Next 
        End If

        Set Upload = Nothing
    End If

    If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
        media.iCatId = rfConvertInt(Request.QueryString("catID"))
        iID = rfConvertInt(Request.QueryString("id"))
        If iID>0 Then
            media.init iID
        End If
    End If

    Set category = new cls_Category
    catgoryOptions = category.listByType(2)
    category.init(media.iCatId)

    ' media.iCatId = rfConvertInt(Request.QueryString("catID"))

%>
<!-- #include file="../../../templates/backend/datasource/photo_info.asp" -->
<%
Set media = Nothing
Set category = Nothing
End Sub


%>
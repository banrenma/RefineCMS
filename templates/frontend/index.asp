<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="{{refinecms_description}}">
    <meta name="keywords" content="{{refinecms_keywords}}">
    <title>{{refinecms_title}}</title>
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">

    <!-- Bootstrap -->
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/example.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="container-fluid">
  <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2 ad-top">{{广告示例}}</div>
  </div>
  <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <div class="my-logo">LOGO</div>
              </div>
              <div class="col-sm-6 col-md-8">
                {{一级导航-自定义}}
              </div>
            </div>
            <div class="row">
                <div class="ad-top">
                {{广告示例}}
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="my-slider">
                {{轮播图-首页-牧马人}}
                </div>
              </div>
              <div class="col-md-4">
                <h4>互联网新闻</h4>
                <div class="news-top-n">
                    {{首页中右部文章列表}}
                </div>
              </div>
              <div class="col-md-4">
                <h4>计算机学习</h4>
                <div class="news-top-n">
                    {{首页轮播图右侧文章列表（计算机常识）}}
                </div>
              </div>
            </div>
            <div class="row">
                <div class="ad-top">
                {{广告示例}}
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 col-md-8">
                {{首页中部-缩略图和标题-文章展示}}
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="ad-top">{{广告示例}}</div>
                <h4>行业新闻</h4>
                <div class="news-top-n">
                    {{首页中右部文章列表}}
                </div>
              
              </div>
            </div>

            <div class="row">
                <div class="ad-top">
                {{广告示例}}
              </div>
            </div>

            <div class="row">
                <h4>歼20美图欣赏</h4>
                <div>
                {{首页中部-歼20-图片展示-带详情页}}
              </div>
              <h4>阿尔卑斯风景图</h4>
                <div>
                {{首页中部-阿尔卑斯-图片展示-模态弹窗展示}}
              </div>
            </div>

            <div class="row">
                <div>
                    <h4>参考资料</h4>
                    <div class="my-referer-links">{{通用页底部友情链接-文字}}</div>
                    <h4>媒体链接</h4>
                    <div>{{首页中下部-友情链接-媒体链接-logo}}</div>
              </div>
            </div>

        </div>
  </div>
</div>



<footer class="bs-docs-footer">
  <div class="container">
    {{通用页底部友情链接-文字}}
    <p>RefineCMS 作者：木鱼（扣扣：伍肆捌捌肆壹捌陆壹）。 代码托管在 <a href="https://gitee.com/banrenma/RefineCMS" target="_blank">码云</a>。</p>
    <p>本项目源码受 <a rel="license" href="https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE" target="_blank">GPL</a>开源协议保护。</p>
    <p>数据库访问次数： {{refinecms_event_db_total_hits}}，数据库访问总时间：{{refinecms_event_db_total_time}} ms</p>
  </div>
</footer>


 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
  </body>
</html>
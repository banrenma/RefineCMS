﻿<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>出错了……<%=RF_CMS_NAME%></title>

    <!-- Bootstrap -->  
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/admin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid">
    <div class="row">
      <div class="page-header">
        <h1><%=RF_CMS_NAME%> <small><%=RF_HOMEPAGE_DESCRIPTION%></small></h1>
      </div>
    </div>

  <div class="row">

    <%If Err.Number <> 0 Then%>
        <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Error or Exception</h3>
          </div>
          <div class="panel-body">
            <ul>
              <li>Number: <%=Err.Number%></li>
              <li>Source: <%=Err.Source%></li>
              <li>Description: <%=Err.Description%></li>
            </ul>
          </div>
        </div>
    </div>
  <%End If%>

  </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </body>
</html>
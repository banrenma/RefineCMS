<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <meta name="description" content="{{refinecms_description}}">
    <meta name="keywords" content="{{refinecms_keywords}}">
    <title>{{refinecms_title}}</title>
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">

    <!-- Bootstrap -->
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/example.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<header class="navbar navbar-static-top bs-docs-nav" id="top">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="./" class="navbar-brand">{{refinecms_sitename}}</a>
    </div>
    <nav id="bs-navbar" class="collapse navbar-collapse">
      {{一级导航-Bootstrap风格}}
      <ul class="nav navbar-nav navbar-right">
        <li><a href="https://gitee.com/banrenma/RefineCMS" target="_blank">源码下载</a></li>
      </ul>
    </nav>
  </div>
</header>
<div class="bs-docs-header" id="content" tabindex="-1">
  <div class="container">
    <h1>关于</h1>
    <p>{{公司介绍面板显示}}</p>
  </div>
</div>
<div class="container bs-docs-container">
      <div class="row">
        <div class="col-md-8" role="main">
        {{标准面包屑}}
        {{refinecms}}
        <div class="banner">
            {{底部广告统计}}
        </div>
        </div>
        <div class="col-md-4" role="complementary">
            
            <h4>二级导航</h4>
            <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm">
                {{二级导航-Bootstrap风格}}
            </nav>

            <h4>林家栋轮播图</h4>
            {{林家栋轮播图}}

            <h4>互联网文章</h4>
            <div class="news-top-n">
            {{页面右侧TOP10-互联网}}
            </div>
            <h4>RefineCMS主页</h4>
            <div>{{我的码云挂件}}</div>
            <div>
              <a class="back-to-top" href="#top">返回顶部</a>
            </div>
            
        </div>
      </div>
</div>
<footer class="bs-docs-footer">
  <div class="container">
    {{通用页底部友情链接-文字}}
    <p>RefineCMS 作者：木鱼（扣扣：伍肆捌捌肆壹捌陆壹）。 代码托管在 <a href="https://gitee.com/banrenma/RefineCMS" target="_blank">码云</a>。<a href='https://gitee.com/banrenma/RefineCMS'><img src='https://gitee.com/banrenma/RefineCMS/widgets/widget_6.svg' alt='Fork me on Gitee'></img></a></p>
    <p>本项目源码受 <a rel="license" href="https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE" target="_blank">GPL</a>开源协议保护。</p>
    <p>数据库访问次数： {{refinecms_event_db_total_hits}}，数据库访问总时间：{{refinecms_event_db_total_time}} ms</p>
  </div>
</footer>


 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
  </body>
</html>
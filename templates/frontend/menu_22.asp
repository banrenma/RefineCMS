﻿<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=960" id="meta-viewport">
    <script>
    if (screen.width < 768) {
        document.getElementById('meta-viewport').setAttribute('content', 'width=768');
    }
    </script>

    <meta name="description" content="{{refinecms_description}}">
    <meta name="keywords" content="{{refinecms_keywords}}">
    <title>{{refinecms_title}}</title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

  <!-- IMPORT: yuilibrary.com -->
  <link rel="stylesheet" href="https://fonts.font.im/css?family=PT+Sans:400,700,400italic,700italic">
  <link href="assets/css/yuicss/cssgrids-min.css" rel="stylesheet">
  <link href="assets/css/yuicss/main-min.css" rel="stylesheet">
  <link href="assets/css/yuicss/prettify-min.css" rel="stylesheet">
  <link href="assets/css/yuicss/docs-min.css" rel="stylesheet">
  <link href="assets/css/yuicss/blog.css" rel="stylesheet">
  <!--如果自定义模板、或者不使用bootstrap.css，请引用这个css文件。它 存放了分页类所需要的样式。分页样式写死到了 RefineCMS 的 /libs/cls_patination.asp，因此这一块无法个性化 -->
  <link href="assets/css/refinecms-pagination.css" rel="stylesheet">

</head>

<body class="single single-post postid-5469 single-format-standard yui3-skin-sam">

  <div id="doc">
        <div id="hd" class="yui3-g">
    </div>
    <div id="bd" class="clearfix yui3-g">

    <div class="yui3-u-3-4" class="post-5469 post type-post status-publish format-standard hentry category-development category-performance" id="post-5469">

      <div class="content">


        {{面包屑-YUI}}

        <h1 class="yui3-u-7-8">计算机常识的自定义频道页示例</h1>

        <div class="entry yui3-u-1">


<div class="display-links">
{{计算机常识-YUI呈现方式}}
</div>

{{计算机常识-YUI呈现方式|Pager}}


      </div>
    </div>
    </div>




<div id="sidebar" class="yui3-u-1-4">
  <div class="liner">
        <div id="recent-posts-3" class="sidebox widget widget_recent_entries">    
          <div class="hd"><h2>二级导航</h2></div>
          <div class="bd">   
            {{标准二级导航-YUI}}
    </div>
  </div>

<div id="categories-2" class="sidebox widget widget_categories">
  <div class="hd"><h2>Categories</h2></div>
  <div class="bd">   
    {{军事动态以YUI样式呈现在右侧}}
  </div>
</div>

</div></div>
    </div><!-- /#bd -->
    <div id="ft">
      <footer>
        <div class="copyright">
      &copy; 2018 {{refinecms_sitename}}   样式来自 YUI blog，仅供学习研究之用。     </div>
      </footer></div></div><!-- /#doc -->

</body>
</html>

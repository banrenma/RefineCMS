<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>RefineCMS</title>

    <!-- Bootstrap -->
    <link href="/resource/bootstrap-3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
<div class="container-fluid">
    <div class="row">
        {refinecms:nav-main(26)}
        {refinecms:nav-main(25)}
        {refinecms:nav-main(24)}
      
        <br />
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>高尔夫轮播图</h3>
            {refinecms:slider-top-n(25, 3, 8, desc, 61)}
            <h3>二级导航</h3>
            {refinecms:nav-sub(21)}
            <h3>最新 3 条</h3>
              {refinecms:list-top-n(31, 1, 2, desc, 17)}
            <h3>点击最高的 4 篇</h3>
              {refinecms:list-top-n(31, 6, 12, hits_desc, 17)}
            <h3>最早发布</h3>
              {refinecms:list-top-n(31, 2, 24, 'asc', 17)}
              <h3>高尔夫轮播图</h3>
            {refinecms:slider-top-n(25, 15, 8, desc, 61)}
        </div>
        <div class="col-md-9">
        {refinecms:breadcrumb(20)}
        {refinecms:media-top-n(25, 4, 8, asc, 60)}
        <div class="clearfix"></div>
        
        {refinecms:content()}
        <div class="clearfix"></div>
        {refinecms:links(2, 50, 10, desc, 64)}
        {refinecms:slider-top-n(25, 30, 8, desc, 63)}
        {refinecms:links(2, 50, 10, desc, 65)}

        </div>
    </div>
<div>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/resource/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    
  </body>
</html>
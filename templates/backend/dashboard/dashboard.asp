<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li class="active"><%=T("rf_lang_detail")%></li>
    </ol>
  </div>

  <div class="col-md-2">
      <p><a href="?<%=QP("rf_action")%>=backend&module=dashboard&opt=flush_cache&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_are_you_sure")%>')){return true;}return false;}" class="btn btn-danger" role="button"><%=T("rf_lang_flush_cache")%></a></p>
  </div>
</div>
</div>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab_profile" aria-controls="tab_profile" role="tab" data-toggle="tab"><%=T("rf_lang_sys_tab_profile")%></a></li>
    <li role="presentation"><a href="#tab_server" aria-controls="tab_server" role="tab" data-toggle="tab"><%=T("rf_lang_sys_tab_server_info")%></a></li>
    <li role="presentation"><a href="#tab_component" aria-controls="tab_component" role="tab" data-toggle="tab"><%=T("rf_lang_sys_tab_components")%></a></li>
    <li role="presentation"><a href="#tab_application" aria-controls="tab_application" role="tab" data-toggle="tab"><%=T("rf_lang_sys_tab_application")%></a></li>
    <li role="presentation"><a href="#tab_session" aria-controls="tab_session" role="tab" data-toggle="tab"><%=T("rf_lang_sys_tab_session")%></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_profile">
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><%=T("rf_lang_env_debug")%></th>
          <td><strong><%=ENV_DEBUG%></strong></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_sys_page_cache")%></th>
          <td><strong><%=RF_PAGE_CACHE%></strong></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_sys_page_cache")%></th>
          <td><%=fmtFileSize(cacheSize)%></td>
        </tr>
        
        <tr>
          <th scope="row"><%=T("rf_lang_timewatcher")%></th>
          <td><%=DEBUG_TIMEWATCHER%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_datasource_category_essays")%></th>
          <td><%=countEssay%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_datasource_category_photos")%></th>
          <td><%=countPhoto%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_datasource_category_friendlinks")%></th>
          <td><%=countFriendlink%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_datasource_category_singlepage")%></th>
          <td><%=countSinglepage%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_presentation")%></th>
          <td><%=countPresentation%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_tag")%></th>
          <td><%=countTag%>&nbsp;<%=T("rf_lang_unit_number")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_database")%></th>
          <td><%=RF_DB_DATABASE%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_upload_max_filesize")%></th>
          <td><%=fmtFileSize(RF_UPLOAD_FILESIZE)%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_upload_filetype")%></th>
          <td><%=Join(RF_UPLOAD_FILETYPE_PHOTO, " | ")%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_uploads")%></th>
          <td><%=RF_UPLOAD_FOLDER%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_uploads_size")%></th>
          <td><%=uploadSize%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_language")%></th>
          <td><%=RF_LANGUAGE%></td>
        </tr>
        
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_server">
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
        <tr>
          <th scope="row">Server Name</th>
          <td><%=Request.ServerVariables("SERVER_NAME")%></td>
        </tr>
        <tr>
          <th scope="row">IP</th>
          <td><%=rfUserIP()%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_sys_OS")%></th>
          <td><%=os%></td>
        </tr>
        <tr>
          <th scope="row"><%=T("rf_lang_sys_NUMBER_OF_PROCESSORS")%></th>
          <td><%=cpuNumber%></td>
        </tr>
        <tr>
          <tr>
          <th scope="row"><%=T("rf_lang_sys_PROCESSOR_IDENTIFIER")%></th>
          <td><%=cpuID%></td>
        </tr>
        <tr>
          <tr>
          <tr>
          <th scope="row"><%=T("rf_lang_sys_Application_number")%></th>
          <td><%=Application.Contents.Count%></td>
        </tr>
        <tr>
          <tr>
          <tr>
          <th scope="row"><%=T("rf_lang_sys_Session_number")%></th>
          <td><%=Session.Contents.Count%></td>
        </tr>
        <tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_component">
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
        <% Dim i, tmpObj %>
        <% For i=0 To Ubound(components) %>
        <% tmpObj = rfCreateObject(components(i))%>
        <tr>
          <th scope="row"><%=components(i)%></th>
          <td><%=fmtReady(tmpObj(0))%></td>
        </tr>
        <% Next %>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_application">
      <h4><%=T("rf_lang_sys_other_cache")%></h4>
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
      <%=trOtherStr%>
      </tbody>
    </table>

    <h4><%=T("rf_lang_sys_page_cache")%></h4>
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
      <%=trCacheStr%>
      </tbody>
    </table>
       
    <h4><%=T("rf_lang_sys_lang_cache")%></h4>
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
        <%=trLangStr%>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_session">
      <table class="table table-bordered table-condensed table-hover">
        <caption></caption>
      <tbody>
        <% Dim keySession %>
        <% For Each keySession in Session.Contents%>
        <tr>
          <th scope="row"><%=keySession%></th>
          <td><%=fmtObjectToString(Session.Contents(keySession))%></td>
        </tr>
        <% Next %>
      </tbody>
    </table>
    </div>
  </div>

</div>


<!-- #include file="../foot.asp" -->
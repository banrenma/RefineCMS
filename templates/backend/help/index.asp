<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=help"><%=T("rf_lang_help")%></a></li>
      <li class="active"><%=T("rf_lang_detail")%></li>
    </ol>
  </div>

</div>
</div>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab_systag" aria-controls="tab_systag" role="tab" data-toggle="tab"><%=T("rf_lang_tag_system")%></a></li>
    <li role="presentation" class="dropdown">
        <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false"><%=T("rf_lang_presentation")%> <span class="caret"></span></a>
        <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
          <li class=""><a href="#essaylist" role="tab" id="essaylist-tab" data-toggle="tab" aria-controls="essaylist" aria-expanded="false"><%=T("rf_lang_presentation_pretype_essay_list")%></a></li>
          <li class=""><a href="#essayinfo" role="tab" id="essayinfo-tab" data-toggle="tab" aria-controls="essayinfo" aria-expanded="true"><%=T("rf_lang_presentation_pretype_essay_info")%></a></li>
          <li class=""><a href="#photolist" role="tab" id="photolist-tab" data-toggle="tab" aria-controls="photolist" aria-expanded="true"><%=T("rf_lang_presentation_pretype_photo_list")%></a></li>
          <li class=""><a href="#photoinfo" role="tab" id="photoinfo-tab" data-toggle="tab" aria-controls="photoinfo" aria-expanded="true"><%=T("rf_lang_presentation_pretype_photo_info")%></a></li>
          <li class=""><a href="#flexslider" role="tab" id="flexslider-tab" data-toggle="tab" aria-controls="flexslider" aria-expanded="true"><%=T("rf_lang_presentation_pretype_photo_flexslider")%></a></li>
          <li class=""><a href="#singlepage" role="tab" id="singlepage-tab" data-toggle="tab" aria-controls="singlepage" aria-expanded="true"><%=T("rf_lang_presentation_pretype_singlepage")%></a></li>
          <li class=""><a href="#breadcrumb" role="tab" id="breadcrumb-tab" data-toggle="tab" aria-controls="breadcrumb" aria-expanded="true"><%=T("rf_lang_presentation_pretype_breadcrumb")%></a></li>
          <li class=""><a href="#mainnav" role="tab" id="mainnav-tab" data-toggle="tab" aria-controls="mainnav" aria-expanded="true"><%=T("rf_lang_presentation_pretype_mainnav")%></a></li>
          <li class=""><a href="#subnav" role="tab" id="subnav-tab" data-toggle="tab" aria-controls="subnav" aria-expanded="true"><%=T("rf_lang_presentation_pretype_subnav")%></a></li>
          <li class=""><a href="#friendlink" role="tab" id="friendlink-tab" data-toggle="tab" aria-controls="friendlink" aria-expanded="true"><%=T("rf_lang_presentation_pretype_friendlink")%></a></li>
          <li role="separator" class="divider"></li>
          <li class=""><a href="#logic" role="tab" id="logic-tab" data-toggle="tab" aria-controls="logic" aria-expanded="true"><%=T("rf_lang_presentation_logic")%></a></li>
          <li role="separator" class="divider"></li>
          <li class=""><a href="#modifier" role="tab" id="modifier-tab" data-toggle="tab" aria-controls="modifier" aria-expanded="true"><%=T("rf_lang_presentation_modifier")%></a></li>
        </ul>
      </li>
      <li role="presentation"><a href="#tab_reference" aria-controls="tab_reference" role="tab" data-toggle="tab"><%=T("rf_lang_reference")%></a></li>
      <li role="presentation"><a href="#tab_iis" aria-controls="tab_iis" role="tab" data-toggle="tab"><%=T("rf_lang_iis")%></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">

    <div role="tabpanel" class="tab-pane fade in active" id="tab_systag">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row">站点名称</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_sitename}}" onfocus="javascript:this.select()" readonly /></td>
          <td>在config.asp, RF_CMS_NAME</td>
        </tr>
        <tr>
          <th scope="row">网页执行事件及事件统计</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_event_all}}" onfocus="javascript:this.select()" readonly /></td>
          <td>由于需要尽可能统计到事件的起止点，该标签会被系统最后解析，与其在模板中的位置无关。</td>
        </tr>
        <tr>
          <th scope="row">数据库访问总次数</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_event_db_total_hits}}" onfocus="javascript:this.select()" readonly /></td>
          <td>当前网页打开时，数据库访问总次数。</td>
        </tr>
        <tr>
          <th scope="row">数据库访问总时间</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_event_db_total_time}}" onfocus="javascript:this.select()" readonly /></td>
          <td>当前网页打开时，数据库访问总时间。</td>
        </tr>
        <tr>
          <th scope="row">网页  &lt;title&gt;</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_title}}" onfocus="javascript:this.select()" readonly /></td>
          <td>显示在浏览器标签，在网页源代码也可查看。</td>
        </tr>
        <tr>
          <th scope="row">网页  meta  description</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_description}}" onfocus="javascript:this.select()" readonly /></td>
          <td>在网页源代码可查看，有利于 SEO</td>
        </tr>
        <tr>
          <th scope="row">网页  meta  keywords</th>
          <td><input type="text" class="form-control input-sm" value="{{refinecms_keywords}}" onfocus="javascript:this.select()" readonly /></td>
          <td>在网页源代码可查看，有利于 SEO</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="essaylist">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>文章标题</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{url}</code></th>
          <td>详情页链接（文章列表有，详情就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{view}</code></th>
          <td>浏览次数（文章列表有，单页就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
        <tr>
          <th scope="row"><code>{editor}</code></th>
          <td>最后更新的管理员</td>
        </tr>
        <tr>
          <th scope="row"><code>{updated}</code></th>
          <td>最后更新时间</td>
        </tr>
        <tr>
          <th scope="row"><code>{created}</code></th>
          <td>创建时间</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="essayinfo">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>文章标题</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{url}</code></th>
          <td>详情页链接（文章列表有，详情就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{content}</code></th>
          <td>正文</td>
        </tr>
        <tr>
          <th scope="row"><code>{view}</code></th>
          <td>浏览次数（文章列表有，单页就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
        <tr>
          <th scope="row"><code>{editor}</code></th>
          <td>最后更新的管理员</td>
        </tr>
        <tr>
          <th scope="row"><code>{updated}</code></th>
          <td>最后更新时间</td>
        </tr>
        <tr>
          <th scope="row"><code>{created}</code></th>
          <td>创建时间</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="photolist">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>图片名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{url}</code></th>
          <td>详情页链接（文章列表有，详情就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{alt}</code></th>
          <td>图片alt属性</td>
        </tr>
        <tr>
          <th scope="row"><code>{resolution}</code></th>
          <td>分辨率</td>
        </tr>
        <tr>
          <th scope="row"><code>{filesize}</code></th>
          <td>文件大小</td>
        </tr>
        <tr>
          <th scope="row"><code>{view}</code></th>
          <td>浏览次数（文章列表有，单页就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
        <tr>
          <th scope="row"><code>{editor}</code></th>
          <td>最后更新的管理员</td>
        </tr>
        <tr>
          <th scope="row"><code>{updated}</code></th>
          <td>最后更新时间</td>
        </tr>
        <tr>
          <th scope="row"><code>{created}</code></th>
          <td>创建时间</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="photoinfo">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>图片名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{alt}</code></th>
          <td>图片alt属性</td>
        </tr>
        <tr>
          <th scope="row"><code>{resolution}</code></th>
          <td>分辨率</td>
        </tr>
        <tr>
          <th scope="row"><code>{filesize}</code></th>
          <td>文件大小</td>
        </tr>
        <tr>
          <th scope="row"><code>{view}</code></th>
          <td>浏览次数（文章列表有，单页就不需要了）</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
        <tr>
          <th scope="row"><code>{editor}</code></th>
          <td>最后更新的管理员</td>
        </tr>
        <tr>
          <th scope="row"><code>{updated}</code></th>
          <td>最后更新时间</td>
        </tr>
        <tr>
          <th scope="row"><code>{created}</code></th>
          <td>创建时间</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="flexslider">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>图片名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{alt}</code></th>
          <td>图片alt属性</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="singlepage">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{title}</code></th>
          <td>文章标题</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>一般和变量修饰器搭配使用，比如 <code>{img|resize:"600x600"}</code></td>
        </tr>
        <tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>图片路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{excerpt}</code></th>
          <td>摘要</td>
        </tr>
        <tr>
          <th scope="row"><code>{content}</code></th>
          <td>正文</td>
        </tr>
        <tr>
          <th scope="row"><code>{editor}</code></th>
          <td>最后更新的管理员</td>
        </tr>
        <tr>
          <th scope="row"><code>{updated}</code></th>
          <td>最后更新时间</td>
        </tr>
        <tr>
          <th scope="row"><code>{created}</code></th>
          <td>创建时间</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="breadcrumb">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{menu}</code></th>
          <td>栏目名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{link}</code></th>
          <td>栏目链接</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="mainnav">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{menu}</code></th>
          <td>栏目名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{link}</code></th>
          <td>栏目链接</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="subnav">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{menu}</code></th>
          <td>栏目名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{link}</code></th>
          <td>栏目链接</td>
        </tr>
      </tbody>
    </table>
    </div>

  <div role="tabpanel" class="tab-pane fade" id="friendlink">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{sitename}</code></th>
          <td>网站名称</td>
        </tr>
        <tr>
          <th scope="row"><code>{siteurl}</code></th>
          <td>网站链接</td>
        </tr>
        <tr>
          <th scope="row"><code>{img(200x100)}</code></th>
          <td>控制width和height属性值，宽不超过200，高不超过100，等比例显示</td>
        </tr>
        <tr>
          <th scope="row"><code>{img}</code></th>
          <td>不增加width和height属性</td>
        </tr>
        <tr>
          <th scope="row"><code>{src}</code></th>
          <td>LOGO路径</td>
        </tr>
        <tr>
          <th scope="row"><code>{category}</code></th>
          <td>分类</td>
        </tr>
      </tbody>
    </table>
    </div>

  <div role="tabpanel" class="tab-pane fade" id="logic">
      <table class="table table-bordered table-condensed table-hover">
      <caption></caption>
      <tbody>
        <tr>
          <th scope="row"><code>{empty}</code> <code>{/empty}</code></th>
          <td>列表数据为空时</td>
          <td><code>{empty}</code> 和 <code>{/empty}</code> 成对出现</td>
          <td>与 <code>{loop}</code> 和 <code>{/loop}</code> 并列使用</td>
          <td>支持的呈现方式有：文章列表、图片列表、面包屑、一级导航、二级导航、友情链接</td>
        </tr>
        <tr>
          <th scope="row"><code>{loop}</code> <code>{/loop}</code></th>
          <td>列表数据不为空时</td>
          <td><code>{loop}</code> 和 <code>{/loop}</code> 成对出现</td>
          <td>与 <code>{empty}</code> 和 <code>{/empty}</code> 并列使用</td>
          <td>支持的呈现方式有：文章列表、图片列表、面包屑、一级导航、二级导航、友情链接</td>
        </tr>
        <tr>
          <th scope="row"><code>{active}</code> <code>{/active}</code></th>
          <td>高亮时，且只能出现在<code>{loop}</code> <code>{/loop}</code>里。</td>
          <td><code>{active}</code> 和 <code>{/active}</code> 成对出现</td>
          <td>与 <code>{normal}</code> 和 <code>{/normal}</code> 并列使用</td>
          <td>支持的呈现方式只有：一级导航、二级导航</td>
        </tr>
        <tr>
          <th scope="row"><code>{normal}</code> <code>{/normal}</code></th>
          <td>非高亮时，且只能出现在<code>{loop}</code> <code>{/loop}</code>里。</td>
          <td><code>{normal}</code> 和 <code>{/normal}</code> 成对出现</td>
          <td>与 <code>{active}</code> 和 <code>{/active}</code> 并列使用</td>
          <td>支持的呈现方式只有：一级导航、二级导航</td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="modifier">
      <table class="table table-bordered table-condensed table-hover">
      <caption>注意：RefineCMS 的变量修饰器不支持连着使用！</caption>
      <tbody>
        <tr>
          <th scope="row">resize</th>
          <td>按照规定的分辨率，对原图做缩略，无论宽高，均不得超过所规定的分辨率</td>
          <td>
            <p>注意：只能用于 <code>{img}</code>，宽高值可以不同，比如，可以传入"200x150"</p>
            <p>例子：如果使用了它、而没有提供参数，缺失的参数值为"200x200"</p>
            <p><code>{img}</code>：对应于 &lt;img src="/uploads/2018/7/31/quan-xin-yi-dai-mu-ma-ren-2018kuan.jpg"&gt;</p>
            <p><code>{img|resize}</code>：对应于 &lt;img src="/uploads/2018/7/31/quan-xin-yi-dai-mu-ma-ren-2018kuan.jpg" width="200" height="150"&gt;</p>
            <p><code>{img|resize:"600x600"}</code>：对应于 &lt;img src="/uploads/2018/7/31/quan-xin-yi-dai-mu-ma-ren-2018kuan.jpg" width="600" height="450"&gt;
&gt;</p>
          </td>
        </tr>
        <tr>
          <th scope="row">date_format</th>
          <td>格式化日期时间</td>
          <td>
            <p>例子：文章发布的日期对应于 <code>{updated}</code>，默认的输出格式为 "y-m-d h:i:s"，比如 2018-07-15 09:30:25</p>
            <p><code>{updated|date_format}</code>：同上</p>
            <p><code>{updated|date_format:"y/m/d h:i:s"}</code>：2018/07/15 09:30:25</p>
            <p><code>{updated|date_format:"h:i"}</code>：09:30</p>
          </td>
        </tr>
        <tr>
          <th scope="row">truncate</th>
          <td>截取字符串到指定长度</td>
          <td>
            <p>例子：如果使用了它、而没有提供参数，将显示全部</p>
            <p>注意：如果使用了它，变量里如果含有 html 标记，则会全部清除！比如：设置为蓝色显示的某个友情链接的站点名称，将失去蓝色设置</p>
            <p><code>{title}</code>：正常显示标题</p>
            <p><code>{title|truncate}</code>：由于没有提供参数，就显示全部</p>
            <p><code>{title|truncate:20}</code>：截取前20个字符，末尾不附加字符串</p>
            <p><code>{title|truncate:20:"..."}</code>：截取前20个字符，末尾附加传递的的"..."，该字符长度不会被计算到截取长度内。</p>
          </td>
        </tr>
        <tr>
          <th scope="row">cat</th>
          <td>连接后面的字符串</td>
          <td>
            <p>例子：title 值为"我是标题"</p>
            <p><code>{title}</code>：正常显示标题</p>
            <p><code>{title|cat}</code>：由于没有提供参数，正常显示标题</p>
            <p><code>{title|cat:" By RefineCMS"}</code>："我是标题 By RefineCMS"</p>
          </td>
        </tr>
        <tr>
          <th scope="row">strip_tags</th>
          <td>去除标记等任何包含在< 和 >中间的字符。</td>
          <td>
            <p>例子：content 值为"我是&lt;strong&gt;正文&lt;/strong&gt;"</p>
            <p><code>{content}</code>："我是&lt;strong&gt;正文&lt;/strong&gt;"</p>
            <p><code>{content|strip_tags}</code>："我是正文"</p>
          </td>
        </tr>
      </tbody>
    </table>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_reference">
      <ul>
        <% If IsNull(friendlinkList) Then %>
            <li><%=T("rf_lang_list_is_empty")%></li>
        <% Else %>
            <% Dim j %>
            <% For j=UBound(friendlinkList, 2) to LBound(friendlinkList, 2) Step -1 %>
            <li><a href="<%=friendlinkList(2, j) %>" target="_blank"><%=friendlinkList(1, j) %></a></li>
            <% Next %>
        <% End If %> 
      </ul>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_iis">
      <p>下面两张截图，方便配置站点。</p>
      <p><a href="./uploads/2018/8/27/IIS-zhan-dian-pei-zhi-zhi-ASP.png" target="_blank"><img src="./uploads/2018/8/27/IIS-zhan-dian-pei-zhi-zhi-ASP.png" width="500px" /></a></p>
      <p><a href="./uploads/2018/8/27/IIS-ying-yong-cheng-xu-chi-zhi-zhan-dian-pei-zhi.png" target="_blank"><img src="./uploads/2018/8/27/IIS-zhan-dian-pei-zhi-zhi-ASP.png" width="500px" /></a></p>
    </div>

  </div>

</div>


<!-- #include file="../foot.asp" -->
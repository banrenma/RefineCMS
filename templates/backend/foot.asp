<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

        </div>

            </div>
        </div>
    </div>
        <footer class="my-footer">
            <p>RefineCMS 作者：木鱼（扣扣：伍肆捌捌肆壹捌陆壹）。 代码托管在 <a href="https://gitee.com/banrenma/RefineCMS" target="_blank">码云</a>。<a href='https://gitee.com/banrenma/RefineCMS'><img src='https://gitee.com/banrenma/RefineCMS/widgets/widget_6.svg' alt='Fork me on Gitee'></img></a></p>
            <p>本项目源码受 <a rel="license" href="https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE" target="_blank">GPL</a>开源协议保护。</p>
            <p>数据库访问次数： <%=g_timerWatcher.DbTotalHits%>，数据库访问总时间：<%=g_timerWatcher.DbTotalTime%> ms</p>
            <%g_timerWatcher.clearLastLog%>
        </footer>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.cookie.js"></script>
    <script src="assets/js/jquery-treeview.js"></script>
  </body>
</html>
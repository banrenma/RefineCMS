<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.config"><%=T("rf_lang_config_presentation")%></a></li>
      <li class="active"><%=T("rf_lang_edit")%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>

      <div class="form-group">
        <label for="listnum" class="col-md-2 control-label"><%=T("rf_lang_tag_listnum")%></label>
        <div class="col-md-3">
        <input type="number" class="form-control" name="listnum" value="<%=tagTextArr(0)%>" min="1" max="100" step="1">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
        <label for="showorder" class="col-md-2 control-label"><%=T("rf_lang_tag_order")%></label>
        <div class="col-md-3">
        <select multiple class="form-control" name="showorder" id="showorder">
          <option value="desc"<%If tagTextArr(1)="desc"  Then%> selected<%End If%>><%=T("rf_lang_tag_order_id_desc")%></option>
          <option value="asc"<%If tagTextArr(1)="asc"  Then%> selected<%End If%>><%=T("rf_lang_tag_order_id_asc")%></option>
        </select>
        </div>
      </div>
    <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_pagination")%></label>
        <div class="checkbox col-md-offset-2">
          <div class="radio">
            <label>
              <input type="radio" name="pagination" value="page_number_big"<%If tagTextArr(2)="page_number_big" Then %> checked<% End If %>>
              <%=T("rf_lang_pagination_page_number_big")%>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="pagination" value="page_number_normal"<%If tagTextArr(2)="page_number_normal" Then %> checked<% End If %>>
              <%=T("rf_lang_pagination_page_number_normal")%>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="pagination" value="page_number_small"<%If tagTextArr(2)="page_number_small" Then %> checked<% End If %>>
              <%=T("rf_lang_pagination_page_number_small")%>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="pagination" value="page_turning_normal"<%If tagTextArr(2)="page_turning_normal" Then %> checked<% End If %>>
              <%=T("rf_lang_pagination_page_turning_normal")%>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="pagination" value="page_turning_align"<%If tagTextArr(2)="page_turning_align" Then %> checked<% End If %>>
              <%=T("rf_lang_pagination_page_turning_align")%>
            </label>
          </div>
        </div>
    </div>

      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_essay_list")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(essaylistPresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k %>
          <% For k=LBound(essaylistPresentationList, 2) to UBound(essaylistPresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="essay_list_pre_id" value="<%=essaylistPresentationList(0, k) %>"<%If essaylistPresentationList(0, k)=rfConvertInt(tagTextArr(3)) Then %> checked<% End If %>>
              <%=essaylistPresentationList(1, k) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_essay_info")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(essayinfoPresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim j2 %>
          <% For j2=LBound(essayinfoPresentationList, 2) to UBound(essayinfoPresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="essay_info_pre_id" value="<%=essayinfoPresentationList(0, j2) %>"<%If essayinfoPresentationList(0, j2)=rfConvertInt(tagTextArr(4)) Then %> checked<% End If %>>
              <%=essayinfoPresentationList(1, j2) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_photo_list")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(photolistPresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k2 %>
          <% For k2=LBound(photolistPresentationList, 2) to UBound(photolistPresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="photo_list_pre_id" value="<%=photolistPresentationList(0, k2) %>"<%If photolistPresentationList(0, k2)=rfConvertInt(tagTextArr(5)) Then %> checked<% End If %>>
              <%=photolistPresentationList(1, k2) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_photo_info")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(photoinfoPresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k3 %>
          <% For k3=LBound(photoinfoPresentationList, 2) to UBound(photoinfoPresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="photo_info_pre_id" value="<%=photoinfoPresentationList(0, k3) %>"<%If photoinfoPresentationList(0, k3)=rfConvertInt(tagTextArr(6)) Then %> checked<% End If %>>
              <%=photoinfoPresentationList(1, k3) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>
  
      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_singlepage")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(singlepagePresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k4 %>
          <% For k4=LBound(singlepagePresentationList, 2) to UBound(singlepagePresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="singlepage_pre_id" value="<%=singlepagePresentationList(0, k4) %>"<%If singlepagePresentationList(0, k4)=rfConvertInt(tagTextArr(7)) Then %> checked<% End If %>>
              <%=singlepagePresentationList(1, k4) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

    
      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype_friendlink")%> - <%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(friendlinkPresentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k8 %>
          <% For k8=LBound(friendlinkPresentationList, 2) to UBound(friendlinkPresentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="friendlink_pre_id" value="<%=friendlinkPresentationList(0, k8) %>"<%If friendlinkPresentationList(0, k8)=rfConvertInt(tagTextArr(8)) Then %> checked<% End If %>>
              <%=friendlinkPresentationList(1, k8) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>


      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
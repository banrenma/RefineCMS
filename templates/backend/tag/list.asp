<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list"><%=T("rf_lang_tag")%></a></li>
      <% If tagtype > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=<%=tagtype%>"><%=fmtPresentationType(tagtype)%></a></li>
      <% End If %>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>
  <% If tagtype > 0 Then %>
  <div class="col-md-2">
      <p><a href="?<%=QP("rf_action")%>=backend&module=<%=rfTagType2Module(tagtype)%>" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
  </div>
  <% End If %>
</div>
</div>


<% If IsNull(tagList) Then %>
    <p><%=T("rf_lang_list_is_empty")%></p>
<% Else %>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th><%=T("rf_lang_tag_tagname")%></th>
                  <th><%=T("rf_lang_presentation_pretype")%></th>
                  <th><%=T("rf_lang_tag_tagtext")%></th>
                  <% If tagtype=1 Or tagtype=3 Then %>
                  <th><%=T("rf_lang_pagination_tag")%></th>
                  <% End If %>
                  <th><%=T("rf_lang_detail")%>/<%=T("rf_lang_tpl")%></th>
                  <th><%=T("rf_lang_updated_at")%></th>
                  <th><%=T("rf_lang_operater")%></th>
                  <th><%=T("rf_lang_opt")%></th>
                </tr>
              </thead>
              <tbody>
              <% Dim i %>
              <% For i=LBound(tagList, 2) to UBound(tagList, 2) %>
                <tr>
                  <td><%=tagList(0, i) %></td>
                  <td><%=tagList(1, i) %></td>
                  <td><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=<%=tagList(2, i)%>"><%=fmtPresentationType(tagList(2, i)) %></a></td>
                  <td><input type="text" class="form-control input-sm" value="{{<%=tagList(1, i) %>}}" onfocus="javascript:this.select()" readonly /></td>
                  <% If tagtype=1 Or tagtype=3 Then %>
                  <td><input type="text" class="form-control input-sm" value="{{<%=tagList(1, i) %>|Pager}}" onfocus="javascript:this.select()" readonly /></td>
                  <% End If %>
                  <td>
                  <% If  tagList(2, i)=1 Or  tagList(2, i)=3 Then %>
                      <% If  tagList(7, i)=1 Then %>
                          <%=T("rf_lang_tpl_common")%>
                      <% ElseIf tagList(7, i)=2 Then %>
                          <%="info_" & tagList(0, i) & ".asp"%>
                      <% Else %>
                          -
                      <% End If %>
                  <% Else %>
                      -
                  <% End If %>
                  </td>
                  <td><%=tagList(5, i) %></td>
                  <td><%=tagList(4, i) %></td>
                  <td>
                         <a href="?<%=QP("rf_action")%>=backend&module=<%=rfTagType2Module(tagList(2, i))%>&tag_name=<%=tagList(1, i) %>"><%=T("rf_lang_edit")%></a>
                         | 
                        <a href="?<%=QP("rf_action")%>=backend&module=tag.list&opt=del&id=<%=tagList(0, i) %><%If tagtype>0 Then Response.Write "&tagtype="&tagtype%>&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_are_you_sure")%>')){return true;}return false;}"><%=T("rf_lang_delete")%></a>
                  </td>
                </tr>
                <% Next %>

              </tbody>
            </table>
          </div>
<% End If %>   
<div class="row"><%=showPage%></div>
<!-- #include file="../foot.asp" -->
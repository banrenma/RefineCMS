<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list"><%=T("rf_lang_tag")%></a></li>
      <% If tag.iTagType > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=<%=tag.iTagType%>"><%=fmtPresentationType(tag.iTagType)%></a></li>
      <% End If %>
      <li class="active"><%If tag.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="tag_name" class="col-md-2 control-label"><%=T("rf_lang_tag_tagname")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="tag_name" name="tag_name" value="<%=tag.sTagName%>" placeholder="<%=T("rf_lang_tag_tagname_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=tag.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
        <label for="cat_id" class="col-md-2 control-label"><%=T("rf_lang_content_from")%></label>
        <div class="col-md-3">
          <% If IsNull(singlepageList) Then %>
          <%=T("rf_lang_content_from_is_empty")%>
          <%Else %>
        <select multiple class="form-control" name="cat_id" id="cat_id">
          <% Dim i %>
              <% For i=LBound(singlepageList, 2) to UBound(singlepageList, 2) %>
          <option value="<%=singlepageList(0, i) %>"<%If rfConvertInt(tagTextArr(0))=singlepageList(0, i)  Then%> selected<%End If%>><%=singlepageList(1, i) %></option>
        <% Next %>
        </select>
        <% End If %>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(presentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k %>
          <% For k=LBound(presentationList, 2) to UBound(presentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="singlepage_pre_id" value="<%=presentationList(0, k) %>"<%If presentationList(0, k)=rfConvertInt(tagTextArr(1)) Then %> checked<% End If %>>
              <%=presentationList(1, k) %>
            </label>
            - [<a href="?<%=QP("rf_action")%>=backend&module=presentation.info&id=<%=presentationList(0, k) %>"><ins><%=T("rf_lang_detail")%></ins></a>]
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>
    
      <div class="form-group">
          <label class="col-md-2 control-label"></label>
          <div class="col-md-4">
          <ul class="list-unstyled">
            <li><%=T("rf_lang_operater")%>: <%=tag.sUsername%></li>
            <li><%=T("rf_lang_updated_at")%>: <%=tag.dUpdatedAt%></li>
            <li><%=T("rf_lang_created_at")%>: <%=tag.dCreatedAt%></li>
          </ul>
          </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
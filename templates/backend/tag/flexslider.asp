<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list"><%=T("rf_lang_tag")%></a></li>
      <% If tag.iTagType > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=<%=tag.iTagType%>"><%=fmtPresentationType(tag.iTagType)%></a></li>
      <% End If %>
      <li class="active"><%If tag.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="tag_name" class="col-md-2 control-label"><%=T("rf_lang_tag_tagname")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="tag_name" name="tag_name" value="<%=tag.sTagName%>" placeholder="<%=T("rf_lang_tag_tagname_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=tag.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
        <label for="cat_id" class="col-md-2 control-label"><%=T("rf_lang_content_from")%></label>
        <div class="col-md-3">
          <% If IsNull(categoryList) Then %>
          <%=T("rf_lang_content_from_is_empty")%>
          <%Else %>
        <select multiple class="form-control" name="cat_id" id="cat_id">
          <% Dim i %>
              <% For i=LBound(categoryList, 2) to UBound(categoryList, 2) %>
          <option value="<%=categoryList(0, i) %>"<%If CInt(tagTextArr(0))=categoryList(0, i)  Then%> selected<%End If%>><%=categoryList(1, i) %></option>
        <% Next %>
        </select>
        <% End If %>
        </div>
      </div>
      <div class="form-group">
        <label for="listnum" class="col-md-2 control-label"><%=T("rf_lang_tag_listnum")%></label>
        <div class="col-md-3">
        <input type="number" class="form-control" name="listnum" value="<%=tagTextArr(1)%>" min="1" max="1000" step="1">
        </div>
      </div>
      <div class="form-group">
        <label for="showorder" class="col-md-2 control-label"><%=T("rf_lang_tag_order")%></label>
        <div class="col-md-3">
        <select multiple class="form-control" name="showorder" id="showorder">
          <option value="desc"<%If tagTextArr(2)="desc"  Then%> selected<%End If%>><%=T("rf_lang_tag_order_id_desc")%></option>
          <option value="asc"<%If tagTextArr(2)="asc"  Then%> selected<%End If%>><%=T("rf_lang_tag_order_id_asc")%></option>
        </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_presentation")%></label>
        <div class="checkbox col-md-offset-2">
          <% If IsNull(presentationList) Then %>
          <%=T("rf_lang_presentation_is_empty")%>
          <%Else %>
          <% Dim k %>
          <% For k=LBound(presentationList, 2) to UBound(presentationList, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="photo_list_pre_id" value="<%=presentationList(0, k) %>"<%If presentationList(0, k)=rfConvertInt(tagTextArr(3)) Then %> checked<% End If %>>
              <%=presentationList(1, k) %>
            </label>
            - [<a href="?<%=QP("rf_action")%>=backend&module=presentation.info&id=<%=presentationList(0, k) %>"><ins><%=T("rf_lang_detail")%></ins></a>]
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>
    
      <div class="form-group">
          <label class="col-md-2 control-label"></label>
          <div class="col-md-4">
          <ul class="list-unstyled">
            <li><%=T("rf_lang_operater")%>: <%=tag.sUsername%></li>
            <li><%=T("rf_lang_updated_at")%>: <%=tag.dUpdatedAt%></li>
            <li><%=T("rf_lang_created_at")%>: <%=tag.dCreatedAt%></li>
          </ul>
          </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
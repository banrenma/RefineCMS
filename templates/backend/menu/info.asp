<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=menus"><%=T("rf_lang_menu")%></a></li>
      <li class="active"><%If menu.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="menu_name" class="col-md-2 control-label"><%=T("rf_lang_menu_menuname")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="menu_name" name="menu_name" value="<%=menu.sMenuName%>" placeholder="<%=T("rf_lang_menu_menuname_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=menu.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
        <label for="pid" class="col-md-2 control-label"><%=T("rf_lang_menu_parentmenu")%></label>
        <div class="col-md-3">
     
        <select multiple class="form-control" name="pid" id="pid">
          <option value="0"<%If menu.iPid=0  Then%> selected<%End If%>>顶级</option>
          <% If Not IsNull(mainMenus) Then %>
          <% Dim i %>
              <% For i=LBound(mainMenus, 2) to UBound(mainMenus, 2) %>
          <option value="<%=mainMenus(0, i) %>"<%If menu.iPid=mainMenus(0, i)  Then%> selected<%End If%>><%=mainMenus(1, i) %></option>
              <% Next %>
        <% End If %>
        </select>
        </div>
      </div>
      <div class="form-group">
          <label class="col-md-2 control-label"><%=T("rf_lang_property")%></label>
          <div class="checkbox col-md-offset-2">
            <div class="radio">
              <label>
                <input type="radio" name="menu_type" value="1"<%If 1=menu.iMenuType Then %> checked<% End If %>>
                <%=T("rf_lang_menu_type_func")%>
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="menu_type" value="2"<%If 2=menu.iMenuType Then %> checked<% End If %>>
                <%=T("rf_lang_menu_type_channel")%>
              </label>
              <% If menu.iID > 0 And menu.iMenuType=2 Then%>
              <p class="help-block"><%=menu.getFilenameOfSource(menu.iID)%></p>
              <% End If %>
            </div>
          </div>
      </div>

      <div class="form-group" id="content_area"<%If 2=menu.iMenuType Then %> style="display: none;"<% End If %>>
        <label class="col-md-2 control-label"><%=T("rf_lang_content_from")%></label>
        <div class=" col-md-4">
          <select class="form-control" name="data_id" id="data_id">
          <% If Not IsNull(categoryList) Then %>
          <% Dim k %>
          <% For k=LBound(categoryList, 2) to UBound(categoryList, 2) %>
          <option value="<%=categoryList(0, k) %>"<%If menu.iIsSinglepage=0 And categoryList(0, k)=menu.iDataID Then %> selected<% End If %>><%=categoryList(1, k) %></option>
              <% Next %>
        <% End If %>

        <% If Not IsNull(singlepageList) Then %>
          <% Dim n %>
          <% For n=LBound(singlepageList, 2) to UBound(singlepageList, 2) %>
          <option value="singlepage_<%=singlepageList(0, n) %>"<%If menu.iIsSinglepage=1 And singlepageList(0, n)=menu.iDataID Then %> selected<% End If %>><%=singlepageList(1, n) %></option>
          <% Next %>
          <% End If %>
        </select>


        </div>
    </div>
    <div class="form-group">
        <label for="excerpt" class="col-md-2 control-label"><%=T("rf_lang_menu_excerpt")%></label>
        <div class="col-md-8">
          <textarea class="form-control" rows="3" name="excerpt" style="overflow-x:hidden"><%=menu.sExcerpt%></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="keywords" class="col-md-2 control-label"><%=T("rf_lang_menu_keywords")%></label>
        <div class="col-md-8">
          <input type="text" class="form-control" name="keywords" value="<%=menu.sKeywords%>" />
        </div>
    </div>
    
      <div class="form-group">
          <label class="col-md-2 control-label"></label>
          <div class="col-md-4">
          <ul class="list-unstyled">
            <li><%=T("rf_lang_operater")%>: <%=menu.sUsername%></li>
            <li><%=T("rf_lang_updated_at")%>: <%=menu.dUpdatedAt%></li>
            <li><%=T("rf_lang_created_at")%>: <%=menu.dCreatedAt%></li>
          </ul>
          </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
<script type="text/javascript">
  $(function(){
      $('input[type=radio][name=menu_type]').change(function() {
          if($(this).val() == 1) {
              $("#content_area").show();
          } else {
              $("#content_area").hide();
          }
      });
  });
</script>
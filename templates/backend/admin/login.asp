<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title><%=RF_CMS_NAME%></title>

    <!-- Bootstrap -->  
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/admin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      
  </head>
  <body>
  <div class="container-fluid">
    <div class="row">
      <div class="page-header">
        <h1><%=RF_CMS_NAME%> <small><%=RF_HOMEPAGE_DESCRIPTION%></small></h1>
      </div>
    </div>

  <div class="row">
    <form class="form-horizontal col-md-6 col-md-offset-3 my-margin" action="?<%=QP("rf_action")%>=backend&module=login" method="post">
      <%=rfCsrfTokenHiddenInput%>
      <input type="hidden" name="btnaction" value="loginAction" />
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <p class="bg-danger"><%=message%></p>
        </div>
      </div>
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label"><%=T("rf_lang_login_user")%></label>
        <div class="col-sm-10">
        <input type="text" class="form-control" id="username" name="username" value="" placeholder="">
        </div>
      </div>
      <div class="form-group">
        <label for="password" class="col-sm-2 control-label"><%=T("rf_lang_login_password")%></label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password" name="password" value="" placeholder="">
        </div>
      </div>
      <div class="form-group">
        <label for="captcha" class="col-sm-2 control-label"><%=T("rf_lang_login_scode")%></label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="captcha" name="captcha" value="" placeholder="">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-10">
          <img src="libs/captcha.asp" onclick="javascript:var urlArr=this.src.split('?');this.src=urlArr[0] + '?' + Math.random();" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_submit")%></button>
        </div>
      </div>
      
    </form>
  </div>

  <footer class="my-footer">
            <p>RefineCMS 作者：木鱼（扣扣：伍肆捌捌肆壹捌陆壹）。 代码托管在 <a href="https://gitee.com/banrenma/RefineCMS" target="_blank">码云</a>。<a href='https://gitee.com/banrenma/RefineCMS'><img src='https://gitee.com/banrenma/RefineCMS/widgets/widget_6.svg' alt='Fork me on Gitee'></img></a></p>
            <p>本项目源码受 <a rel="license" href="https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE" target="_blank">GPL</a>开源协议保护。</p>
            <p>数据库访问次数： <%=g_timerWatcher.DbTotalHits%>，数据库访问总时间：<%=g_timerWatcher.DbTotalTime%> ms</p>
            <%g_timerWatcher.clearLastLog%>
        </footer>

</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>$(document).ready(function(){  $("input[name=username]").focus();});</script>
  </body>
</html>
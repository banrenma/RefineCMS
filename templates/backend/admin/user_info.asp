<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=user.list"><%=T("rf_lang_admin_page_user_management")%></a></li>
      <li class="active"><%=T("rf_lang_add")%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="username" class="col-md-2 control-label"><%=T("rf_lang_login_user")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="username" name="username" value="<%=admin.sUsername%>" placeholder="<%=T("rf_lang_login_user_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=admin.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
        <label for="password" class="col-md-2 control-label"><%=T("rf_lang_login_password")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="password" name="password" value="" placeholder="<%=T("rf_lang_login_password_placeholder")%>">
        </div>
      </div>
      <div class="form-group">
          <label class="col-md-2 control-label"><%=T("rf_lang_login_role")%></label>
          <div class="checkbox col-md-offset-2">
            <label class="col-md-2">
              <input type="radio" name="role" id="role" value="1"<% If admin.iRole = 1 Then %> checked="checked"<%End If%>><%=T("rf_lang_user_role_admin")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="role" id="role" value="2"<% If admin.iRole = 2 Then %> checked="checked"<%End If%>><%=T("rf_lang_user_role_editor")%>
            </label>
          </div>
      </div>
      <div class="form-group">
          <label class="col-md-2 control-label"><%=T("rf_lang_login_status")%></label>
          <div class="checkbox col-md-offset-2">
            <label class="col-md-2">
              <input type="radio" name="status" value="1"<% If admin.iStatus = 1 Then %> checked="checked"<%End If%>><%=T("rf_lang_user_status_normal")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="status" value="2"<% If admin.iStatus = 2 Then %> checked="checked"<%End If%>><%=T("rf_lang_user_status_forbidden")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="status" value="3"<% If admin.iStatus = 3 Then %> checked="checked"<%End If%>><%=T("rf_lang_user_status_deleted")%>
            </label>
          </div>
      </div>
      <div class="form-group">
        <div class="col-md-4  col-md-offset-2">
        
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
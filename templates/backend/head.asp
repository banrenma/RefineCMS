<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title><%=RF_CMS_NAME%></title>

    <!-- Bootstrap -->
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
     <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/jquery.treeview.css" rel="stylesheet">
    <link href="assets/css/admin.css" rel="stylesheet">
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
        $("#navigation").treeview({
          animated: "fast",
          expand: true,
          unique: true,
          persist: "cookie",
          toggle: function() {
            // window.console && console.log("%o was toggled", this);
          }
        });
      });
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="https://gitee.com/banrenma/RefineCMS" target="_blank"><%=RF_CMS_NAME%></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<%=rfIndexUrl%>/" target="_blank">访问网站</a></li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=logout">退出</a></li>
          </ul>
          <form class="navbar-form navbar-right col-md-6" method="post" action="?<%=QP("rf_action")%>=backend&module=tag.import">
                <div class="form-group">
                  <label class="sr-only" for="tag_name">标签</label>
                  <input type="input" class="form-control" name="tag_name" placeholder="快速解析标签，回车！">
                </div>
          </form>

        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">

          <ul id="navigation" class="hidden-xs hidden-sm">
            <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a>
            </li>
            <li>
                <a href="?<%=QP("rf_action")%>=backend&module=menus"><%=T("rf_lang_menu")%></a>
            </li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=tag.config"><%=T("rf_lang_config_presentation")%></a></li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource")%></a>
              <ul>
                <li><a href="?<%=QP("rf_action")%>=backend&module=ds.singlepage"><%=T("rf_lang_datasource_category_singlepage")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=ds.essays"><%=T("rf_lang_datasource_category_essays")%></a><%=rf_mod_ds_category_by_type("ds.essays", 1)%></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=ds.photos"><%=T("rf_lang_datasource_category_photos")%></a><%=rf_mod_ds_category_by_type("ds.photos", 2)%></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks"><%=T("rf_lang_datasource_category_friendlinks")%></a><%=rf_mod_ds_category_by_type("ds.friendlinks", 3)%></li>
              </ul>
            </li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list"><%=T("rf_lang_presentation")%></a>
              <ul>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=1"><%=T("rf_lang_presentation_pretype_essay_list")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=2"><%=T("rf_lang_presentation_pretype_essay_info")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=3"><%=T("rf_lang_presentation_pretype_photo_list")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=4"><%=T("rf_lang_presentation_pretype_photo_info")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=11"><%=T("rf_lang_presentation_pretype_photo_flexslider")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=5"><%=T("rf_lang_presentation_pretype_singlepage")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=6"><%=T("rf_lang_presentation_pretype_breadcrumb")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=7"><%=T("rf_lang_presentation_pretype_mainnav")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=8"><%=T("rf_lang_presentation_pretype_subnav")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=9"><%=T("rf_lang_presentation_pretype_friendlink")%></a></li>
              </ul>
            </li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list"><%=T("rf_lang_tag")%></a>
              <ul>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=1"><%=T("rf_lang_presentation_pretype_essay_list")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=3"><%=T("rf_lang_presentation_pretype_photo_list")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=11"><%=T("rf_lang_presentation_pretype_photo_flexslider")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=5"><%=T("rf_lang_presentation_pretype_singlepage")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=6"><%=T("rf_lang_presentation_pretype_breadcrumb")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=7"><%=T("rf_lang_presentation_pretype_mainnav")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=8"><%=T("rf_lang_presentation_pretype_subnav")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=9"><%=T("rf_lang_presentation_pretype_friendlink")%></a></li>
                <li><a href="?<%=QP("rf_action")%>=backend&module=tag.list&tagtype=10"><%=T("rf_lang_partial")%></a></li>
              </ul>
            </li>
            <li>
                <a href="?<%=QP("rf_action")%>=backend&module=tpl"><%=T("rf_lang_tpl")%></a>
            </li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=user.list"><%=T("rf_lang_admin_page_user_management")%></a>
            </li>
            <li><a href="?<%=QP("rf_action")%>=backend&module=help"><%=T("rf_lang_help")%></a>
            </li>
            
          </ul>



      </div>
      <div class="col-md-10">
        <div class="container-fluid">
        
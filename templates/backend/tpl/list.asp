<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tpl"><%=T("rf_lang_tpl")%></a></li>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>


</div>
</div>

<div class="container-fluid">
  <div class="row">
  <% If rfIsEmpty(folderFrontendNotExists) Then %>
    <div class="panel panel-success">
      <div class="panel-heading"><%=T("rf_lang_tpl_index_asp")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_index_asp_desc")%></p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <% If rfIsEmpty(fileIndexAsp) Then %>
          <span class="badge"><%=T("rf_lang_status_fail")%></span><%=T("rf_lang_file_not_found")%>: index.asp
          <% Else %>
          <span class="badge"><%=T("rf_lang_status_success")%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=index.asp">index.asp</a>
          <% End If %>
        </li>
      </ul>
    </div>

    <div class="panel panel-success">
      <div class="panel-heading"><%=T("rf_lang_tpl_common_html")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_common_html_desc")%></p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <% If rfIsEmpty(fileCommon) Then %>
          <span class="badge"><%=T("rf_lang_status_fail")%></span><%=T("rf_lang_file_not_found")%>: common.asp
          <% Else %>
          <span class="badge"><%=T("rf_lang_status_success")%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=common.asp">common.asp</a>
          <% End If %>
        </li>
      </ul>
    </div>

    <% If filelistInfo.getItemCount > 0 Then %>
    <div class="panel panel-info">
      <div class="panel-heading"><%=T("rf_lang_tpl_info_files")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_info_files_desc")%></p>
      </div>
      <ul class="list-group">
        <% 
        Do 
          tmpFile = filelistInfo.pop()
        %>
        <li class="list-group-item"><span class="badge"><%=fmtTipsStatus(rfCustomTplCheck(tmpFile))%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=<%=tmpFile%>"><%=tmpFile%></a></li>
        <% Loop While filelistInfo.getItemCount > 0 %>
      </ul>
    </div>
    <% End If %>

    <% If filelistMenu.getItemCount > 0 Then %>
    <div class="panel panel-info">
      <div class="panel-heading"><%=T("rf_lang_tpl_menu_files")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_menu_files_desc")%></p>
      </div>
      <ul class="list-group">
        <% 
        Do 
          tmpFile = filelistMenu.pop()
        %>
        <li class="list-group-item"><span class="badge"><%=fmtTipsStatus(rfCustomTplCheck(tmpFile))%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=<%=tmpFile%>"><%=tmpFile%></a></li>
        <% Loop While filelistMenu.getItemCount > 0 %>
      </ul>
    </div>
    <% End If %>

    <div class="panel panel-warning">
      <div class="panel-heading"><%=T("rf_lang_tpl_error")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_error_desc")%></p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <% If rfIsEmpty(fileError) Then %>
          <span class="badge"><%=T("rf_lang_status_fail")%></span><%=T("rf_lang_file_not_found")%>: error.asp
          <% Else %>
          <span class="badge"><%=T("rf_lang_status_success")%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=error.asp">error.asp</a>
          <% End If %>
        </li>
      </ul>
    </div>

    <div class="panel panel-warning">
      <div class="panel-heading"><%=T("rf_lang_tpl_404")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_404_desc")%></p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <% If rfIsEmpty(file404) Then %>
          <span class="badge"><%=T("rf_lang_status_fail")%></span><%=T("rf_lang_file_not_found")%>: 404.asp
          <% Else %>
          <span class="badge"><%=T("rf_lang_status_success")%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=404.asp">404.asp</a>
          <% End If %>
        </li>
      </ul>
    </div>

    <div class="panel panel-danger">
      <div class="panel-heading"><%=T("rf_lang_tpl_index_html")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_index_html_desc")%></p>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <% If rfIsEmpty(fileIndexHtml) Then %>
          <span class="badge"><%=T("rf_lang_status_fail")%></span><%=T("rf_lang_file_not_found")%>: index.html
          <% Else %>
          <span class="badge"><%=T("rf_lang_status_success")%></span><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=index.html">index.html</a>
          <% End If %>
        </li>
      </ul>
    </div>

    <% If filelistDeleted.getItemCount > 0 Then %>
    <div class="panel panel-default">
      <div class="panel-heading"><%=T("rf_lang_tpl_deleted_files")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_deleted_files_desc")%></p>
      </div>
      <ul class="list-group">
        <% 
        Do 
          tmpFile = filelistDeleted.pop()
        %>
        <li class="list-group-item"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=<%=tmpFile%>"><%=tmpFile%></a></li>
        <% Loop While filelistDeleted.getItemCount > 0 %>
      </ul>
    </div>
    <% End If %>

    <% If filelistUnknow.getItemCount > 0 Then %>
    <div class="panel panel-default">
      <div class="panel-heading"><%=T("rf_lang_tpl_unknown_files")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_unknown_files_desc")%></p>
      </div>
      <ul class="list-group">
        <% 
        Do 
          tmpFile = filelistUnknow.pop()
        %>
        <li class="list-group-item"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a href="?<%=QP("rf_action")%>=backend&module=tpl.info&file=<%=tmpFile%>"><%=tmpFile%></a></li>
        <% Loop While filelistUnknow.getItemCount > 0 %>
      </ul>
    </div>
    <% End If %>

    <% If folderlistUnknow.getItemCount > 0 Then %>
    <div class="panel panel-default">
      <div class="panel-heading"><%=T("rf_lang_tpl_unknown_folders")%></div>
      <div class="panel-body">
        <p><%=T("rf_lang_tpl_unknown_folders_desc")%></p>
      </div>
      <ul class="list-group">
        <% 
        Do 
          tmpFile = folderlistUnknow.pop()
        %>
        <li class="list-group-item"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp;<%=tmpFile%></li>
        <% Loop While folderlistUnknow.getItemCount > 0 %>
      </ul>
    </div>
    <% End If %>
  <% Else %>
    <div class="panel panel-default">
      <div class="panel-heading"><%=T("rf_lang_status_fail")%></div>
      <div class="panel-body">
        <p><%=folderFrontendNotExists%></p>
      </div>
    </div>
  <% End If %>

  </div>
</div>
<!-- #include file="../foot.asp" -->
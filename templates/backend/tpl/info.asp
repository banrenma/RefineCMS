<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<script src="/assets/codemirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="/assets/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/assets/codemirror/theme/tomorrow-night-eighties.css">
<script src="/assets/codemirror/mode/javascript/javascript.js"></script>

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=tpl"><%=T("rf_lang_tpl")%></a></li>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>


</div>
</div>


<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
        <label for="pre_name" class="col-md-2 control-label"><%=T("rf_lang_tpl_filename")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="filename" name="filename" value="<%=fileName%>" readonly>
        </div>
      </div>
      
      <div class="form-group">
          <label for="filestr" class="col-md-2 control-label"><%=T("rf_lang_tpl_filestr")%></label>
          <div class="col-md-8">
            <textarea class="form-control" rows="8" id="filestr" name="filestr"><%=fileStr%></textarea>
          </div>
      </div>
      
      <div class="form-group">
          <label class="col-md-2 control-label"></label>
          <div class="col-md-4">
          <ul class="list-unstyled">
            <li><%=T("rf_lang_updated_at")%>: <%=dateLastModified%></li>
            <li><%=T("rf_lang_created_at")%>: <%=dateCreated%></li>
          </ul>
          </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>
<script type="text/javascript">
var editor = CodeMirror.fromTextArea(document.getElementById("filestr"), {
    lineNumbers: true,
    styleActiveLine: true,
    theme: "tomorrow-night-eighties",
    matchBrackets: true
  });
 editor.setSize('auto','600px');
</script>
<!-- #include file="../foot.asp" -->
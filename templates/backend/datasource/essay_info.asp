<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<script src="/assets/tinymce/tinymce.js"></script>
<script type="text/javascript">
        tinymce.init({
            menubar : false,
            selector: "#mainbody",
            language: "zh_CN",
            theme: "modern",
            width: '100%',
            height: 500,
            convert_urls: false,
            plugins: [
                 "advlist  link image lists charmap  hr anchor ",
                 "searchreplace wordcount visualblocks visualchars code nonbreaking save contextmenu directionality  paste  rfmedia"
           ], //bbcode：如果添加该插件，编辑器「代码」状态将是 bbcode 风格
           content_css: "/assets/bootstrap/3.3.7/css/bootstrap.min.css",
           toolbar: "rfmedia formatselect blockquote link | bullist numlist | alignleft aligncenter alignright alignjustify bold italic  code"
        });
    </script>
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource_category")%></a></li>
      <% If IsObject(category) And category.iID > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.essays&catID=<%=category.iID%>"><%=category.sCatName%></a></li>
      <% End If %>
      <li class="active"><%If essay.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    <%If Not rfIsEmpty(message) Then%>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <% End If %>
    <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_category_cattype")%></label>
        <div class="checkbox col-md-offset-2">
          <% If Not IsNull(catgoryOptions) Then %>
          <% Dim i %>
          <% For i=LBound(catgoryOptions, 2) to UBound(catgoryOptions, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="cat_id" value="<%=catgoryOptions(0, i) %>"<%If catgoryOptions(0, i)=essay.iCatId Then %> checked<% End If %>>
              <%=catgoryOptions(1, i) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

    <div class="form-group">
        <label for="title" class="col-md-2 control-label"><%=T("rf_lang_essay_title")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="title" name="title" value="<%=essay.sTitle%>" placeholder="<%=T("rf_lang_essay_title_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=essay.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
    </div>
    <div class="form-group">
        <label for="excerpt" class="col-md-2 control-label"><%=T("rf_lang_essay_excerpt")%></label>
        <div class="col-md-8">
          <textarea class="form-control" rows="3" name="excerpt" style="overflow-x:hidden"><%=essay.sExcerpt%></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="keywords" class="col-md-2 control-label"><%=T("rf_lang_essay_keywords")%></label>
        <div class="col-md-8">
          <input type="text" class="form-control" name="keywords" value="<%=essay.sKeywords%>" />
        </div>
    </div>
      <div class="form-group">
          <label for="alt" class="col-md-2 control-label"><%=T("rf_lang_media_thumbnail")%></label>
          <div class="col-md-4" id="thumbnail_area">
            <% If Not rfIsEmpty(essay.sFilepath) Then%>
            <img src="<%=essay.sFilepath%>" <%=rfPhotoAutoScale(150, 150, essay.sResolution)%> />
            <p onclick="removeThumbnail();"><%=T("rf_lang_essay_thumbnail_remove")%></p>
            <% Else %>
            <%=T("rf_lang_essay_thumbnail_tips")%>
            <% End If %>
          </div>
          <input type="hidden" name="filepath_hidden" id="filepath_hidden" value="<%=essay.sFilepath%>">
          <input type="hidden" name="resolution_hidden" id="resolution_hidden" value="<%=essay.sResolution%>">
      </div>
      
    <div class="form-group">
        <label for="mainbody" class="col-md-2 control-label"><%=T("rf_lang_essay_mainbody")%></label>
        <div class="col-md-8">
          <textarea class="form-control"  name="mainbody" id="mainbody"><%=essay.sMainbody%></textarea>
        </div>
    </div>

      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>
<script type="text/javascript">
    function removeThumbnail() {
        $("#thumbnail_area").empty();
        $("#filepath_hidden").val("");
        $("#resolution_hidden").val("");
    }
  </script>
<!-- #include file="../foot.asp" -->
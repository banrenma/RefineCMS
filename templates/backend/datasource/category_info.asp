<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource_category")%></a></li>
      <li class="active"><%If category.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="catname" class="col-md-2 control-label"><%=T("rf_lang_category_catname")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="catname" name="catname" value="<%=category.sCatName%>" placeholder="<%=T("rf_lang_category_catname_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=category.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>

      <div class="form-group">
          <label class="col-md-2 control-label"><%=T("rf_lang_category_cattype")%></label>
          <div class="checkbox col-md-offset-2">
            <label class="col-md-2">
              <input type="radio" name="cattype" value="1"<% If category.iCatType = 1 Then %> checked="checked"<%End If%>><%=T("rf_lang_datasource_category_essays")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="cattype" value="2"<% If category.iCatType = 2 Then %> checked="checked"<%End If%>><%=T("rf_lang_datasource_category_photos")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="cattype" value="3"<% If category.iCatType = 3 Then %> checked="checked"<%End If%>><%=T("rf_lang_datasource_category_friendlinks")%>
            </label>
          </div>
      </div>
      
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
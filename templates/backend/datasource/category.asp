<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource_category")%></a></li>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>
  <div class="col-md-2">
      <p><a href="?<%=QP("rf_action")%>=backend&module=ds.category.info" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
  </div>
</div>
</div>


<% If IsNull(catList) Then %>
    <p><%=T("rf_lang_list_is_empty")%></p>
<% Else %>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th><%=T("rf_lang_category_catname")%></th>
                  <th><%=T("rf_lang_category_cattype")%></th>
                  <th><%=T("rf_lang_category_cattype")%></th>
                  <th><%=T("rf_lang_opt")%></th>
                </tr>
              </thead>
              <tbody>
              <% Dim i %>
              <% For i=LBound(catList, 2) to UBound(catList, 2) %>
                <tr>
                  <td><%=catList(0, i) %></td>
                  <td><%=catList(1, i) %></td>
                  <td><%=fmtCategoryIcon(catList(2, i)) %></td>
                  <td><%=fmtCategoryType(catList(2, i)) %></td>
                  <td>
                         <a href="?<%=QP("rf_action")%>=backend&module=ds.category.info&id=<%=catList(0, i) %>"><%=T("rf_lang_edit")%></a>
                         | 
                        <a href="?<%=QP("rf_action")%>=backend&module=ds.category&opt=del&id=<%=catList(0, i) %>&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_are_you_sure")%>')){return true;}return false;}"><%=T("rf_lang_delete")%></a>
                  </td>
                </tr>
                <% Next %>

              </tbody>
            </table>
          </div>
<% End If %>   
<div class="row"><%=showPage%></div>
<!-- #include file="../foot.asp" -->
<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource_category")%></a></li>
      <% If IsObject(category) And category.iID > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks&catID=<%=category.iID%>"><%=category.sCatName%></a></li>
      <% End If %>
      <li class="active"><%If friendlink.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>
<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" ENCTYPE="multipart/form-data" action="" method="post">
    <%If Not rfIsEmpty(message) Then%>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <% End If %>
    <% If Not rfIsEmpty(friendlink.sFilepath) Then%>
    <div class="form-group">
        <label for="alt" class="col-md-2 control-label"><%=T("rf_lang_detail")%></label>
        <div class="col-md-4">
          <img src="<%=friendlink.sFilepath%>" <%=rfPhotoAutoScale(300, 300, friendlink.sResolution)%> />
        </div>
    </div>
    <div class="form-group">
        <label for="alt" class="col-md-2 control-label"></label>
        <div class="col-md-4">
        <ul class="list-unstyled">
          <li><%=friendlink.sResolution%></li>
          <li><%=friendlink.sFilepath%></li>
          <li><%=friendlink.sUsername%></li>
          <li><%=T("rf_lang_updated_at")%>: <%=friendlink.dUpdatedAt%></li>
          <li><%=T("rf_lang_created_at")%>: <%=friendlink.dCreatedAt%></li>
        </ul>
        </div>
    </div>
    <% End If %>
    <div class="form-group">
        <label class="col-md-2 control-label"><%=T("rf_lang_category_cattype")%></label>
        <div class="checkbox col-md-offset-2">
          <% If Not IsNull(catgoryOptions) Then %>
          <% Dim i %>
          <% For i=LBound(catgoryOptions, 2) to UBound(catgoryOptions, 2) %>
          <div class="radio">
            <label>
              <input type="radio" name="cat_id" value="<%=catgoryOptions(0, i) %>"<%If catgoryOptions(0, i)=CInt(friendlink.iCatId) Then %> checked<% End If %>>
              <%=catgoryOptions(1, i) %>
            </label>
          </div>
          <% Next %>
          <% End If %>   
        </div>
    </div>

    <div class="form-group">
        <label for="linkname" class="col-md-2 control-label"><%=T("rf_lang_friendlink_linkname")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="linkname" name="linkname" value="<%=friendlink.sLinkname%>" placeholder="<%=T("rf_lang_friendlink_linkname_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=friendlink.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
    </div>
    <div class="form-group">
        <label for="linkurl" class="col-md-2 control-label"><%=T("rf_lang_friendlink_linkurl")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="linkurl" name="linkurl" value="<%=friendlink.sLinkurl%>" placeholder="">
        </div>
    </div>
    <div class="form-group">
        <label for="alt" class="col-md-2 control-label"><%=T("rf_lang_friendlink_fontcolor")%></label>
        <div class="col-md-4">
          <label class="col-md-2">
              <input type="radio" name="fontcolor" value="1"<% If friendlink.iFontcolor = 1 Then %> checked="checked"<%End If%>><%=T("rf_lang_friendlink_fontcolor_default")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="fontcolor" value="2"<% If friendlink.iFontcolor = 2 Then %> checked="checked"<%End If%>><span style="color: #c00;"><%=T("rf_lang_friendlink_fontcolor_red")%></span>
            </label>
            <label class="col-md-2">
              <input type="radio" name="fontcolor" value="3"<% If friendlink.iFontcolor = 3 Then %> checked="checked"<%End If%>><span style="color: #0000ff;"><%=T("rf_lang_friendlink_fontcolor_blue")%></span>
            </label>
            <label class="col-md-2">
              <input type="radio" name="fontcolor" value="3"<% If friendlink.iFontcolor = 4 Then %> checked="checked"<%End If%>><span style="color: #777;"><%=T("rf_lang_friendlink_fontcolor_grey")%></span>
            </label>
        </div>
    </div>

    <div class="form-group">
        <label for="fullscale" class="col-md-2 control-label"><%=T("rf_lang_media_fullscale")%></label>
        <div class="col-md-4">
        <INPUT TYPE="file" NAME="fullscale" id="fullscale" />
        <p class="help-block"><%=Join(RF_UPLOAD_FILETYPE_PHOTO, " | ")%></p>
        </div>
    </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>

<!-- #include file="../foot.asp" -->
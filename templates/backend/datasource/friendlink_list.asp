<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.category"><%=T("rf_lang_datasource_category")%></a></li>
      <% If IsObject(category) And category.iID > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks&catID=<%=category.iID%>"><%=category.sCatName%></a></li>
      <% End If %>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>
  <div class="col-md-2">
      <% If catID > 0 Then %>
      <p><a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks.info&catID=<%=catID%>" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
      <% Else %>
      <p><a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks.info" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
      <% End If %>
  </div>
</div>
</div>


<% If IsNull(friendlinkList) Then %>
    <p><%=T("rf_lang_list_is_empty")%></p>
<% Else %>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th><%=T("rf_lang_media_thumbnail")%></th>
                  <th><%=T("rf_lang_friendlink_linkname")%></th>
                  <th><%=T("rf_lang_friendlink_linkurl")%></th>
                  <th><%=T("rf_lang_category_cattype")%></th>
                  <th><%=T("rf_lang_friendlink_fontcolor")%></th>
                  <th><%=T("rf_lang_updated_at")%></th>
                  <th><%=T("rf_lang_opt")%></th>
                </tr>
              </thead>
              <tbody>
              <% Dim j %>
              <% For j=LBound(friendlinkList, 2) to UBound(friendlinkList, 2) %>
                <tr>
                  <td><%=friendlinkList(0, j) %></td>
                  <td>
                    <% If Not rfIsEmpty(friendlinkList(7, j)) Then %>
                    <img src="<%=friendlinkList(7, j) %>"  <%=rfPhotoAutoScale(50, 50, friendlinkList(6, j))%> />
                    <% Else %>
                    -
                    <% End If %>
                  </td>
                  <td><%=fmtFriendlinkFontcolorText(friendlinkList(5, j), friendlinkList(1, j)) %></td>
                  <td><%=friendlinkList(2, j) %></td> 
                  <td><%=friendlinkList(4, j) %></td>
                  <td><%=fmtFriendlinkFontcolorText(friendlinkList(5, j), fmtFriendlinkFontcolor(friendlinkList(5, j))) %></td>
                  <td><%=friendlinkList(8, j) %></td>
                  <td>
                         <a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks.info&id=<%=friendlinkList(0, j) %>"><%=T("rf_lang_edit")%></a>
                         | 
                        <a href="?<%=QP("rf_action")%>=backend&module=ds.friendlinks&opt=del&id=<%=friendlinkList(0, j) %><%If catID>0 Then Response.Write "&catID="&catID%>&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_are_you_sure")%>')){return true;}return false;}"><%=T("rf_lang_delete")%></a>
                  </td>
                </tr>
                <% Next %>

              </tbody>
            </table>
          </div>
<% End If %>   
<div class="row"><%=showPage%></div>
<!-- #include file="../foot.asp" -->
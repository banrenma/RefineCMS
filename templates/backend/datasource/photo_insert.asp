<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title><%=RF_CMS_NAME%></title>

    <!-- Bootstrap -->
    <link href="assets/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/jquery.treeview.css" rel="stylesheet">
    <link href="assets/css/admin.css" rel="stylesheet">
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-4" style="padding-bottom: 10px">
          <% If IsNull(categoryList) Then %>
          <%=T("rf_lang_list_is_empty")%>
          <%Else %>
        <select  class="form-control" id="cat_select">
          <option value="0"<%If CInt(catID)=categoryList(0, j2)  Then%> selected<%End If%>><%=T("rf_lang_datasource_category_all")%></option>
          <% Dim j2 %>
              <% For j2=LBound(categoryList, 2) to UBound(categoryList, 2) %>
          <option value="<%=categoryList(0, j2) %>"<%If CInt(catID)=categoryList(0, j2)  Then%> selected<%End If%>><%=categoryList(1, j2) %></option>
              <% Next %>
        </select>
        <% End If %>
  </div>

    </div>
    <div class="row">
      <div class="col-md-12">
          <% If IsNull(mediaList) Then %>
              <p><%=T("rf_lang_list_is_empty")%></p>
          <% Else %>
              <% Dim i %>
              <% For i=LBound(mediaList, 2) to UBound(mediaList, 2) %>
              <div class="col-md-2 col-sm-3 col-xs-6" style="height: 180px;">
                <a href="javascript:void(0);" class="thumbnail">
                    <img src="<%=mediaList(9, i) %>" alt="<%=mediaList(5, i) %>"  <%=rfPhotoAutoScale(150, 150, mediaList(8, i))%> />
                </a>
                <div class="hidden">
                  <a href="<%=mediaList(9, i) %>">
                      <img src="<%=mediaList(9, i) %>" alt="<%=mediaList(5, i) %>" />
                  </a>
                </div>
                <!-- 缩略图，动态更新到表单 -->
                <div class="hidden">
                      <img src="<%=mediaList(9, i) %>" alt="<%=mediaList(5, i) %>"  <%=rfPhotoAutoScale(150, 150, mediaList(8, i))%> />
                      <p onclick="removeThumbnail();"><%=T("rf_lang_essay_thumbnail_remove")%></p>
                </div>
                <!-- 缩略图的路径，插入到隐藏的input -->
                <div class="hidden"><%=mediaList(9, i) %></div>
                <!-- 缩略图的尺寸，插入到隐藏的input -->
                <div class="hidden"><%=mediaList(8, i) %></div>
              </div>
              <% Next %>
          <% End If %>  
      </div>
    </div>
    <div class="row"><div class="col-md-12"><%=showPage%></div></div>
    <div class="row">
      <div class="col-md-6 col-md-offset-6">
          <a href="" class="btn btn-success" role="button" id="btnAsThumbnail"><%=T("rf_lang_essay_thumbnail_insert")%></a>
          <a href="" class="btn btn-success" role="button" id="insertMedia"><%=T("rf_lang_essay_insert_mainbody")%></a>
          <a href="javascript:top.tinymce.activeEditor.windowManager.close();" class="btn btn-default" role="button"><%=T("rf_lang_close_window")%></a>
      </div>
  </div>
  </div>
        
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.cookie.js"></script>
    <script type="text/javascript">
    $(function(){
        var args = top.tinymce.activeEditor.windowManager.getParams();
        var lastObj = false;
        $('a.thumbnail').click(function(){
            if(lastObj) {
                $(lastObj).removeClass('my-media-selected');
            }
            $(this).addClass('my-media-selected');
            lastObj = this;
        });

        $('#insertMedia').click(function(){
            if(lastObj) {
                args.ed.insertContent($(lastObj).siblings().eq(0).html());
                top.tinymce.activeEditor.windowManager.close();
            } else {
              alert('<%=T("rf_lang_media_unselected")%>');
              return false;
            }
        });

        $('#btnAsThumbnail').click(function(){
            if(lastObj) {
               var thumbnail_area = top.document.getElementById("thumbnail_area");
               var filepath_hidden = top.document.getElementById("filepath_hidden");
               var resolution_hidden = top.document.getElementById("resolution_hidden");
               if($(lastObj).siblings().eq(1).html() == undefined) {
                  alert('<%=T("rf_lang_media_thumbnail_caution")%>');
                  return false;
               }
               thumbnail_area.innerHTML = $(lastObj).siblings().eq(1).html();
               filepath_hidden.value = $(lastObj).siblings().eq(2).html();
               resolution_hidden.value = $(lastObj).siblings().eq(3).html();
               top.tinymce.activeEditor.windowManager.close();
            } else {
              alert('<%=T("rf_lang_media_unselected")%>');
            }
        });

        $("#cat_select").change(function() {
          var targetUrl;
          var targetCatID = $(this).val();
          if(location.href.indexOf("catID") != -1) {
            targetUrl = location.href.replace(/catID=\d+/, "catID="+targetCatID);
          } else {
            targetUrl = location.href + "&catID="+targetCatID;
          }
          location.href = targetUrl;
        });
        

    });
  </script>
  </body>
</html>
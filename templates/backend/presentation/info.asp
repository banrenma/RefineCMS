<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->
<script src="/assets/codemirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="/assets/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/assets/codemirror/theme/tomorrow-night-eighties.css">
<script src="/assets/codemirror/mode/javascript/javascript.js"></script>

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list"><%=T("rf_lang_presentation")%></a></li>
      <% If presentation.iPreType > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=<%=presentation.iPreType%>"><%=fmtPresentationType(presentation.iPreType)%></a></li>
      <% End If %>
      <li class="active"><%If presentation.iID>0 Then%><%=T("rf_lang_edit")%><%Else%><%=T("rf_lang_add")%><%End If%></li>
    </ol>
  </div>
  <div class="col-md-2">
      
  </div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <form class="form-horizontal" action="" method="post">
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <p class="bg-danger"><%=message%></p>
      </div>
    </div>
    <div class="form-group">
        <label for="pre_name" class="col-md-2 control-label"><%=T("rf_lang_presentation_prename")%></label>
        <div class="col-md-4">
        <input type="text" class="form-control" id="pre_name" name="pre_name" value="<%=presentation.sPreName%>" placeholder="<%=T("rf_lang_presentation_prename_validate")%>">
        <input type="hidden" name="id_hidden" value="<%=presentation.iID%>">
        <%=rfCsrfTokenHiddenInput%>
        </div>
      </div>
      <div class="form-group">
          <label class="col-md-2 control-label"><%=T("rf_lang_presentation_pretype")%></label>
          <div class="checkbox col-md-offset-2">
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="1"<% If presentation.iPreType = 1 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_essay_list")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="2"<% If presentation.iPreType = 2 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_essay_info")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="11"<% If presentation.iPreType = 11 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_photo_flexslider")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="3"<% If presentation.iPreType = 3 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_photo_list")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="4"<% If presentation.iPreType = 4 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_photo_info")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="5"<% If presentation.iPreType = 5 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_singlepage")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="6"<% If presentation.iPreType = 6 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_breadcrumb")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="7"<% If presentation.iPreType = 7 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_mainnav")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="8"<% If presentation.iPreType = 8 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_subnav")%>
            </label>
            <label class="col-md-2">
              <input type="radio" name="pre_type" value="9"<% If presentation.iPreType = 9 Then %> checked="checked"<%End If%>><%=T("rf_lang_presentation_pretype_friendlink")%>
            </label>
          </div>
      </div>
      <div class="form-group">
          <label for="codehtml" class="col-md-2 control-label"><%=T("rf_lang_presentation_codehtml")%></label>
          <div class="col-md-8">
            <textarea class="form-control" rows="8" id="codehtml" name="codehtml"><%=presentation.sCodehtml%></textarea>
          </div>
      </div>
      <div class="form-group">
          <label for="remark" class="col-md-2 control-label"><%=T("rf_lang_remark")%></label>
          <div class="col-md-8">
            <textarea class="form-control" rows="3" name="remark" style="overflow-x:hidden"><%=presentation.sRemark%></textarea>
          </div>
      </div>
      <div class="form-group">
          <label class="col-md-2 control-label"></label>
          <div class="col-md-4">
          <ul class="list-unstyled">
            <li><%=T("rf_lang_operater")%>: <%=presentation.sUsername%></li>
            <li><%=T("rf_lang_updated_at")%>: <%=presentation.dUpdatedAt%></li>
            <li><%=T("rf_lang_created_at")%>: <%=presentation.dCreatedAt%></li>
          </ul>
          </div>
      </div>
      
      <div class="form-group">
        <div class="col-md-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary"><%=T("rf_lang_save")%></button>
        </div>
      </div>
    </form>
  </div>
  </div>
<script type="text/javascript">
var editor = CodeMirror.fromTextArea(document.getElementById("codehtml"), {
    lineNumbers: true,
    styleActiveLine: true,
    theme: "tomorrow-night-eighties",
    matchBrackets: true
  });
</script>
<!-- #include file="../foot.asp" -->
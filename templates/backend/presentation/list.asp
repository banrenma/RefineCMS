<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 版权所有（C）2018 viviworld
' 本程序为自由软件，在自由软件联盟发布的GNU通用公共许可协议的约束下，你可以对其进行再发布及修改。协议版本为第三版或（随你）更新的版本。
' 我们希望发布的这款程序有用，但不保证，甚至不保证它有经济价值和适合特定用途。详情参见GNU通用公共许可协议。
' 你理当已收到一份GNU通用公共许可协议的副本，如果没有，请查阅<http://www.gnu.org/licenses/>

' QQ: 548841861 （木鱼）
' Email： 365zph@gmail.com
' 项目地址：https://gitee.com/banrenma/RefineCMS
' 协议地址： https://gitee.com/banrenma/RefineCMS/blob/master/LICENSE
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<% If Not DIRECT_VISIT Then Response.Write "No direct script access allowed" : Response.End%>

<!-- #include file="../head.asp" -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-10">
    <ol class="breadcrumb">
      <li><a href="?<%=QP("rf_action")%>=backend&module=dashboard"><%=T("rf_lang_dashboard")%></a></li>
      <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list"><%=T("rf_lang_presentation")%></a></li>
      <% If pretype > 0 Then %>
      <li><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=<%=pretype%>"><%=fmtPresentationType(pretype)%></a></li>
      <% End If %>
      <li class="active"><%=T("rf_lang_list")%></li>
    </ol>
  </div>

  <div class="col-md-2">
      <% If pretype > 0 Then %>
      <p><a href="?<%=QP("rf_action")%>=backend&module=presentation.info&pretype=<%=pretype%>" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
      <% Else %>
      <p><a href="?<%=QP("rf_action")%>=backend&module=presentation.info" class="btn btn-success" role="button"><%=T("rf_lang_add")%></a></p>
      <% End If %>
  </div>
</div>
</div>


<% If IsNull(presentationList) Then %>
    <p><%=T("rf_lang_list_is_empty")%></p>
<% Else %>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th><%=T("rf_lang_presentation_prename")%></th>
                  <th><%=T("rf_lang_presentation_pretype")%></th>
                  <th><%=T("rf_lang_presentation_official")%></th>
                  <th><%=T("rf_lang_updated_at")%></th>
                  <th><%=T("rf_lang_operater")%></th>
                  <th><%=T("rf_lang_opt")%></th>
                </tr>
              </thead>
              <tbody>
              <% Dim i %>
              <% For i=LBound(presentationList, 2) to UBound(presentationList, 2) %>
                <tr>
                  <td><%=presentationList(0, i) %></td>
                  <td><%=presentationList(1, i) %></td>
                  <td><a href="?<%=QP("rf_action")%>=backend&module=presentation.list&pretype=<%=presentationList(2, i)%>"><%=fmtPresentationType(presentationList(2, i)) %></a></td>
                  <td><%=fmtPresentationOfficial(presentationList(4, i)) %></td>
                  <td><%=presentationList(7, i) %></td>
                  <td><%=presentationList(6, i) %></td>
                  <td>
                         
                         <a href="?<%=QP("rf_action")%>=backend&module=presentation.info&id=<%=presentationList(0, i) %>"><%=T("rf_lang_edit")%></a>
                         | 
                        <a href="?<%=QP("rf_action")%>=backend&module=presentation.list&opt=copy&pretype=<%=pretype%>&id=<%=presentationList(0, i) %>&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_do_you_copy")%>')){return true;}return false;}"><%=T("rf_lang_copy")%></a>
                         | 
                         <a href="?<%=QP("rf_action")%>=backend&module=presentation.list&opt=del&pretype=<%=pretype%>&id=<%=presentationList(0, i) %>&<%=rfCsrfTokenQueryString%>" onclick="{if(confirm('<%=T("rf_lang_are_you_sure")%>')){return true;}return false;}"><%=T("rf_lang_delete")%></a>
                  </td>
                </tr>
                <% Next %>

              </tbody>
            </table>
          </div>
<% End If %>   
<div class="row"><%=showPage%></div>
<!-- #include file="../foot.asp" -->